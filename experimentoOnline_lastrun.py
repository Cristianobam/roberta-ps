﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2020.2.10),
    on Thu Apr 29 18:05:06 2021
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2020.2.10'
expName = 'Linguagem'  # from the Builder filename that created this script
expInfo = {'Participante': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['Participante'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='/Users/cristiano/Documents/Gitlab/Roberta-PS/experimentoOnline_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.DEBUG)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# Setup the Window
win = visual.Window(
    size=[1440, 900], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='deg')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "Welcome"
WelcomeClock = core.Clock()
intro = visual.TextStim(win=win, name='intro',
    text='Olá, participante!\n\nVamos começar o experimento?',
    font='Arial',
    pos=(0, 6), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_welcome = visual.TextStim(win=win, name='next_welcome',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
next_welcome_resp = keyboard.Keyboard()
import numpy as np

if np.random.rand() > 0.5:
    keys_resp = ['l', 'a']
    thisExp.addData('Keys', keys_resp)
else:
    keys_resp = ['a', 'l']
    thisExp.addData('Keys', keys_resp)

# Initialize components for Routine "Instructions_DL1"
Instructions_DL1Clock = core.Clock()
DL = visual.TextStim(win=win, name='DL',
    text='Tarefa de Decisão Lexical',
    font='Arial',
    pos=(0, 0), height=2, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
intro_DL1 = visual.TextStim(win=win, name='intro_DL1',
    text='No centro da tela você verá uma CRUZ BRANCA para indicar o local aproximado em que deve fixar seu olhar. \n\nLogo depois, uma palavra aparecerá e você deverá decidir, o mais rapidamente possível, se esta palavra existe ou não em português.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
next_DL1 = visual.TextStim(win=win, name='next_DL1',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_next_DL1 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL2"
Instructions_DL2Clock = core.Clock()
intro_DL2 = visual.TextStim(win=win, name='intro_DL2',
    text='Todas as palavras que aparecerão serão verbos ou pseudoverbos (verbos que não existem em português, mas que parecem com verbos por conta de sua terminação) e após sua aparição você deve responder se a palavra lida era um verbo.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_DL2 = visual.TextStim(win=win, name='next_DL2',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_DL2 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL3"
Instructions_DL3Clock = core.Clock()
intro_DL3 = visual.TextStim(win=win, name='intro_DL3',
    text=f"""
Se você julgar que o que leu foi um VERBO, aperte, O MAIS RÁPIDO POSSÍVEL, em seu teclado, a TECLA {keys_resp[0].upper()}. 

Caso tenha lido um verbo que NÃO EXISTA, aperte a TECLA {keys_resp[1].upper()}.
""",
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_DL3 = visual.TextStim(win=win, name='next_DL3',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_DL3 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL4"
Instructions_DL4Clock = core.Clock()
intro_DL4 = visual.TextStim(win=win, name='intro_DL4',
    text=f"""
Por exemplo: 

- Se após a cruz branca você ler BEIJAR, esse é um verbo pertencente ao vocabulário da Língua Portuguesa, então você deve, nesse caso, apertar a tecla {keys_resp[0].upper()}. 

- Caso após a cruz branca você ler BAIJER, esse NÃO é um verbo pertencente ao vocabulário da Língua Portuguesa, então você deve, nesse caso, apertar a tecla {keys_resp[1].upper()}.
""",
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_DL4 = visual.TextStim(win=win, name='next_DL4',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_DL4 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL5"
Instructions_DL5Clock = core.Clock()
intro_DL5 = visual.TextStim(win=win, name='intro_DL5',
    text='IMPORTANTE: Em alguns momentos, durante o andamento da tarefa, você verá na tela uma seta que pode apontar para cima ou para baixo. Após o aparecimento da seta, haverá a pergunta: A SETA APONTA PARA CIMA?  \n \nNeste momento, de acordo com a orientação da seta, você também usará as mesmas teclas do teclado correspondentes ao SIM e ao NÃO para responder.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_DL5 = visual.TextStim(win=win, name='next_DL5',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_DL5 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL6"
Instructions_DL6Clock = core.Clock()
intro_DL6 = visual.TextStim(win=win, name='intro_DL6',
    text='Antes da tarefa começar, você poderá treinar um pouco para garantir que entendeu a dinâmica desta parte do experimento.\n \nLembre-se: sua tarefa agora é a de julgar, o mais rápido possível, se o que apareceu escrito na tela foi um verbo em português ou simplesmente uma sequência de letras que não faz sentido.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_DL6 = visual.TextStim(win=win, name='next_DL6',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_DL6 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL7"
Instructions_DL7Clock = core.Clock()
intro_DL7 = visual.TextStim(win=win, name='intro_DL7',
    text='Procure responder o mais rápido possível e já posicione e mantenha sempre seus dedos indicadores  sobre as teclas referentes às possíveis respostas ( tecla A ou L).\n \nVamos começar? Quando estiver pronto para treinar, APERTE ESPAÇO PARA PROSSEGUIR',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_next_DL7 = keyboard.Keyboard()

# Initialize components for Routine "Train_DL"
Train_DLClock = core.Clock()
fixation_cross_train_DL = visual.ShapeStim(
    win=win, name='fixation_cross_train_DL', vertices='cross',
    size=(0.5, 0.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-1.0, interpolate=True)
text_train_DL = visual.TextStim(win=win, name='text_train_DL',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1.31, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_train_DL = keyboard.Keyboard()

# Initialize components for Routine "Feedback_Train_DL"
Feedback_Train_DLClock = core.Clock()
respcorrs = []
msg = 'doh!'
feedback_text_train_DL = visual.TextStim(win=win, name='feedback_text_train_DL',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "Train_DL_Arrow"
Train_DL_ArrowClock = core.Clock()
fixation_cross_train_DL_2 = visual.ShapeStim(
    win=win, name='fixation_cross_train_DL_2', vertices='cross',
    size=(.5,.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-2.0, interpolate=True)
arrow_image_train_DL = visual.ImageStim(
    win=win,
    name='arrow_image_train_DL', 
    image='arrow.png', mask=None,
    ori=1.0, pos=(0, 0), size=(4, 6),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=512, interpolate=True, depth=-3.0)
train_DL_text_arrow = visual.TextStim(win=win, name='train_DL_text_arrow',
    text='A seta estava apontando para cima?',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
resp_train_DL_arrow = keyboard.Keyboard()

# Initialize components for Routine "Instructions_DL8"
Instructions_DL8Clock = core.Clock()
intro_DL8 = visual.TextStim(win=win, name='intro_DL8',
    text='Fim do Treino!\n\nLembre-se: sua tarefa é a de julgar, o mais rápido possível, se o que apareceu escrito foi um verbo em português ou simplesmente uma sequência de letras que não faz sentido.\n \nQuando estiver pronto, APERTE ESPAÇO PARA DAR INÍCIO ao teste e sempre mantenha seus dedos indicadores posicionados sobre as teclas A e L.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_next_DL8 = keyboard.Keyboard()

# Initialize components for Routine "Test_DL"
Test_DLClock = core.Clock()
fixation_cross_test_DL = visual.ShapeStim(
    win=win, name='fixation_cross_test_DL', vertices='cross',
    size=(0.5, 0.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-1.0, interpolate=True)
text_test_DL = visual.TextStim(win=win, name='text_test_DL',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1.31, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_test_DL = keyboard.Keyboard()

# Initialize components for Routine "Test_DL_Arrow"
Test_DL_ArrowClock = core.Clock()
fixation_cross_test_DL_2 = visual.ShapeStim(
    win=win, name='fixation_cross_test_DL_2', vertices='cross',
    size=(.5,.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-2.0, interpolate=True)
arrow_image_test_DL = visual.ImageStim(
    win=win,
    name='arrow_image_test_DL', 
    image='arrow.png', mask=None,
    ori=1.0, pos=(0, 0), size=(4, 6),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=512, interpolate=True, depth=-3.0)
test_DL_text_arrow = visual.TextStim(win=win, name='test_DL_text_arrow',
    text='A seta estava apontando para cima?',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
resp_test_DL_arrow = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS1_1"
Instructions_JS1_1Clock = core.Clock()
JS1_1 = visual.TextStim(win=win, name='JS1_1',
    text='Tarefa de Julgamento Semântico\n\nParte 1',
    font='Arial',
    pos=(0, 0), height=2, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
intro_JS1_1 = visual.TextStim(win=win, name='intro_JS1_1',
    text='Esta etapa é semelhante à anterior, só que agora, no centro da tela você verá uma CRUZ BRANCA, depois dela surgirá um verbo e você deve responder, usando as mesmas teclas de antes (A ou L), se esse verbo para ser feito utiliza ESPECIFICAMENTE A BOCA, O NARIZ OU AS MÃOS.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
next_JS1_1 = visual.TextStim(win=win, name='next_JS1_1',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_next_JS1_1 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS1_2"
Instructions_JS1_2Clock = core.Clock()
intro_JS1_2 = visual.TextStim(win=win, name='intro_JS1_2',
    text=f"""
Se A RESPOSTA FOR NÃO, aperte, o MAIS RÁPIDO POSSÍVEL, a tecla {keys_resp[1].upper()}.

Se A AÇÃO UTILIZAR UMA DESSAS PARTES DO CORPO (BOCA, NARIZ OU MÃO), A RESPOSTA SERÁ SIM,  ENTÃO aperte, o MAIS RÁPIDO POSSÍVEL, a tecla {keys_resp[0].upper()}.
""",
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_JS1_2 = visual.TextStim(win=win, name='next_JS1_2',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_JS1_2 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS1_3"
Instructions_JS1_3Clock = core.Clock()
intro_JS1_3 = visual.TextStim(win=win, name='intro_JS1_3',
    text='Um verbo como Remexer é considerado como sendo de muito movimento, enquanto que um verbo como Meditar é considerado como sendo de pouco movimento.\n\nVamos lá? Os verbos lidos a seguir são verbos que possuem MUITO MOVIMENTO?',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_JS1_3 = visual.TextStim(win=win, name='next_JS1_3',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_JS1_3 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS1_4"
Instructions_JS1_4Clock = core.Clock()
intro_JS1_4 = visual.TextStim(win=win, name='intro_JS1_4',
    text='Vamos lá? Os verbos lidos a seguir são verbos que utilizam ESPECIFICAMENTE boca, nariz ou mãos para serem executados?\n \nQuando estiver pronto para responder, aperte espaço para iniciar.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_next_JS1_4 = keyboard.Keyboard()

# Initialize components for Routine "Train_JS1"
Train_JS1Clock = core.Clock()
fixation_cross_train_JS1 = visual.ShapeStim(
    win=win, name='fixation_cross_train_JS1', vertices='cross',
    size=(0.5, 0.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-1.0, interpolate=True)
text_train_JS1 = visual.TextStim(win=win, name='text_train_JS1',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1.31, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_train_JS1 = keyboard.Keyboard()

# Initialize components for Routine "Feedback_Train_JS1"
Feedback_Train_JS1Clock = core.Clock()
respcorrs = []
feedback_text_train_JS1 = visual.TextStim(win=win, name='feedback_text_train_JS1',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "Train_JS1_Arrow"
Train_JS1_ArrowClock = core.Clock()
fixation_cross_train_JS1_2 = visual.ShapeStim(
    win=win, name='fixation_cross_train_JS1_2', vertices='cross',
    size=(.5,.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-2.0, interpolate=True)
arrow_image_train_JS1 = visual.ImageStim(
    win=win,
    name='arrow_image_train_JS1', 
    image='arrow.png', mask=None,
    ori=1.0, pos=(0, 0), size=(4, 6),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=512, interpolate=True, depth=-3.0)
train_JS1_text_arrow = visual.TextStim(win=win, name='train_JS1_text_arrow',
    text='A seta estava apontando para cima?',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
resp_train_JS1_arrow = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS1_5"
Instructions_JS1_5Clock = core.Clock()
intro_JS1_5 = visual.TextStim(win=win, name='intro_JS1_5',
    text='Fim do treino!\n \nLembre-se: sua tarefa agora é a de julgar, o mais rápido possível, se os verbos lidos são verbos que utilizam ESPECIFICAMENTE boca, nariz ou mãos para serem executados?\n \nQuando estiver pronto, APERTE ESPAÇO PARA DAR INÍCIO ao teste e sempre mantenha seus dedos indicadores posicionados sobre as teclas A e L.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_next_JS1_5 = keyboard.Keyboard()

# Initialize components for Routine "Test_JS1"
Test_JS1Clock = core.Clock()
fixation_cross_test_JS1_1 = visual.ShapeStim(
    win=win, name='fixation_cross_test_JS1_1', vertices='cross',
    size=(0.5, 0.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-1.0, interpolate=True)
text_test_JS1 = visual.TextStim(win=win, name='text_test_JS1',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1.31, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_test_JS1 = keyboard.Keyboard()

# Initialize components for Routine "Test_JS1_Arrow"
Test_JS1_ArrowClock = core.Clock()
fixation_cross_test_JS1 = visual.ShapeStim(
    win=win, name='fixation_cross_test_JS1', vertices='cross',
    size=(.5,.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-2.0, interpolate=True)
arrow_image_test_JS1 = visual.ImageStim(
    win=win,
    name='arrow_image_test_JS1', 
    image='arrow.png', mask=None,
    ori=1.0, pos=(0, 0), size=(4, 6),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=512, interpolate=True, depth=-3.0)
test_JS1_text_arrow = visual.TextStim(win=win, name='test_JS1_text_arrow',
    text='A seta estava apontando para cima?',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
resp_test_JS1_arrow = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS2_1"
Instructions_JS2_1Clock = core.Clock()
JS2_1 = visual.TextStim(win=win, name='JS2_1',
    text='Tarefa de Julgamento Semântico\n\nParte 2',
    font='Arial',
    pos=(0, 0), height=2, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
intro_JS2_1 = visual.TextStim(win=win, name='intro_JS2_1',
    text=f"""
Nesta última fase da tarefa, você lerá na tela um par de verbos e deve responder se esses dois verbos são sinônimos entre si. 

São considerados sinônimos, os verbos que têm sentido semelhante.

Se o verbo NÃO for sinônimo, aperte o MAIS RÁPIDO POSSÍVEL, a TECLA {keys_resp[1].upper()}.
Se o verbo for sinônimo, aperte o MAIS RÁPIDO POSSÍVEL, a TECLA {keys_resp[0].upper()}.
""",
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
next_JS2_1 = visual.TextStim(win=win, name='next_JS2_1',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_next_JS2_1 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS2_2"
Instructions_JS2_2Clock = core.Clock()
intro_JS2_2 = visual.TextStim(win=win, name='intro_JS2_2',
    text=f"""
Por exemplo:
 
Se após a cruz branca, aparecer o par de verbos AMAR-SAMBAR, você deve apertar a  TECLA {keys_resp[1].upper()},  porque amar e sambar NÃO  são sinônimos.
 
Se após a cruz branca, aparecer o par de verbos AMAR-ADORAR, você deve apertar a TECLA {keys_resp[0].upper()},  porque amar e adorar SÃO  sinônimos.
""",
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_JS2_2 = visual.TextStim(win=win, name='next_JS2_2',
    text='Pressione ESPAÇO para continuar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
resp_next_JS2_2 = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS2_3"
Instructions_JS2_3Clock = core.Clock()
intro_JS2_3 = visual.TextStim(win=win, name='intro_JS2_3',
    text='IMPORTANTE: procure responder o mais rápido possível e mantenha sempre seus dedos indicadores  sobre as teclas referentes às possíveis respostas (tecla A ou L).\n \nNovamente, antes de começar o registro oficial de respostas, haverá um treino para que você entenda a dinâmica da tarefa.\n \nVamos lá?\n \nAperte espaço para prosseguir.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_next_JS2_3 = keyboard.Keyboard()

# Initialize components for Routine "Train_JS2"
Train_JS2Clock = core.Clock()
fixation_cross_train_JS2 = visual.ShapeStim(
    win=win, name='fixation_cross_train_JS2', vertices='cross',
    size=(0.5, 0.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-1.0, interpolate=True)
text_train_JS2 = visual.TextStim(win=win, name='text_train_JS2',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_train_JS2 = keyboard.Keyboard()

# Initialize components for Routine "Feedback_Train_JS2"
Feedback_Train_JS2Clock = core.Clock()
respcorrs = []
feedback_text_train_JS2 = visual.TextStim(win=win, name='feedback_text_train_JS2',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);

# Initialize components for Routine "Train_JS2_Arrow"
Train_JS2_ArrowClock = core.Clock()
fixation_cross_train_JS2_2 = visual.ShapeStim(
    win=win, name='fixation_cross_train_JS2_2', vertices='cross',
    size=(.5,.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-2.0, interpolate=True)
arrow_image_train_JS2 = visual.ImageStim(
    win=win,
    name='arrow_image_train_JS2', 
    image='arrow.png', mask=None,
    ori=1.0, pos=(0, 0), size=(4, 6),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=512, interpolate=True, depth=-3.0)
train_JS2_text_arrow = visual.TextStim(win=win, name='train_JS2_text_arrow',
    text='A seta estava apontando para cima?',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
resp_train_JS2_arrow = keyboard.Keyboard()

# Initialize components for Routine "Instructions_JS2_4"
Instructions_JS2_4Clock = core.Clock()
intro_JS2_4 = visual.TextStim(win=win, name='intro_JS2_4',
    text='Fim do treino!\n \nLembre-se: sua tarefa agora é a de julgar, o mais rápido possível, se os pares de verbos são SINÔNIMOS.\n \nQuando estiver pronto, APERTE ESPAÇO PARA DAR INÍCIO ao teste e sempre mantenha seus dedos indicadores posicionados nas teclas A e L.',
    font='Arial',
    pos=(0, 2), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_next_JS2_4 = keyboard.Keyboard()

# Initialize components for Routine "Test_JS2"
Test_JS2Clock = core.Clock()
fixation_cross_test_JS2_1 = visual.ShapeStim(
    win=win, name='fixation_cross_test_JS2_1', vertices='cross',
    size=(0.5, 0.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-1.0, interpolate=True)
text_test_JS2 = visual.TextStim(win=win, name='text_test_JS2',
    text='default text',
    font='Arial',
    pos=(0, 0), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-2.0);
resp_test_JS2 = keyboard.Keyboard()

# Initialize components for Routine "Test_JS2_Arrow"
Test_JS2_ArrowClock = core.Clock()
fixation_cross_test_JS2 = visual.ShapeStim(
    win=win, name='fixation_cross_test_JS2', vertices='cross',
    size=(.5,.5),
    ori=0, pos=(0, 0),
    lineWidth=1, lineColor=[1,1,1], lineColorSpace='rgb',
    fillColor=[1,1,1], fillColorSpace='rgb',
    opacity=1, depth=-2.0, interpolate=True)
arrow_image_test_JS2 = visual.ImageStim(
    win=win,
    name='arrow_image_test_JS2', 
    image='arrow.png', mask=None,
    ori=1.0, pos=(0, 0), size=(4, 6),
    color=[1,1,1], colorSpace='rgb', opacity=1,
    flipHoriz=False, flipVert=False,
    texRes=512, interpolate=True, depth=-3.0)
test_JS2_text_arrow = visual.TextStim(win=win, name='test_JS2_text_arrow',
    text='A seta estava apontando para cima?',
    font='Arial',
    pos=(0, 0), height=1, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-4.0);
resp_test_JS2_arrow = keyboard.Keyboard()

# Initialize components for Routine "End"
EndClock = core.Clock()
end = visual.TextStim(win=win, name='end',
    text='Bem, acabamos por aqui!\n\nMuito obrigada por participar. ',
    font='Arial',
    pos=(0, 6), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
next_end = visual.TextStim(win=win, name='next_end',
    text='Pressione ESPAÇO para finalizar.',
    font='Arial',
    pos=(0, -8), height=1.31, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=-1.0);
next_end_resp = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "Welcome"-------
continueRoutine = True
# update component parameters for each repeat
next_welcome_resp.keys = []
next_welcome_resp.rt = []
_next_welcome_resp_allKeys = []
# keep track of which components have finished
WelcomeComponents = [intro, next_welcome, next_welcome_resp]
for thisComponent in WelcomeComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
WelcomeClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Welcome"-------
while continueRoutine:
    # get current time
    t = WelcomeClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=WelcomeClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro* updates
    if intro.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro.frameNStart = frameN  # exact frame index
        intro.tStart = t  # local t and not account for scr refresh
        intro.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro, 'tStartRefresh')  # time at next scr refresh
        intro.setAutoDraw(True)
    
    # *next_welcome* updates
    if next_welcome.status == NOT_STARTED and tThisFlip >= .5-frameTolerance:
        # keep track of start time/frame for later
        next_welcome.frameNStart = frameN  # exact frame index
        next_welcome.tStart = t  # local t and not account for scr refresh
        next_welcome.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_welcome, 'tStartRefresh')  # time at next scr refresh
        next_welcome.setAutoDraw(True)
    
    # *next_welcome_resp* updates
    waitOnFlip = False
    if next_welcome_resp.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
        # keep track of start time/frame for later
        next_welcome_resp.frameNStart = frameN  # exact frame index
        next_welcome_resp.tStart = t  # local t and not account for scr refresh
        next_welcome_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_welcome_resp, 'tStartRefresh')  # time at next scr refresh
        next_welcome_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(next_welcome_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(next_welcome_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if next_welcome_resp.status == STARTED and not waitOnFlip:
        theseKeys = next_welcome_resp.getKeys(keyList=['space'], waitRelease=False)
        _next_welcome_resp_allKeys.extend(theseKeys)
        if len(_next_welcome_resp_allKeys):
            next_welcome_resp.keys = _next_welcome_resp_allKeys[-1].name  # just the last key pressed
            next_welcome_resp.rt = _next_welcome_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in WelcomeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Welcome"-------
for thisComponent in WelcomeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro.started', intro.tStartRefresh)
thisExp.addData('intro.stopped', intro.tStopRefresh)
thisExp.addData('next_welcome.started', next_welcome.tStartRefresh)
thisExp.addData('next_welcome.stopped', next_welcome.tStopRefresh)
# check responses
if next_welcome_resp.keys in ['', [], None]:  # No response was made
    next_welcome_resp.keys = None
thisExp.addData('next_welcome_resp.keys',next_welcome_resp.keys)
if next_welcome_resp.keys != None:  # we had a response
    thisExp.addData('next_welcome_resp.rt', next_welcome_resp.rt)
thisExp.addData('next_welcome_resp.started', next_welcome_resp.tStartRefresh)
thisExp.addData('next_welcome_resp.stopped', next_welcome_resp.tStopRefresh)
thisExp.nextEntry()
# the Routine "Welcome" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL1"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL1.keys = []
resp_next_DL1.rt = []
_resp_next_DL1_allKeys = []
# keep track of which components have finished
Instructions_DL1Components = [DL, intro_DL1, next_DL1, resp_next_DL1]
for thisComponent in Instructions_DL1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL1"-------
while continueRoutine:
    # get current time
    t = Instructions_DL1Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL1Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *DL* updates
    if DL.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        DL.frameNStart = frameN  # exact frame index
        DL.tStart = t  # local t and not account for scr refresh
        DL.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(DL, 'tStartRefresh')  # time at next scr refresh
        DL.setAutoDraw(True)
    if DL.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > DL.tStartRefresh + 1.0-frameTolerance:
            # keep track of stop time/frame for later
            DL.tStop = t  # not accounting for scr refresh
            DL.frameNStop = frameN  # exact frame index
            win.timeOnFlip(DL, 'tStopRefresh')  # time at next scr refresh
            DL.setAutoDraw(False)
    
    # *intro_DL1* updates
    if intro_DL1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        intro_DL1.frameNStart = frameN  # exact frame index
        intro_DL1.tStart = t  # local t and not account for scr refresh
        intro_DL1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL1, 'tStartRefresh')  # time at next scr refresh
        intro_DL1.setAutoDraw(True)
    
    # *next_DL1* updates
    if next_DL1.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
        # keep track of start time/frame for later
        next_DL1.frameNStart = frameN  # exact frame index
        next_DL1.tStart = t  # local t and not account for scr refresh
        next_DL1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_DL1, 'tStartRefresh')  # time at next scr refresh
        next_DL1.setAutoDraw(True)
    
    # *resp_next_DL1* updates
    waitOnFlip = False
    if resp_next_DL1.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL1.frameNStart = frameN  # exact frame index
        resp_next_DL1.tStart = t  # local t and not account for scr refresh
        resp_next_DL1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL1, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL1.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL1.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL1.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL1.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL1.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL1_allKeys.extend(theseKeys)
        if len(_resp_next_DL1_allKeys):
            resp_next_DL1.keys = _resp_next_DL1_allKeys[-1].name  # just the last key pressed
            resp_next_DL1.rt = _resp_next_DL1_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL1"-------
for thisComponent in Instructions_DL1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('DL.started', DL.tStartRefresh)
thisExp.addData('DL.stopped', DL.tStopRefresh)
thisExp.addData('intro_DL1.started', intro_DL1.tStartRefresh)
thisExp.addData('intro_DL1.stopped', intro_DL1.tStopRefresh)
thisExp.addData('next_DL1.started', next_DL1.tStartRefresh)
thisExp.addData('next_DL1.stopped', next_DL1.tStopRefresh)
# check responses
if resp_next_DL1.keys in ['', [], None]:  # No response was made
    resp_next_DL1.keys = None
thisExp.addData('resp_next_DL1.keys',resp_next_DL1.keys)
if resp_next_DL1.keys != None:  # we had a response
    thisExp.addData('resp_next_DL1.rt', resp_next_DL1.rt)
thisExp.addData('resp_next_DL1.started', resp_next_DL1.tStartRefresh)
thisExp.addData('resp_next_DL1.stopped', resp_next_DL1.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL2"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL2.keys = []
resp_next_DL2.rt = []
_resp_next_DL2_allKeys = []
# keep track of which components have finished
Instructions_DL2Components = [intro_DL2, next_DL2, resp_next_DL2]
for thisComponent in Instructions_DL2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL2"-------
while continueRoutine:
    # get current time
    t = Instructions_DL2Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL2Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL2* updates
    if intro_DL2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL2.frameNStart = frameN  # exact frame index
        intro_DL2.tStart = t  # local t and not account for scr refresh
        intro_DL2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL2, 'tStartRefresh')  # time at next scr refresh
        intro_DL2.setAutoDraw(True)
    
    # *next_DL2* updates
    if next_DL2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_DL2.frameNStart = frameN  # exact frame index
        next_DL2.tStart = t  # local t and not account for scr refresh
        next_DL2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_DL2, 'tStartRefresh')  # time at next scr refresh
        next_DL2.setAutoDraw(True)
    
    # *resp_next_DL2* updates
    waitOnFlip = False
    if resp_next_DL2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL2.frameNStart = frameN  # exact frame index
        resp_next_DL2.tStart = t  # local t and not account for scr refresh
        resp_next_DL2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL2, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL2.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL2.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL2.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL2.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL2.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL2_allKeys.extend(theseKeys)
        if len(_resp_next_DL2_allKeys):
            resp_next_DL2.keys = _resp_next_DL2_allKeys[-1].name  # just the last key pressed
            resp_next_DL2.rt = _resp_next_DL2_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL2"-------
for thisComponent in Instructions_DL2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL2.started', intro_DL2.tStartRefresh)
thisExp.addData('intro_DL2.stopped', intro_DL2.tStopRefresh)
thisExp.addData('next_DL2.started', next_DL2.tStartRefresh)
thisExp.addData('next_DL2.stopped', next_DL2.tStopRefresh)
# check responses
if resp_next_DL2.keys in ['', [], None]:  # No response was made
    resp_next_DL2.keys = None
thisExp.addData('resp_next_DL2.keys',resp_next_DL2.keys)
if resp_next_DL2.keys != None:  # we had a response
    thisExp.addData('resp_next_DL2.rt', resp_next_DL2.rt)
thisExp.addData('resp_next_DL2.started', resp_next_DL2.tStartRefresh)
thisExp.addData('resp_next_DL2.stopped', resp_next_DL2.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL3"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL3.keys = []
resp_next_DL3.rt = []
_resp_next_DL3_allKeys = []
# keep track of which components have finished
Instructions_DL3Components = [intro_DL3, next_DL3, resp_next_DL3]
for thisComponent in Instructions_DL3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL3Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL3"-------
while continueRoutine:
    # get current time
    t = Instructions_DL3Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL3Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL3* updates
    if intro_DL3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL3.frameNStart = frameN  # exact frame index
        intro_DL3.tStart = t  # local t and not account for scr refresh
        intro_DL3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL3, 'tStartRefresh')  # time at next scr refresh
        intro_DL3.setAutoDraw(True)
    
    # *next_DL3* updates
    if next_DL3.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_DL3.frameNStart = frameN  # exact frame index
        next_DL3.tStart = t  # local t and not account for scr refresh
        next_DL3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_DL3, 'tStartRefresh')  # time at next scr refresh
        next_DL3.setAutoDraw(True)
    
    # *resp_next_DL3* updates
    waitOnFlip = False
    if resp_next_DL3.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL3.frameNStart = frameN  # exact frame index
        resp_next_DL3.tStart = t  # local t and not account for scr refresh
        resp_next_DL3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL3, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL3.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL3.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL3.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL3.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL3.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL3_allKeys.extend(theseKeys)
        if len(_resp_next_DL3_allKeys):
            resp_next_DL3.keys = _resp_next_DL3_allKeys[-1].name  # just the last key pressed
            resp_next_DL3.rt = _resp_next_DL3_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL3"-------
for thisComponent in Instructions_DL3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL3.started', intro_DL3.tStartRefresh)
thisExp.addData('intro_DL3.stopped', intro_DL3.tStopRefresh)
thisExp.addData('next_DL3.started', next_DL3.tStartRefresh)
thisExp.addData('next_DL3.stopped', next_DL3.tStopRefresh)
# check responses
if resp_next_DL3.keys in ['', [], None]:  # No response was made
    resp_next_DL3.keys = None
thisExp.addData('resp_next_DL3.keys',resp_next_DL3.keys)
if resp_next_DL3.keys != None:  # we had a response
    thisExp.addData('resp_next_DL3.rt', resp_next_DL3.rt)
thisExp.addData('resp_next_DL3.started', resp_next_DL3.tStartRefresh)
thisExp.addData('resp_next_DL3.stopped', resp_next_DL3.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL4"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL4.keys = []
resp_next_DL4.rt = []
_resp_next_DL4_allKeys = []
# keep track of which components have finished
Instructions_DL4Components = [intro_DL4, next_DL4, resp_next_DL4]
for thisComponent in Instructions_DL4Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL4Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL4"-------
while continueRoutine:
    # get current time
    t = Instructions_DL4Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL4Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL4* updates
    if intro_DL4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL4.frameNStart = frameN  # exact frame index
        intro_DL4.tStart = t  # local t and not account for scr refresh
        intro_DL4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL4, 'tStartRefresh')  # time at next scr refresh
        intro_DL4.setAutoDraw(True)
    
    # *next_DL4* updates
    if next_DL4.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_DL4.frameNStart = frameN  # exact frame index
        next_DL4.tStart = t  # local t and not account for scr refresh
        next_DL4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_DL4, 'tStartRefresh')  # time at next scr refresh
        next_DL4.setAutoDraw(True)
    
    # *resp_next_DL4* updates
    waitOnFlip = False
    if resp_next_DL4.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL4.frameNStart = frameN  # exact frame index
        resp_next_DL4.tStart = t  # local t and not account for scr refresh
        resp_next_DL4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL4, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL4.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL4.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL4.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL4.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL4.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL4_allKeys.extend(theseKeys)
        if len(_resp_next_DL4_allKeys):
            resp_next_DL4.keys = _resp_next_DL4_allKeys[-1].name  # just the last key pressed
            resp_next_DL4.rt = _resp_next_DL4_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL4Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL4"-------
for thisComponent in Instructions_DL4Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL4.started', intro_DL4.tStartRefresh)
thisExp.addData('intro_DL4.stopped', intro_DL4.tStopRefresh)
thisExp.addData('next_DL4.started', next_DL4.tStartRefresh)
thisExp.addData('next_DL4.stopped', next_DL4.tStopRefresh)
# check responses
if resp_next_DL4.keys in ['', [], None]:  # No response was made
    resp_next_DL4.keys = None
thisExp.addData('resp_next_DL4.keys',resp_next_DL4.keys)
if resp_next_DL4.keys != None:  # we had a response
    thisExp.addData('resp_next_DL4.rt', resp_next_DL4.rt)
thisExp.addData('resp_next_DL4.started', resp_next_DL4.tStartRefresh)
thisExp.addData('resp_next_DL4.stopped', resp_next_DL4.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL4" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL5"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL5.keys = []
resp_next_DL5.rt = []
_resp_next_DL5_allKeys = []
# keep track of which components have finished
Instructions_DL5Components = [intro_DL5, next_DL5, resp_next_DL5]
for thisComponent in Instructions_DL5Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL5Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL5"-------
while continueRoutine:
    # get current time
    t = Instructions_DL5Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL5Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL5* updates
    if intro_DL5.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL5.frameNStart = frameN  # exact frame index
        intro_DL5.tStart = t  # local t and not account for scr refresh
        intro_DL5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL5, 'tStartRefresh')  # time at next scr refresh
        intro_DL5.setAutoDraw(True)
    
    # *next_DL5* updates
    if next_DL5.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_DL5.frameNStart = frameN  # exact frame index
        next_DL5.tStart = t  # local t and not account for scr refresh
        next_DL5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_DL5, 'tStartRefresh')  # time at next scr refresh
        next_DL5.setAutoDraw(True)
    
    # *resp_next_DL5* updates
    waitOnFlip = False
    if resp_next_DL5.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL5.frameNStart = frameN  # exact frame index
        resp_next_DL5.tStart = t  # local t and not account for scr refresh
        resp_next_DL5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL5, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL5.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL5.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL5.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL5.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL5.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL5_allKeys.extend(theseKeys)
        if len(_resp_next_DL5_allKeys):
            resp_next_DL5.keys = _resp_next_DL5_allKeys[-1].name  # just the last key pressed
            resp_next_DL5.rt = _resp_next_DL5_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL5Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL5"-------
for thisComponent in Instructions_DL5Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL5.started', intro_DL5.tStartRefresh)
thisExp.addData('intro_DL5.stopped', intro_DL5.tStopRefresh)
thisExp.addData('next_DL5.started', next_DL5.tStartRefresh)
thisExp.addData('next_DL5.stopped', next_DL5.tStopRefresh)
# check responses
if resp_next_DL5.keys in ['', [], None]:  # No response was made
    resp_next_DL5.keys = None
thisExp.addData('resp_next_DL5.keys',resp_next_DL5.keys)
if resp_next_DL5.keys != None:  # we had a response
    thisExp.addData('resp_next_DL5.rt', resp_next_DL5.rt)
thisExp.addData('resp_next_DL5.started', resp_next_DL5.tStartRefresh)
thisExp.addData('resp_next_DL5.stopped', resp_next_DL5.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL5" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL6"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL6.keys = []
resp_next_DL6.rt = []
_resp_next_DL6_allKeys = []
# keep track of which components have finished
Instructions_DL6Components = [intro_DL6, next_DL6, resp_next_DL6]
for thisComponent in Instructions_DL6Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL6Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL6"-------
while continueRoutine:
    # get current time
    t = Instructions_DL6Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL6Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL6* updates
    if intro_DL6.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL6.frameNStart = frameN  # exact frame index
        intro_DL6.tStart = t  # local t and not account for scr refresh
        intro_DL6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL6, 'tStartRefresh')  # time at next scr refresh
        intro_DL6.setAutoDraw(True)
    
    # *next_DL6* updates
    if next_DL6.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_DL6.frameNStart = frameN  # exact frame index
        next_DL6.tStart = t  # local t and not account for scr refresh
        next_DL6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_DL6, 'tStartRefresh')  # time at next scr refresh
        next_DL6.setAutoDraw(True)
    
    # *resp_next_DL6* updates
    waitOnFlip = False
    if resp_next_DL6.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL6.frameNStart = frameN  # exact frame index
        resp_next_DL6.tStart = t  # local t and not account for scr refresh
        resp_next_DL6.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL6, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL6.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL6.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL6.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL6.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL6.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL6_allKeys.extend(theseKeys)
        if len(_resp_next_DL6_allKeys):
            resp_next_DL6.keys = _resp_next_DL6_allKeys[-1].name  # just the last key pressed
            resp_next_DL6.rt = _resp_next_DL6_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL6Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL6"-------
for thisComponent in Instructions_DL6Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL6.started', intro_DL6.tStartRefresh)
thisExp.addData('intro_DL6.stopped', intro_DL6.tStopRefresh)
thisExp.addData('next_DL6.started', next_DL6.tStartRefresh)
thisExp.addData('next_DL6.stopped', next_DL6.tStopRefresh)
# check responses
if resp_next_DL6.keys in ['', [], None]:  # No response was made
    resp_next_DL6.keys = None
thisExp.addData('resp_next_DL6.keys',resp_next_DL6.keys)
if resp_next_DL6.keys != None:  # we had a response
    thisExp.addData('resp_next_DL6.rt', resp_next_DL6.rt)
thisExp.addData('resp_next_DL6.started', resp_next_DL6.tStartRefresh)
thisExp.addData('resp_next_DL6.stopped', resp_next_DL6.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL6" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_DL7"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL7.keys = []
resp_next_DL7.rt = []
_resp_next_DL7_allKeys = []
# keep track of which components have finished
Instructions_DL7Components = [intro_DL7, resp_next_DL7]
for thisComponent in Instructions_DL7Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL7Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL7"-------
while continueRoutine:
    # get current time
    t = Instructions_DL7Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL7Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL7* updates
    if intro_DL7.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL7.frameNStart = frameN  # exact frame index
        intro_DL7.tStart = t  # local t and not account for scr refresh
        intro_DL7.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL7, 'tStartRefresh')  # time at next scr refresh
        intro_DL7.setAutoDraw(True)
    
    # *resp_next_DL7* updates
    waitOnFlip = False
    if resp_next_DL7.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL7.frameNStart = frameN  # exact frame index
        resp_next_DL7.tStart = t  # local t and not account for scr refresh
        resp_next_DL7.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL7, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL7.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL7.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL7.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL7.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL7.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL7_allKeys.extend(theseKeys)
        if len(_resp_next_DL7_allKeys):
            resp_next_DL7.keys = _resp_next_DL7_allKeys[-1].name  # just the last key pressed
            resp_next_DL7.rt = _resp_next_DL7_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL7Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL7"-------
for thisComponent in Instructions_DL7Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL7.started', intro_DL7.tStartRefresh)
thisExp.addData('intro_DL7.stopped', intro_DL7.tStopRefresh)
# check responses
if resp_next_DL7.keys in ['', [], None]:  # No response was made
    resp_next_DL7.keys = None
thisExp.addData('resp_next_DL7.keys',resp_next_DL7.keys)
if resp_next_DL7.keys != None:  # we had a response
    thisExp.addData('resp_next_DL7.rt', resp_next_DL7.rt)
thisExp.addData('resp_next_DL7.started', resp_next_DL7.tStartRefresh)
thisExp.addData('resp_next_DL7.stopped', resp_next_DL7.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL7" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
Seq_Train_DL = data.TrialHandler(nReps=100, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Estimulos.xlsx', selection=':20'),
    seed=None, name='Seq_Train_DL')
thisExp.addLoop(Seq_Train_DL)  # add the loop to the experiment
thisSeq_Train_DL = Seq_Train_DL.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSeq_Train_DL.rgb)
if thisSeq_Train_DL != None:
    for paramName in thisSeq_Train_DL:
        exec('{} = thisSeq_Train_DL[paramName]'.format(paramName))

for thisSeq_Train_DL in Seq_Train_DL:
    currentLoop = Seq_Train_DL
    # abbreviate parameter names if possible (e.g. rgb = thisSeq_Train_DL.rgb)
    if thisSeq_Train_DL != None:
        for paramName in thisSeq_Train_DL:
            exec('{} = thisSeq_Train_DL[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Train_DL"-------
    continueRoutine = True
    routineTimer.add(5.000000)
    # update component parameters for each repeat
    if keys_resp.index('a')==0 and RESPOSTA_DL_TREINO==1:
        key_corr = 'a' 
    elif keys_resp.index('l')==1 and RESPOSTA_DL_TREINO==0:
        key_corr = 'l'
    elif keys_resp.index('l')==0 and RESPOSTA_DL_TREINO==1:
        key_corr = 'l' 
    elif keys_resp.index('a')==1 and RESPOSTA_DL_TREINO==0:
        key_corr = 'a'
    text_train_DL.setText(VERBO_DL_TREINO)
    resp_train_DL.keys = []
    resp_train_DL.rt = []
    _resp_train_DL_allKeys = []
    # keep track of which components have finished
    Train_DLComponents = [fixation_cross_train_DL, text_train_DL, resp_train_DL]
    for thisComponent in Train_DLComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Train_DLClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Train_DL"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = Train_DLClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Train_DLClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_train_DL* updates
        if fixation_cross_train_DL.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_train_DL.frameNStart = frameN  # exact frame index
            fixation_cross_train_DL.tStart = t  # local t and not account for scr refresh
            fixation_cross_train_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_train_DL, 'tStartRefresh')  # time at next scr refresh
            fixation_cross_train_DL.setAutoDraw(True)
        if fixation_cross_train_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_train_DL.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_train_DL.tStop = t  # not accounting for scr refresh
                fixation_cross_train_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(fixation_cross_train_DL, 'tStopRefresh')  # time at next scr refresh
                fixation_cross_train_DL.setAutoDraw(False)
        
        # *text_train_DL* updates
        if text_train_DL.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            text_train_DL.frameNStart = frameN  # exact frame index
            text_train_DL.tStart = t  # local t and not account for scr refresh
            text_train_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_train_DL, 'tStartRefresh')  # time at next scr refresh
            text_train_DL.setAutoDraw(True)
        if text_train_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > text_train_DL.tStartRefresh + 0.350-frameTolerance:
                # keep track of stop time/frame for later
                text_train_DL.tStop = t  # not accounting for scr refresh
                text_train_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(text_train_DL, 'tStopRefresh')  # time at next scr refresh
                text_train_DL.setAutoDraw(False)
        
        # *resp_train_DL* updates
        waitOnFlip = False
        if resp_train_DL.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            resp_train_DL.frameNStart = frameN  # exact frame index
            resp_train_DL.tStart = t  # local t and not account for scr refresh
            resp_train_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(resp_train_DL, 'tStartRefresh')  # time at next scr refresh
            resp_train_DL.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(resp_train_DL.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(resp_train_DL.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if resp_train_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > resp_train_DL.tStartRefresh + 4-frameTolerance:
                # keep track of stop time/frame for later
                resp_train_DL.tStop = t  # not accounting for scr refresh
                resp_train_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(resp_train_DL, 'tStopRefresh')  # time at next scr refresh
                resp_train_DL.status = FINISHED
        if resp_train_DL.status == STARTED and not waitOnFlip:
            theseKeys = resp_train_DL.getKeys(keyList=['a', 'l'], waitRelease=False)
            _resp_train_DL_allKeys.extend(theseKeys)
            if len(_resp_train_DL_allKeys):
                resp_train_DL.keys = _resp_train_DL_allKeys[-1].name  # just the last key pressed
                resp_train_DL.rt = _resp_train_DL_allKeys[-1].rt
                # was this correct?
                if (resp_train_DL.keys == str(key_corr)) or (resp_train_DL.keys == key_corr):
                    resp_train_DL.corr = 1
                else:
                    resp_train_DL.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Train_DLComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Train_DL"-------
    for thisComponent in Train_DLComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    Seq_Train_DL.addData('fixation_cross_train_DL.started', fixation_cross_train_DL.tStartRefresh)
    Seq_Train_DL.addData('fixation_cross_train_DL.stopped', fixation_cross_train_DL.tStopRefresh)
    Seq_Train_DL.addData('text_train_DL.started', text_train_DL.tStartRefresh)
    Seq_Train_DL.addData('text_train_DL.stopped', text_train_DL.tStopRefresh)
    # check responses
    if resp_train_DL.keys in ['', [], None]:  # No response was made
        resp_train_DL.keys = None
        # was no response the correct answer?!
        if str(key_corr).lower() == 'none':
           resp_train_DL.corr = 1;  # correct non-response
        else:
           resp_train_DL.corr = 0;  # failed to respond (incorrectly)
    # store data for Seq_Train_DL (TrialHandler)
    Seq_Train_DL.addData('resp_train_DL.keys',resp_train_DL.keys)
    Seq_Train_DL.addData('resp_train_DL.corr', resp_train_DL.corr)
    if resp_train_DL.keys != None:  # we had a response
        Seq_Train_DL.addData('resp_train_DL.rt', resp_train_DL.rt)
    Seq_Train_DL.addData('resp_train_DL.started', resp_train_DL.tStartRefresh)
    Seq_Train_DL.addData('resp_train_DL.stopped', resp_train_DL.tStopRefresh)
    
    # ------Prepare to start Routine "Feedback_Train_DL"-------
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    if not resp_train_DL.keys :
        msg="Demorou muito!"
    elif resp_train_DL.corr == 1:
        msg="Acertou!"
    else:
        msg="Errou"
    feedback_text_train_DL.setText(msg)
    # keep track of which components have finished
    Feedback_Train_DLComponents = [feedback_text_train_DL]
    for thisComponent in Feedback_Train_DLComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Feedback_Train_DLClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Feedback_Train_DL"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = Feedback_Train_DLClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Feedback_Train_DLClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *feedback_text_train_DL* updates
        if feedback_text_train_DL.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            feedback_text_train_DL.frameNStart = frameN  # exact frame index
            feedback_text_train_DL.tStart = t  # local t and not account for scr refresh
            feedback_text_train_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(feedback_text_train_DL, 'tStartRefresh')  # time at next scr refresh
            feedback_text_train_DL.setAutoDraw(True)
        if feedback_text_train_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > feedback_text_train_DL.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                feedback_text_train_DL.tStop = t  # not accounting for scr refresh
                feedback_text_train_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(feedback_text_train_DL, 'tStopRefresh')  # time at next scr refresh
                feedback_text_train_DL.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Feedback_Train_DLComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Feedback_Train_DL"-------
    for thisComponent in Feedback_Train_DLComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    respcorrs.append(resp_train_DL.corr) 
    
    if len(respcorrs) % 20 == 0:
        seq = respcorrs[20*(int(len(respcorrs) / 20) - 1) : ] 
        if len(list(filter(lambda x: x==1, seq))) > 12:
            Seq_Train_DL.finished = True
    Seq_Train_DL.addData('feedback_text_train_DL.started', feedback_text_train_DL.tStartRefresh)
    Seq_Train_DL.addData('feedback_text_train_DL.stopped', feedback_text_train_DL.tStopRefresh)
    
    # set up handler to look after randomisation of conditions etc
    Train_DL_Arrow_Rep = data.TrialHandler(nReps=2, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='Train_DL_Arrow_Rep')
    thisExp.addLoop(Train_DL_Arrow_Rep)  # add the loop to the experiment
    thisTrain_DL_Arrow_Rep = Train_DL_Arrow_Rep.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrain_DL_Arrow_Rep.rgb)
    if thisTrain_DL_Arrow_Rep != None:
        for paramName in thisTrain_DL_Arrow_Rep:
            exec('{} = thisTrain_DL_Arrow_Rep[paramName]'.format(paramName))
    
    for thisTrain_DL_Arrow_Rep in Train_DL_Arrow_Rep:
        currentLoop = Train_DL_Arrow_Rep
        # abbreviate parameter names if possible (e.g. rgb = thisTrain_DL_Arrow_Rep.rgb)
        if thisTrain_DL_Arrow_Rep != None:
            for paramName in thisTrain_DL_Arrow_Rep:
                exec('{} = thisTrain_DL_Arrow_Rep[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "Train_DL_Arrow"-------
        continueRoutine = True
        routineTimer.add(5.350000)
        # update component parameters for each repeat
        if Seq_Train_DL.thisTrialN % 15 != 0:
            continueRoutine = False
        arrow_angle = 90 * (round(np.random.rand()) * 2 - 1)
        thisExp.addData('train_DL_arrow_angle', arrow_angle)
        
        if keys_resp.index('a')==0 and arrow_angle == 90:
            key_corr = 'a' 
        elif keys_resp.index('l')==1 and arrow_angle == -90:
            key_corr = 'l'
        elif keys_resp.index('l')==0 and arrow_angle == 90:
            key_corr = 'l' 
        elif keys_resp.index('a')==1 and arrow_angle == -90:
            key_corr = 'a'
        arrow_image_train_DL.setOri(arrow_angle)
        resp_train_DL_arrow.keys = []
        resp_train_DL_arrow.rt = []
        _resp_train_DL_arrow_allKeys = []
        # keep track of which components have finished
        Train_DL_ArrowComponents = [fixation_cross_train_DL_2, arrow_image_train_DL, train_DL_text_arrow, resp_train_DL_arrow]
        for thisComponent in Train_DL_ArrowComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        Train_DL_ArrowClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "Train_DL_Arrow"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = Train_DL_ArrowClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=Train_DL_ArrowClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_train_DL_2* updates
            if fixation_cross_train_DL_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_train_DL_2.frameNStart = frameN  # exact frame index
                fixation_cross_train_DL_2.tStart = t  # local t and not account for scr refresh
                fixation_cross_train_DL_2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_train_DL_2, 'tStartRefresh')  # time at next scr refresh
                fixation_cross_train_DL_2.setAutoDraw(True)
            if fixation_cross_train_DL_2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_train_DL_2.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_train_DL_2.tStop = t  # not accounting for scr refresh
                    fixation_cross_train_DL_2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(fixation_cross_train_DL_2, 'tStopRefresh')  # time at next scr refresh
                    fixation_cross_train_DL_2.setAutoDraw(False)
            
            # *arrow_image_train_DL* updates
            if arrow_image_train_DL.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
                # keep track of start time/frame for later
                arrow_image_train_DL.frameNStart = frameN  # exact frame index
                arrow_image_train_DL.tStart = t  # local t and not account for scr refresh
                arrow_image_train_DL.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arrow_image_train_DL, 'tStartRefresh')  # time at next scr refresh
                arrow_image_train_DL.setAutoDraw(True)
            if arrow_image_train_DL.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arrow_image_train_DL.tStartRefresh + .350-frameTolerance:
                    # keep track of stop time/frame for later
                    arrow_image_train_DL.tStop = t  # not accounting for scr refresh
                    arrow_image_train_DL.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(arrow_image_train_DL, 'tStopRefresh')  # time at next scr refresh
                    arrow_image_train_DL.setAutoDraw(False)
            
            # *train_DL_text_arrow* updates
            if train_DL_text_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                train_DL_text_arrow.frameNStart = frameN  # exact frame index
                train_DL_text_arrow.tStart = t  # local t and not account for scr refresh
                train_DL_text_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(train_DL_text_arrow, 'tStartRefresh')  # time at next scr refresh
                train_DL_text_arrow.setAutoDraw(True)
            if train_DL_text_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > train_DL_text_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    train_DL_text_arrow.tStop = t  # not accounting for scr refresh
                    train_DL_text_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(train_DL_text_arrow, 'tStopRefresh')  # time at next scr refresh
                    train_DL_text_arrow.setAutoDraw(False)
            
            # *resp_train_DL_arrow* updates
            waitOnFlip = False
            if resp_train_DL_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                resp_train_DL_arrow.frameNStart = frameN  # exact frame index
                resp_train_DL_arrow.tStart = t  # local t and not account for scr refresh
                resp_train_DL_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_train_DL_arrow, 'tStartRefresh')  # time at next scr refresh
                resp_train_DL_arrow.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(resp_train_DL_arrow.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(resp_train_DL_arrow.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if resp_train_DL_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_train_DL_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_train_DL_arrow.tStop = t  # not accounting for scr refresh
                    resp_train_DL_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_train_DL_arrow, 'tStopRefresh')  # time at next scr refresh
                    resp_train_DL_arrow.status = FINISHED
            if resp_train_DL_arrow.status == STARTED and not waitOnFlip:
                theseKeys = resp_train_DL_arrow.getKeys(keyList=['a', 'l'], waitRelease=False)
                _resp_train_DL_arrow_allKeys.extend(theseKeys)
                if len(_resp_train_DL_arrow_allKeys):
                    resp_train_DL_arrow.keys = _resp_train_DL_arrow_allKeys[-1].name  # just the last key pressed
                    resp_train_DL_arrow.rt = _resp_train_DL_arrow_allKeys[-1].rt
                    # was this correct?
                    if (resp_train_DL_arrow.keys == str(key_corr)) or (resp_train_DL_arrow.keys == key_corr):
                        resp_train_DL_arrow.corr = 1
                    else:
                        resp_train_DL_arrow.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in Train_DL_ArrowComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Train_DL_Arrow"-------
        for thisComponent in Train_DL_ArrowComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        Train_DL_Arrow_Rep.addData('fixation_cross_train_DL_2.started', fixation_cross_train_DL_2.tStartRefresh)
        Train_DL_Arrow_Rep.addData('fixation_cross_train_DL_2.stopped', fixation_cross_train_DL_2.tStopRefresh)
        Train_DL_Arrow_Rep.addData('arrow_image_train_DL.started', arrow_image_train_DL.tStartRefresh)
        Train_DL_Arrow_Rep.addData('arrow_image_train_DL.stopped', arrow_image_train_DL.tStopRefresh)
        Train_DL_Arrow_Rep.addData('train_DL_text_arrow.started', train_DL_text_arrow.tStartRefresh)
        Train_DL_Arrow_Rep.addData('train_DL_text_arrow.stopped', train_DL_text_arrow.tStopRefresh)
        # check responses
        if resp_train_DL_arrow.keys in ['', [], None]:  # No response was made
            resp_train_DL_arrow.keys = None
            # was no response the correct answer?!
            if str(key_corr).lower() == 'none':
               resp_train_DL_arrow.corr = 1;  # correct non-response
            else:
               resp_train_DL_arrow.corr = 0;  # failed to respond (incorrectly)
        # store data for Train_DL_Arrow_Rep (TrialHandler)
        Train_DL_Arrow_Rep.addData('resp_train_DL_arrow.keys',resp_train_DL_arrow.keys)
        Train_DL_Arrow_Rep.addData('resp_train_DL_arrow.corr', resp_train_DL_arrow.corr)
        if resp_train_DL_arrow.keys != None:  # we had a response
            Train_DL_Arrow_Rep.addData('resp_train_DL_arrow.rt', resp_train_DL_arrow.rt)
        Train_DL_Arrow_Rep.addData('resp_train_DL_arrow.started', resp_train_DL_arrow.tStartRefresh)
        Train_DL_Arrow_Rep.addData('resp_train_DL_arrow.stopped', resp_train_DL_arrow.tStopRefresh)
        thisExp.nextEntry()
        
    # completed 2 repeats of 'Train_DL_Arrow_Rep'
    
    # get names of stimulus parameters
    if Train_DL_Arrow_Rep.trialList in ([], [None], None):
        params = []
    else:
        params = Train_DL_Arrow_Rep.trialList[0].keys()
    # save data for this loop
    Train_DL_Arrow_Rep.saveAsText(filename + 'Train_DL_Arrow_Rep.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 100 repeats of 'Seq_Train_DL'

# get names of stimulus parameters
if Seq_Train_DL.trialList in ([], [None], None):
    params = []
else:
    params = Seq_Train_DL.trialList[0].keys()
# save data for this loop
Seq_Train_DL.saveAsText(filename + 'Seq_Train_DL.csv', delim=',',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "Instructions_DL8"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_DL8.keys = []
resp_next_DL8.rt = []
_resp_next_DL8_allKeys = []
# keep track of which components have finished
Instructions_DL8Components = [intro_DL8, resp_next_DL8]
for thisComponent in Instructions_DL8Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_DL8Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_DL8"-------
while continueRoutine:
    # get current time
    t = Instructions_DL8Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_DL8Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_DL8* updates
    if intro_DL8.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_DL8.frameNStart = frameN  # exact frame index
        intro_DL8.tStart = t  # local t and not account for scr refresh
        intro_DL8.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_DL8, 'tStartRefresh')  # time at next scr refresh
        intro_DL8.setAutoDraw(True)
    
    # *resp_next_DL8* updates
    waitOnFlip = False
    if resp_next_DL8.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_DL8.frameNStart = frameN  # exact frame index
        resp_next_DL8.tStart = t  # local t and not account for scr refresh
        resp_next_DL8.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_DL8, 'tStartRefresh')  # time at next scr refresh
        resp_next_DL8.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_DL8.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_DL8.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_DL8.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_DL8.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_DL8_allKeys.extend(theseKeys)
        if len(_resp_next_DL8_allKeys):
            resp_next_DL8.keys = _resp_next_DL8_allKeys[-1].name  # just the last key pressed
            resp_next_DL8.rt = _resp_next_DL8_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_DL8Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_DL8"-------
for thisComponent in Instructions_DL8Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_DL8.started', intro_DL8.tStartRefresh)
thisExp.addData('intro_DL8.stopped', intro_DL8.tStopRefresh)
# check responses
if resp_next_DL8.keys in ['', [], None]:  # No response was made
    resp_next_DL8.keys = None
thisExp.addData('resp_next_DL8.keys',resp_next_DL8.keys)
if resp_next_DL8.keys != None:  # we had a response
    thisExp.addData('resp_next_DL8.rt', resp_next_DL8.rt)
thisExp.addData('resp_next_DL8.started', resp_next_DL8.tStartRefresh)
thisExp.addData('resp_next_DL8.stopped', resp_next_DL8.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_DL8" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
Seq_Test_DL = data.TrialHandler(nReps=1, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Estimulos.xlsx', selection=':180'),
    seed=None, name='Seq_Test_DL')
thisExp.addLoop(Seq_Test_DL)  # add the loop to the experiment
thisSeq_Test_DL = Seq_Test_DL.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSeq_Test_DL.rgb)
if thisSeq_Test_DL != None:
    for paramName in thisSeq_Test_DL:
        exec('{} = thisSeq_Test_DL[paramName]'.format(paramName))

for thisSeq_Test_DL in Seq_Test_DL:
    currentLoop = Seq_Test_DL
    # abbreviate parameter names if possible (e.g. rgb = thisSeq_Test_DL.rgb)
    if thisSeq_Test_DL != None:
        for paramName in thisSeq_Test_DL:
            exec('{} = thisSeq_Test_DL[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Test_DL"-------
    continueRoutine = True
    routineTimer.add(5.000000)
    # update component parameters for each repeat
    if keys_resp.index('a')==0 and RESPOSTA_DL_TESTE==1:
        key_corr = 'a' 
    elif keys_resp.index('l')==1 and RESPOSTA_DL_TESTE==0:
        key_corr = 'l'
    elif keys_resp.index('l')==0 and RESPOSTA_DL_TESTE==1:
        key_corr = 'l' 
    elif keys_resp.index('a')==1 and RESPOSTA_DL_TESTE==0:
        key_corr = 'a'
    text_test_DL.setText(VERBO_DL_TESTE)
    resp_test_DL.keys = []
    resp_test_DL.rt = []
    _resp_test_DL_allKeys = []
    # keep track of which components have finished
    Test_DLComponents = [fixation_cross_test_DL, text_test_DL, resp_test_DL]
    for thisComponent in Test_DLComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Test_DLClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Test_DL"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = Test_DLClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Test_DLClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_test_DL* updates
        if fixation_cross_test_DL.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_test_DL.frameNStart = frameN  # exact frame index
            fixation_cross_test_DL.tStart = t  # local t and not account for scr refresh
            fixation_cross_test_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_test_DL, 'tStartRefresh')  # time at next scr refresh
            fixation_cross_test_DL.setAutoDraw(True)
        if fixation_cross_test_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_test_DL.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_test_DL.tStop = t  # not accounting for scr refresh
                fixation_cross_test_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(fixation_cross_test_DL, 'tStopRefresh')  # time at next scr refresh
                fixation_cross_test_DL.setAutoDraw(False)
        
        # *text_test_DL* updates
        if text_test_DL.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            text_test_DL.frameNStart = frameN  # exact frame index
            text_test_DL.tStart = t  # local t and not account for scr refresh
            text_test_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_test_DL, 'tStartRefresh')  # time at next scr refresh
            text_test_DL.setAutoDraw(True)
        if text_test_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > text_test_DL.tStartRefresh + 0.350-frameTolerance:
                # keep track of stop time/frame for later
                text_test_DL.tStop = t  # not accounting for scr refresh
                text_test_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(text_test_DL, 'tStopRefresh')  # time at next scr refresh
                text_test_DL.setAutoDraw(False)
        
        # *resp_test_DL* updates
        waitOnFlip = False
        if resp_test_DL.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            resp_test_DL.frameNStart = frameN  # exact frame index
            resp_test_DL.tStart = t  # local t and not account for scr refresh
            resp_test_DL.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(resp_test_DL, 'tStartRefresh')  # time at next scr refresh
            resp_test_DL.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(resp_test_DL.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(resp_test_DL.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if resp_test_DL.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > resp_test_DL.tStartRefresh + 4-frameTolerance:
                # keep track of stop time/frame for later
                resp_test_DL.tStop = t  # not accounting for scr refresh
                resp_test_DL.frameNStop = frameN  # exact frame index
                win.timeOnFlip(resp_test_DL, 'tStopRefresh')  # time at next scr refresh
                resp_test_DL.status = FINISHED
        if resp_test_DL.status == STARTED and not waitOnFlip:
            theseKeys = resp_test_DL.getKeys(keyList=['a', 'l'], waitRelease=False)
            _resp_test_DL_allKeys.extend(theseKeys)
            if len(_resp_test_DL_allKeys):
                resp_test_DL.keys = _resp_test_DL_allKeys[-1].name  # just the last key pressed
                resp_test_DL.rt = _resp_test_DL_allKeys[-1].rt
                # was this correct?
                if (resp_test_DL.keys == str(key_corr)) or (resp_test_DL.keys == key_corr):
                    resp_test_DL.corr = 1
                else:
                    resp_test_DL.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Test_DLComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Test_DL"-------
    for thisComponent in Test_DLComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    Seq_Test_DL.addData('fixation_cross_test_DL.started', fixation_cross_test_DL.tStartRefresh)
    Seq_Test_DL.addData('fixation_cross_test_DL.stopped', fixation_cross_test_DL.tStopRefresh)
    Seq_Test_DL.addData('text_test_DL.started', text_test_DL.tStartRefresh)
    Seq_Test_DL.addData('text_test_DL.stopped', text_test_DL.tStopRefresh)
    # check responses
    if resp_test_DL.keys in ['', [], None]:  # No response was made
        resp_test_DL.keys = None
        # was no response the correct answer?!
        if str(key_corr).lower() == 'none':
           resp_test_DL.corr = 1;  # correct non-response
        else:
           resp_test_DL.corr = 0;  # failed to respond (incorrectly)
    # store data for Seq_Test_DL (TrialHandler)
    Seq_Test_DL.addData('resp_test_DL.keys',resp_test_DL.keys)
    Seq_Test_DL.addData('resp_test_DL.corr', resp_test_DL.corr)
    if resp_test_DL.keys != None:  # we had a response
        Seq_Test_DL.addData('resp_test_DL.rt', resp_test_DL.rt)
    Seq_Test_DL.addData('resp_test_DL.started', resp_test_DL.tStartRefresh)
    Seq_Test_DL.addData('resp_test_DL.stopped', resp_test_DL.tStopRefresh)
    
    # set up handler to look after randomisation of conditions etc
    Test_DL_Arrow_Rep = data.TrialHandler(nReps=2, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='Test_DL_Arrow_Rep')
    thisExp.addLoop(Test_DL_Arrow_Rep)  # add the loop to the experiment
    thisTest_DL_Arrow_Rep = Test_DL_Arrow_Rep.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTest_DL_Arrow_Rep.rgb)
    if thisTest_DL_Arrow_Rep != None:
        for paramName in thisTest_DL_Arrow_Rep:
            exec('{} = thisTest_DL_Arrow_Rep[paramName]'.format(paramName))
    
    for thisTest_DL_Arrow_Rep in Test_DL_Arrow_Rep:
        currentLoop = Test_DL_Arrow_Rep
        # abbreviate parameter names if possible (e.g. rgb = thisTest_DL_Arrow_Rep.rgb)
        if thisTest_DL_Arrow_Rep != None:
            for paramName in thisTest_DL_Arrow_Rep:
                exec('{} = thisTest_DL_Arrow_Rep[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "Test_DL_Arrow"-------
        continueRoutine = True
        routineTimer.add(5.350000)
        # update component parameters for each repeat
        if Seq_Test_DL.thisTrialN % 15 != 0:
            continueRoutine = False
        arrow_angle = 90 * (round(np.random.rand()) * 2 - 1)
        thisExp.addData('test_DL_arrow_angle', arrow_angle)
        
        if keys_resp.index('a')==0 and arrow_angle == 90:
            key_corr = 'a' 
        elif keys_resp.index('l')==1 and arrow_angle == -90:
            key_corr = 'l'
        elif keys_resp.index('l')==0 and arrow_angle == 90:
            key_corr = 'l' 
        elif keys_resp.index('a')==1 and arrow_angle == -90:
            key_corr = 'a'
        arrow_image_test_DL.setOri(arrow_angle)
        resp_test_DL_arrow.keys = []
        resp_test_DL_arrow.rt = []
        _resp_test_DL_arrow_allKeys = []
        # keep track of which components have finished
        Test_DL_ArrowComponents = [fixation_cross_test_DL_2, arrow_image_test_DL, test_DL_text_arrow, resp_test_DL_arrow]
        for thisComponent in Test_DL_ArrowComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        Test_DL_ArrowClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "Test_DL_Arrow"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = Test_DL_ArrowClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=Test_DL_ArrowClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_test_DL_2* updates
            if fixation_cross_test_DL_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_test_DL_2.frameNStart = frameN  # exact frame index
                fixation_cross_test_DL_2.tStart = t  # local t and not account for scr refresh
                fixation_cross_test_DL_2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_test_DL_2, 'tStartRefresh')  # time at next scr refresh
                fixation_cross_test_DL_2.setAutoDraw(True)
            if fixation_cross_test_DL_2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_test_DL_2.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_test_DL_2.tStop = t  # not accounting for scr refresh
                    fixation_cross_test_DL_2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(fixation_cross_test_DL_2, 'tStopRefresh')  # time at next scr refresh
                    fixation_cross_test_DL_2.setAutoDraw(False)
            
            # *arrow_image_test_DL* updates
            if arrow_image_test_DL.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
                # keep track of start time/frame for later
                arrow_image_test_DL.frameNStart = frameN  # exact frame index
                arrow_image_test_DL.tStart = t  # local t and not account for scr refresh
                arrow_image_test_DL.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arrow_image_test_DL, 'tStartRefresh')  # time at next scr refresh
                arrow_image_test_DL.setAutoDraw(True)
            if arrow_image_test_DL.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arrow_image_test_DL.tStartRefresh + .350-frameTolerance:
                    # keep track of stop time/frame for later
                    arrow_image_test_DL.tStop = t  # not accounting for scr refresh
                    arrow_image_test_DL.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(arrow_image_test_DL, 'tStopRefresh')  # time at next scr refresh
                    arrow_image_test_DL.setAutoDraw(False)
            
            # *test_DL_text_arrow* updates
            if test_DL_text_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                test_DL_text_arrow.frameNStart = frameN  # exact frame index
                test_DL_text_arrow.tStart = t  # local t and not account for scr refresh
                test_DL_text_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(test_DL_text_arrow, 'tStartRefresh')  # time at next scr refresh
                test_DL_text_arrow.setAutoDraw(True)
            if test_DL_text_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > test_DL_text_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    test_DL_text_arrow.tStop = t  # not accounting for scr refresh
                    test_DL_text_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(test_DL_text_arrow, 'tStopRefresh')  # time at next scr refresh
                    test_DL_text_arrow.setAutoDraw(False)
            
            # *resp_test_DL_arrow* updates
            waitOnFlip = False
            if resp_test_DL_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                resp_test_DL_arrow.frameNStart = frameN  # exact frame index
                resp_test_DL_arrow.tStart = t  # local t and not account for scr refresh
                resp_test_DL_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_test_DL_arrow, 'tStartRefresh')  # time at next scr refresh
                resp_test_DL_arrow.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(resp_test_DL_arrow.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(resp_test_DL_arrow.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if resp_test_DL_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_test_DL_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_test_DL_arrow.tStop = t  # not accounting for scr refresh
                    resp_test_DL_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_test_DL_arrow, 'tStopRefresh')  # time at next scr refresh
                    resp_test_DL_arrow.status = FINISHED
            if resp_test_DL_arrow.status == STARTED and not waitOnFlip:
                theseKeys = resp_test_DL_arrow.getKeys(keyList=['a', 'l'], waitRelease=False)
                _resp_test_DL_arrow_allKeys.extend(theseKeys)
                if len(_resp_test_DL_arrow_allKeys):
                    resp_test_DL_arrow.keys = _resp_test_DL_arrow_allKeys[-1].name  # just the last key pressed
                    resp_test_DL_arrow.rt = _resp_test_DL_arrow_allKeys[-1].rt
                    # was this correct?
                    if (resp_test_DL_arrow.keys == str(key_corr)) or (resp_test_DL_arrow.keys == key_corr):
                        resp_test_DL_arrow.corr = 1
                    else:
                        resp_test_DL_arrow.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in Test_DL_ArrowComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Test_DL_Arrow"-------
        for thisComponent in Test_DL_ArrowComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        Test_DL_Arrow_Rep.addData('fixation_cross_test_DL_2.started', fixation_cross_test_DL_2.tStartRefresh)
        Test_DL_Arrow_Rep.addData('fixation_cross_test_DL_2.stopped', fixation_cross_test_DL_2.tStopRefresh)
        Test_DL_Arrow_Rep.addData('arrow_image_test_DL.started', arrow_image_test_DL.tStartRefresh)
        Test_DL_Arrow_Rep.addData('arrow_image_test_DL.stopped', arrow_image_test_DL.tStopRefresh)
        Test_DL_Arrow_Rep.addData('test_DL_text_arrow.started', test_DL_text_arrow.tStartRefresh)
        Test_DL_Arrow_Rep.addData('test_DL_text_arrow.stopped', test_DL_text_arrow.tStopRefresh)
        # check responses
        if resp_test_DL_arrow.keys in ['', [], None]:  # No response was made
            resp_test_DL_arrow.keys = None
            # was no response the correct answer?!
            if str(key_corr).lower() == 'none':
               resp_test_DL_arrow.corr = 1;  # correct non-response
            else:
               resp_test_DL_arrow.corr = 0;  # failed to respond (incorrectly)
        # store data for Test_DL_Arrow_Rep (TrialHandler)
        Test_DL_Arrow_Rep.addData('resp_test_DL_arrow.keys',resp_test_DL_arrow.keys)
        Test_DL_Arrow_Rep.addData('resp_test_DL_arrow.corr', resp_test_DL_arrow.corr)
        if resp_test_DL_arrow.keys != None:  # we had a response
            Test_DL_Arrow_Rep.addData('resp_test_DL_arrow.rt', resp_test_DL_arrow.rt)
        Test_DL_Arrow_Rep.addData('resp_test_DL_arrow.started', resp_test_DL_arrow.tStartRefresh)
        Test_DL_Arrow_Rep.addData('resp_test_DL_arrow.stopped', resp_test_DL_arrow.tStopRefresh)
        thisExp.nextEntry()
        
    # completed 2 repeats of 'Test_DL_Arrow_Rep'
    
    # get names of stimulus parameters
    if Test_DL_Arrow_Rep.trialList in ([], [None], None):
        params = []
    else:
        params = Test_DL_Arrow_Rep.trialList[0].keys()
    # save data for this loop
    Test_DL_Arrow_Rep.saveAsText(filename + 'Test_DL_Arrow_Rep.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 1 repeats of 'Seq_Test_DL'

# get names of stimulus parameters
if Seq_Test_DL.trialList in ([], [None], None):
    params = []
else:
    params = Seq_Test_DL.trialList[0].keys()
# save data for this loop
Seq_Test_DL.saveAsText(filename + 'Seq_Test_DL.csv', delim=',',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "Instructions_JS1_1"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS1_1.keys = []
resp_next_JS1_1.rt = []
_resp_next_JS1_1_allKeys = []
# keep track of which components have finished
Instructions_JS1_1Components = [JS1_1, intro_JS1_1, next_JS1_1, resp_next_JS1_1]
for thisComponent in Instructions_JS1_1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS1_1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS1_1"-------
while continueRoutine:
    # get current time
    t = Instructions_JS1_1Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS1_1Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *JS1_1* updates
    if JS1_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        JS1_1.frameNStart = frameN  # exact frame index
        JS1_1.tStart = t  # local t and not account for scr refresh
        JS1_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(JS1_1, 'tStartRefresh')  # time at next scr refresh
        JS1_1.setAutoDraw(True)
    if JS1_1.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > JS1_1.tStartRefresh + 1.0-frameTolerance:
            # keep track of stop time/frame for later
            JS1_1.tStop = t  # not accounting for scr refresh
            JS1_1.frameNStop = frameN  # exact frame index
            win.timeOnFlip(JS1_1, 'tStopRefresh')  # time at next scr refresh
            JS1_1.setAutoDraw(False)
    
    # *intro_JS1_1* updates
    if intro_JS1_1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        intro_JS1_1.frameNStart = frameN  # exact frame index
        intro_JS1_1.tStart = t  # local t and not account for scr refresh
        intro_JS1_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS1_1, 'tStartRefresh')  # time at next scr refresh
        intro_JS1_1.setAutoDraw(True)
    
    # *next_JS1_1* updates
    if next_JS1_1.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
        # keep track of start time/frame for later
        next_JS1_1.frameNStart = frameN  # exact frame index
        next_JS1_1.tStart = t  # local t and not account for scr refresh
        next_JS1_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_JS1_1, 'tStartRefresh')  # time at next scr refresh
        next_JS1_1.setAutoDraw(True)
    
    # *resp_next_JS1_1* updates
    waitOnFlip = False
    if resp_next_JS1_1.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS1_1.frameNStart = frameN  # exact frame index
        resp_next_JS1_1.tStart = t  # local t and not account for scr refresh
        resp_next_JS1_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS1_1, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS1_1.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS1_1.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS1_1.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS1_1.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS1_1.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS1_1_allKeys.extend(theseKeys)
        if len(_resp_next_JS1_1_allKeys):
            resp_next_JS1_1.keys = _resp_next_JS1_1_allKeys[-1].name  # just the last key pressed
            resp_next_JS1_1.rt = _resp_next_JS1_1_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS1_1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS1_1"-------
for thisComponent in Instructions_JS1_1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('JS1_1.started', JS1_1.tStartRefresh)
thisExp.addData('JS1_1.stopped', JS1_1.tStopRefresh)
thisExp.addData('intro_JS1_1.started', intro_JS1_1.tStartRefresh)
thisExp.addData('intro_JS1_1.stopped', intro_JS1_1.tStopRefresh)
thisExp.addData('next_JS1_1.started', next_JS1_1.tStartRefresh)
thisExp.addData('next_JS1_1.stopped', next_JS1_1.tStopRefresh)
# check responses
if resp_next_JS1_1.keys in ['', [], None]:  # No response was made
    resp_next_JS1_1.keys = None
thisExp.addData('resp_next_JS1_1.keys',resp_next_JS1_1.keys)
if resp_next_JS1_1.keys != None:  # we had a response
    thisExp.addData('resp_next_JS1_1.rt', resp_next_JS1_1.rt)
thisExp.addData('resp_next_JS1_1.started', resp_next_JS1_1.tStartRefresh)
thisExp.addData('resp_next_JS1_1.stopped', resp_next_JS1_1.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS1_1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_JS1_2"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS1_2.keys = []
resp_next_JS1_2.rt = []
_resp_next_JS1_2_allKeys = []
# keep track of which components have finished
Instructions_JS1_2Components = [intro_JS1_2, next_JS1_2, resp_next_JS1_2]
for thisComponent in Instructions_JS1_2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS1_2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS1_2"-------
while continueRoutine:
    # get current time
    t = Instructions_JS1_2Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS1_2Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS1_2* updates
    if intro_JS1_2.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS1_2.frameNStart = frameN  # exact frame index
        intro_JS1_2.tStart = t  # local t and not account for scr refresh
        intro_JS1_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS1_2, 'tStartRefresh')  # time at next scr refresh
        intro_JS1_2.setAutoDraw(True)
    
    # *next_JS1_2* updates
    if next_JS1_2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_JS1_2.frameNStart = frameN  # exact frame index
        next_JS1_2.tStart = t  # local t and not account for scr refresh
        next_JS1_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_JS1_2, 'tStartRefresh')  # time at next scr refresh
        next_JS1_2.setAutoDraw(True)
    
    # *resp_next_JS1_2* updates
    waitOnFlip = False
    if resp_next_JS1_2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS1_2.frameNStart = frameN  # exact frame index
        resp_next_JS1_2.tStart = t  # local t and not account for scr refresh
        resp_next_JS1_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS1_2, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS1_2.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS1_2.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS1_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS1_2.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS1_2.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS1_2_allKeys.extend(theseKeys)
        if len(_resp_next_JS1_2_allKeys):
            resp_next_JS1_2.keys = _resp_next_JS1_2_allKeys[-1].name  # just the last key pressed
            resp_next_JS1_2.rt = _resp_next_JS1_2_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS1_2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS1_2"-------
for thisComponent in Instructions_JS1_2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS1_2.started', intro_JS1_2.tStartRefresh)
thisExp.addData('intro_JS1_2.stopped', intro_JS1_2.tStopRefresh)
thisExp.addData('next_JS1_2.started', next_JS1_2.tStartRefresh)
thisExp.addData('next_JS1_2.stopped', next_JS1_2.tStopRefresh)
# check responses
if resp_next_JS1_2.keys in ['', [], None]:  # No response was made
    resp_next_JS1_2.keys = None
thisExp.addData('resp_next_JS1_2.keys',resp_next_JS1_2.keys)
if resp_next_JS1_2.keys != None:  # we had a response
    thisExp.addData('resp_next_JS1_2.rt', resp_next_JS1_2.rt)
thisExp.addData('resp_next_JS1_2.started', resp_next_JS1_2.tStartRefresh)
thisExp.addData('resp_next_JS1_2.stopped', resp_next_JS1_2.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS1_2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_JS1_3"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS1_3.keys = []
resp_next_JS1_3.rt = []
_resp_next_JS1_3_allKeys = []
# keep track of which components have finished
Instructions_JS1_3Components = [intro_JS1_3, next_JS1_3, resp_next_JS1_3]
for thisComponent in Instructions_JS1_3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS1_3Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS1_3"-------
while continueRoutine:
    # get current time
    t = Instructions_JS1_3Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS1_3Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS1_3* updates
    if intro_JS1_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS1_3.frameNStart = frameN  # exact frame index
        intro_JS1_3.tStart = t  # local t and not account for scr refresh
        intro_JS1_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS1_3, 'tStartRefresh')  # time at next scr refresh
        intro_JS1_3.setAutoDraw(True)
    
    # *next_JS1_3* updates
    if next_JS1_3.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_JS1_3.frameNStart = frameN  # exact frame index
        next_JS1_3.tStart = t  # local t and not account for scr refresh
        next_JS1_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_JS1_3, 'tStartRefresh')  # time at next scr refresh
        next_JS1_3.setAutoDraw(True)
    
    # *resp_next_JS1_3* updates
    waitOnFlip = False
    if resp_next_JS1_3.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS1_3.frameNStart = frameN  # exact frame index
        resp_next_JS1_3.tStart = t  # local t and not account for scr refresh
        resp_next_JS1_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS1_3, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS1_3.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS1_3.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS1_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS1_3.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS1_3.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS1_3_allKeys.extend(theseKeys)
        if len(_resp_next_JS1_3_allKeys):
            resp_next_JS1_3.keys = _resp_next_JS1_3_allKeys[-1].name  # just the last key pressed
            resp_next_JS1_3.rt = _resp_next_JS1_3_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS1_3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS1_3"-------
for thisComponent in Instructions_JS1_3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS1_3.started', intro_JS1_3.tStartRefresh)
thisExp.addData('intro_JS1_3.stopped', intro_JS1_3.tStopRefresh)
thisExp.addData('next_JS1_3.started', next_JS1_3.tStartRefresh)
thisExp.addData('next_JS1_3.stopped', next_JS1_3.tStopRefresh)
# check responses
if resp_next_JS1_3.keys in ['', [], None]:  # No response was made
    resp_next_JS1_3.keys = None
thisExp.addData('resp_next_JS1_3.keys',resp_next_JS1_3.keys)
if resp_next_JS1_3.keys != None:  # we had a response
    thisExp.addData('resp_next_JS1_3.rt', resp_next_JS1_3.rt)
thisExp.addData('resp_next_JS1_3.started', resp_next_JS1_3.tStartRefresh)
thisExp.addData('resp_next_JS1_3.stopped', resp_next_JS1_3.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS1_3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_JS1_4"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS1_4.keys = []
resp_next_JS1_4.rt = []
_resp_next_JS1_4_allKeys = []
# keep track of which components have finished
Instructions_JS1_4Components = [intro_JS1_4, resp_next_JS1_4]
for thisComponent in Instructions_JS1_4Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS1_4Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS1_4"-------
while continueRoutine:
    # get current time
    t = Instructions_JS1_4Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS1_4Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS1_4* updates
    if intro_JS1_4.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS1_4.frameNStart = frameN  # exact frame index
        intro_JS1_4.tStart = t  # local t and not account for scr refresh
        intro_JS1_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS1_4, 'tStartRefresh')  # time at next scr refresh
        intro_JS1_4.setAutoDraw(True)
    
    # *resp_next_JS1_4* updates
    waitOnFlip = False
    if resp_next_JS1_4.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS1_4.frameNStart = frameN  # exact frame index
        resp_next_JS1_4.tStart = t  # local t and not account for scr refresh
        resp_next_JS1_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS1_4, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS1_4.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS1_4.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS1_4.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS1_4.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS1_4.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS1_4_allKeys.extend(theseKeys)
        if len(_resp_next_JS1_4_allKeys):
            resp_next_JS1_4.keys = _resp_next_JS1_4_allKeys[-1].name  # just the last key pressed
            resp_next_JS1_4.rt = _resp_next_JS1_4_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS1_4Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS1_4"-------
for thisComponent in Instructions_JS1_4Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS1_4.started', intro_JS1_4.tStartRefresh)
thisExp.addData('intro_JS1_4.stopped', intro_JS1_4.tStopRefresh)
# check responses
if resp_next_JS1_4.keys in ['', [], None]:  # No response was made
    resp_next_JS1_4.keys = None
thisExp.addData('resp_next_JS1_4.keys',resp_next_JS1_4.keys)
if resp_next_JS1_4.keys != None:  # we had a response
    thisExp.addData('resp_next_JS1_4.rt', resp_next_JS1_4.rt)
thisExp.addData('resp_next_JS1_4.started', resp_next_JS1_4.tStartRefresh)
thisExp.addData('resp_next_JS1_4.stopped', resp_next_JS1_4.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS1_4" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
Seq_Train_JS1 = data.TrialHandler(nReps=100, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Estimulos.xlsx', selection=':20'),
    seed=None, name='Seq_Train_JS1')
thisExp.addLoop(Seq_Train_JS1)  # add the loop to the experiment
thisSeq_Train_JS1 = Seq_Train_JS1.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSeq_Train_JS1.rgb)
if thisSeq_Train_JS1 != None:
    for paramName in thisSeq_Train_JS1:
        exec('{} = thisSeq_Train_JS1[paramName]'.format(paramName))

for thisSeq_Train_JS1 in Seq_Train_JS1:
    currentLoop = Seq_Train_JS1
    # abbreviate parameter names if possible (e.g. rgb = thisSeq_Train_JS1.rgb)
    if thisSeq_Train_JS1 != None:
        for paramName in thisSeq_Train_JS1:
            exec('{} = thisSeq_Train_JS1[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Train_JS1"-------
    continueRoutine = True
    # update component parameters for each repeat
    if keys_resp.index('a')==0 and RESPOSTA_JS1_TREINO==1:
        key_corr = 'a' 
    elif keys_resp.index('l')==1 and RESPOSTA_JS1_TREINO==0:
        key_corr = 'l'
    elif keys_resp.index('l')==0 and RESPOSTA_JS1_TREINO==1:
        key_corr = 'l' 
    elif keys_resp.index('a')==1 and RESPOSTA_JS1_TREINO==0:
        key_corr = 'a'
    text_train_JS1.setText(VERBO_JS1_TREINO)
    resp_train_JS1.keys = []
    resp_train_JS1.rt = []
    _resp_train_JS1_allKeys = []
    # keep track of which components have finished
    Train_JS1Components = [fixation_cross_train_JS1, text_train_JS1, resp_train_JS1]
    for thisComponent in Train_JS1Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Train_JS1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Train_JS1"-------
    while continueRoutine:
        # get current time
        t = Train_JS1Clock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Train_JS1Clock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_train_JS1* updates
        if fixation_cross_train_JS1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_train_JS1.frameNStart = frameN  # exact frame index
            fixation_cross_train_JS1.tStart = t  # local t and not account for scr refresh
            fixation_cross_train_JS1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_train_JS1, 'tStartRefresh')  # time at next scr refresh
            fixation_cross_train_JS1.setAutoDraw(True)
        if fixation_cross_train_JS1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_train_JS1.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_train_JS1.tStop = t  # not accounting for scr refresh
                fixation_cross_train_JS1.frameNStop = frameN  # exact frame index
                win.timeOnFlip(fixation_cross_train_JS1, 'tStopRefresh')  # time at next scr refresh
                fixation_cross_train_JS1.setAutoDraw(False)
        
        # *text_train_JS1* updates
        if text_train_JS1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            text_train_JS1.frameNStart = frameN  # exact frame index
            text_train_JS1.tStart = t  # local t and not account for scr refresh
            text_train_JS1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_train_JS1, 'tStartRefresh')  # time at next scr refresh
            text_train_JS1.setAutoDraw(True)
        
        # *resp_train_JS1* updates
        waitOnFlip = False
        if resp_train_JS1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            resp_train_JS1.frameNStart = frameN  # exact frame index
            resp_train_JS1.tStart = t  # local t and not account for scr refresh
            resp_train_JS1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(resp_train_JS1, 'tStartRefresh')  # time at next scr refresh
            resp_train_JS1.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(resp_train_JS1.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(resp_train_JS1.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if resp_train_JS1.status == STARTED and not waitOnFlip:
            theseKeys = resp_train_JS1.getKeys(keyList=['a', 'l'], waitRelease=False)
            _resp_train_JS1_allKeys.extend(theseKeys)
            if len(_resp_train_JS1_allKeys):
                resp_train_JS1.keys = _resp_train_JS1_allKeys[-1].name  # just the last key pressed
                resp_train_JS1.rt = _resp_train_JS1_allKeys[-1].rt
                # was this correct?
                if (resp_train_JS1.keys == str(key_corr)) or (resp_train_JS1.keys == key_corr):
                    resp_train_JS1.corr = 1
                else:
                    resp_train_JS1.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Train_JS1Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Train_JS1"-------
    for thisComponent in Train_JS1Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    Seq_Train_JS1.addData('fixation_cross_train_JS1.started', fixation_cross_train_JS1.tStartRefresh)
    Seq_Train_JS1.addData('fixation_cross_train_JS1.stopped', fixation_cross_train_JS1.tStopRefresh)
    Seq_Train_JS1.addData('text_train_JS1.started', text_train_JS1.tStartRefresh)
    Seq_Train_JS1.addData('text_train_JS1.stopped', text_train_JS1.tStopRefresh)
    # check responses
    if resp_train_JS1.keys in ['', [], None]:  # No response was made
        resp_train_JS1.keys = None
        # was no response the correct answer?!
        if str(key_corr).lower() == 'none':
           resp_train_JS1.corr = 1;  # correct non-response
        else:
           resp_train_JS1.corr = 0;  # failed to respond (incorrectly)
    # store data for Seq_Train_JS1 (TrialHandler)
    Seq_Train_JS1.addData('resp_train_JS1.keys',resp_train_JS1.keys)
    Seq_Train_JS1.addData('resp_train_JS1.corr', resp_train_JS1.corr)
    if resp_train_JS1.keys != None:  # we had a response
        Seq_Train_JS1.addData('resp_train_JS1.rt', resp_train_JS1.rt)
    Seq_Train_JS1.addData('resp_train_JS1.started', resp_train_JS1.tStartRefresh)
    Seq_Train_JS1.addData('resp_train_JS1.stopped', resp_train_JS1.tStopRefresh)
    # the Routine "Train_JS1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "Feedback_Train_JS1"-------
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    if resp_train_JS1.corr == 1:
        msg="Acertou!"
    else:
        msg="Errou"
    feedback_text_train_JS1.setText(msg)
    # keep track of which components have finished
    Feedback_Train_JS1Components = [feedback_text_train_JS1]
    for thisComponent in Feedback_Train_JS1Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Feedback_Train_JS1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Feedback_Train_JS1"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = Feedback_Train_JS1Clock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Feedback_Train_JS1Clock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *feedback_text_train_JS1* updates
        if feedback_text_train_JS1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            feedback_text_train_JS1.frameNStart = frameN  # exact frame index
            feedback_text_train_JS1.tStart = t  # local t and not account for scr refresh
            feedback_text_train_JS1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(feedback_text_train_JS1, 'tStartRefresh')  # time at next scr refresh
            feedback_text_train_JS1.setAutoDraw(True)
        if feedback_text_train_JS1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > feedback_text_train_JS1.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                feedback_text_train_JS1.tStop = t  # not accounting for scr refresh
                feedback_text_train_JS1.frameNStop = frameN  # exact frame index
                win.timeOnFlip(feedback_text_train_JS1, 'tStopRefresh')  # time at next scr refresh
                feedback_text_train_JS1.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Feedback_Train_JS1Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Feedback_Train_JS1"-------
    for thisComponent in Feedback_Train_JS1Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    respcorrs.append(resp_train_JS1.corr) 
    
    if len(respcorrs) % 20 == 0:
        seq = respcorrs[20*(int(len(respcorrs) / 20) - 1) : ] 
        if len(list(filter(lambda x: x==1, seq))) > 12:
            Seq_Train_JS1.finished = True
    Seq_Train_JS1.addData('feedback_text_train_JS1.started', feedback_text_train_JS1.tStartRefresh)
    Seq_Train_JS1.addData('feedback_text_train_JS1.stopped', feedback_text_train_JS1.tStopRefresh)
    
    # set up handler to look after randomisation of conditions etc
    Train_JS1_Arrow_Rep = data.TrialHandler(nReps=2, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='Train_JS1_Arrow_Rep')
    thisExp.addLoop(Train_JS1_Arrow_Rep)  # add the loop to the experiment
    thisTrain_JS1_Arrow_Rep = Train_JS1_Arrow_Rep.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrain_JS1_Arrow_Rep.rgb)
    if thisTrain_JS1_Arrow_Rep != None:
        for paramName in thisTrain_JS1_Arrow_Rep:
            exec('{} = thisTrain_JS1_Arrow_Rep[paramName]'.format(paramName))
    
    for thisTrain_JS1_Arrow_Rep in Train_JS1_Arrow_Rep:
        currentLoop = Train_JS1_Arrow_Rep
        # abbreviate parameter names if possible (e.g. rgb = thisTrain_JS1_Arrow_Rep.rgb)
        if thisTrain_JS1_Arrow_Rep != None:
            for paramName in thisTrain_JS1_Arrow_Rep:
                exec('{} = thisTrain_JS1_Arrow_Rep[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "Train_JS1_Arrow"-------
        continueRoutine = True
        routineTimer.add(5.350000)
        # update component parameters for each repeat
        if Seq_Train_JS1.thisTrialN % 15 != 0:
            continueRoutine = False
        arrow_angle = 90 * (round(np.random.rand()) * 2 - 1)
        thisExp.addData('train_JS1_arrow_angle', arrow_angle)
        
        if keys_resp.index('a')==0 and arrow_angle == 90:
            key_corr = 'a' 
        elif keys_resp.index('l')==1 and arrow_angle == -90:
            key_corr = 'l'
        elif keys_resp.index('l')==0 and arrow_angle == 90:
            key_corr = 'l' 
        elif keys_resp.index('a')==1 and arrow_angle == -90:
            key_corr = 'a'
        arrow_image_train_JS1.setOri(arrow_angle)
        resp_train_JS1_arrow.keys = []
        resp_train_JS1_arrow.rt = []
        _resp_train_JS1_arrow_allKeys = []
        # keep track of which components have finished
        Train_JS1_ArrowComponents = [fixation_cross_train_JS1_2, arrow_image_train_JS1, train_JS1_text_arrow, resp_train_JS1_arrow]
        for thisComponent in Train_JS1_ArrowComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        Train_JS1_ArrowClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "Train_JS1_Arrow"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = Train_JS1_ArrowClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=Train_JS1_ArrowClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_train_JS1_2* updates
            if fixation_cross_train_JS1_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_train_JS1_2.frameNStart = frameN  # exact frame index
                fixation_cross_train_JS1_2.tStart = t  # local t and not account for scr refresh
                fixation_cross_train_JS1_2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_train_JS1_2, 'tStartRefresh')  # time at next scr refresh
                fixation_cross_train_JS1_2.setAutoDraw(True)
            if fixation_cross_train_JS1_2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_train_JS1_2.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_train_JS1_2.tStop = t  # not accounting for scr refresh
                    fixation_cross_train_JS1_2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(fixation_cross_train_JS1_2, 'tStopRefresh')  # time at next scr refresh
                    fixation_cross_train_JS1_2.setAutoDraw(False)
            
            # *arrow_image_train_JS1* updates
            if arrow_image_train_JS1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
                # keep track of start time/frame for later
                arrow_image_train_JS1.frameNStart = frameN  # exact frame index
                arrow_image_train_JS1.tStart = t  # local t and not account for scr refresh
                arrow_image_train_JS1.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arrow_image_train_JS1, 'tStartRefresh')  # time at next scr refresh
                arrow_image_train_JS1.setAutoDraw(True)
            if arrow_image_train_JS1.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arrow_image_train_JS1.tStartRefresh + .350-frameTolerance:
                    # keep track of stop time/frame for later
                    arrow_image_train_JS1.tStop = t  # not accounting for scr refresh
                    arrow_image_train_JS1.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(arrow_image_train_JS1, 'tStopRefresh')  # time at next scr refresh
                    arrow_image_train_JS1.setAutoDraw(False)
            
            # *train_JS1_text_arrow* updates
            if train_JS1_text_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                train_JS1_text_arrow.frameNStart = frameN  # exact frame index
                train_JS1_text_arrow.tStart = t  # local t and not account for scr refresh
                train_JS1_text_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(train_JS1_text_arrow, 'tStartRefresh')  # time at next scr refresh
                train_JS1_text_arrow.setAutoDraw(True)
            if train_JS1_text_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > train_JS1_text_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    train_JS1_text_arrow.tStop = t  # not accounting for scr refresh
                    train_JS1_text_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(train_JS1_text_arrow, 'tStopRefresh')  # time at next scr refresh
                    train_JS1_text_arrow.setAutoDraw(False)
            
            # *resp_train_JS1_arrow* updates
            waitOnFlip = False
            if resp_train_JS1_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                resp_train_JS1_arrow.frameNStart = frameN  # exact frame index
                resp_train_JS1_arrow.tStart = t  # local t and not account for scr refresh
                resp_train_JS1_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_train_JS1_arrow, 'tStartRefresh')  # time at next scr refresh
                resp_train_JS1_arrow.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(resp_train_JS1_arrow.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(resp_train_JS1_arrow.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if resp_train_JS1_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_train_JS1_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_train_JS1_arrow.tStop = t  # not accounting for scr refresh
                    resp_train_JS1_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_train_JS1_arrow, 'tStopRefresh')  # time at next scr refresh
                    resp_train_JS1_arrow.status = FINISHED
            if resp_train_JS1_arrow.status == STARTED and not waitOnFlip:
                theseKeys = resp_train_JS1_arrow.getKeys(keyList=['a', 'l'], waitRelease=False)
                _resp_train_JS1_arrow_allKeys.extend(theseKeys)
                if len(_resp_train_JS1_arrow_allKeys):
                    resp_train_JS1_arrow.keys = _resp_train_JS1_arrow_allKeys[-1].name  # just the last key pressed
                    resp_train_JS1_arrow.rt = _resp_train_JS1_arrow_allKeys[-1].rt
                    # was this correct?
                    if (resp_train_JS1_arrow.keys == str(key_corr)) or (resp_train_JS1_arrow.keys == key_corr):
                        resp_train_JS1_arrow.corr = 1
                    else:
                        resp_train_JS1_arrow.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in Train_JS1_ArrowComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Train_JS1_Arrow"-------
        for thisComponent in Train_JS1_ArrowComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        Train_JS1_Arrow_Rep.addData('fixation_cross_train_JS1_2.started', fixation_cross_train_JS1_2.tStartRefresh)
        Train_JS1_Arrow_Rep.addData('fixation_cross_train_JS1_2.stopped', fixation_cross_train_JS1_2.tStopRefresh)
        Train_JS1_Arrow_Rep.addData('arrow_image_train_JS1.started', arrow_image_train_JS1.tStartRefresh)
        Train_JS1_Arrow_Rep.addData('arrow_image_train_JS1.stopped', arrow_image_train_JS1.tStopRefresh)
        Train_JS1_Arrow_Rep.addData('train_JS1_text_arrow.started', train_JS1_text_arrow.tStartRefresh)
        Train_JS1_Arrow_Rep.addData('train_JS1_text_arrow.stopped', train_JS1_text_arrow.tStopRefresh)
        # check responses
        if resp_train_JS1_arrow.keys in ['', [], None]:  # No response was made
            resp_train_JS1_arrow.keys = None
            # was no response the correct answer?!
            if str(key_corr).lower() == 'none':
               resp_train_JS1_arrow.corr = 1;  # correct non-response
            else:
               resp_train_JS1_arrow.corr = 0;  # failed to respond (incorrectly)
        # store data for Train_JS1_Arrow_Rep (TrialHandler)
        Train_JS1_Arrow_Rep.addData('resp_train_JS1_arrow.keys',resp_train_JS1_arrow.keys)
        Train_JS1_Arrow_Rep.addData('resp_train_JS1_arrow.corr', resp_train_JS1_arrow.corr)
        if resp_train_JS1_arrow.keys != None:  # we had a response
            Train_JS1_Arrow_Rep.addData('resp_train_JS1_arrow.rt', resp_train_JS1_arrow.rt)
        Train_JS1_Arrow_Rep.addData('resp_train_JS1_arrow.started', resp_train_JS1_arrow.tStartRefresh)
        Train_JS1_Arrow_Rep.addData('resp_train_JS1_arrow.stopped', resp_train_JS1_arrow.tStopRefresh)
        thisExp.nextEntry()
        
    # completed 2 repeats of 'Train_JS1_Arrow_Rep'
    
    # get names of stimulus parameters
    if Train_JS1_Arrow_Rep.trialList in ([], [None], None):
        params = []
    else:
        params = Train_JS1_Arrow_Rep.trialList[0].keys()
    # save data for this loop
    Train_JS1_Arrow_Rep.saveAsText(filename + 'Train_JS1_Arrow_Rep.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 100 repeats of 'Seq_Train_JS1'

# get names of stimulus parameters
if Seq_Train_JS1.trialList in ([], [None], None):
    params = []
else:
    params = Seq_Train_JS1.trialList[0].keys()
# save data for this loop
Seq_Train_JS1.saveAsText(filename + 'Seq_Train_JS1.csv', delim=',',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "Instructions_JS1_5"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS1_5.keys = []
resp_next_JS1_5.rt = []
_resp_next_JS1_5_allKeys = []
# keep track of which components have finished
Instructions_JS1_5Components = [intro_JS1_5, resp_next_JS1_5]
for thisComponent in Instructions_JS1_5Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS1_5Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS1_5"-------
while continueRoutine:
    # get current time
    t = Instructions_JS1_5Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS1_5Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS1_5* updates
    if intro_JS1_5.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS1_5.frameNStart = frameN  # exact frame index
        intro_JS1_5.tStart = t  # local t and not account for scr refresh
        intro_JS1_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS1_5, 'tStartRefresh')  # time at next scr refresh
        intro_JS1_5.setAutoDraw(True)
    
    # *resp_next_JS1_5* updates
    waitOnFlip = False
    if resp_next_JS1_5.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS1_5.frameNStart = frameN  # exact frame index
        resp_next_JS1_5.tStart = t  # local t and not account for scr refresh
        resp_next_JS1_5.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS1_5, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS1_5.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS1_5.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS1_5.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS1_5.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS1_5.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS1_5_allKeys.extend(theseKeys)
        if len(_resp_next_JS1_5_allKeys):
            resp_next_JS1_5.keys = _resp_next_JS1_5_allKeys[-1].name  # just the last key pressed
            resp_next_JS1_5.rt = _resp_next_JS1_5_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS1_5Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS1_5"-------
for thisComponent in Instructions_JS1_5Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS1_5.started', intro_JS1_5.tStartRefresh)
thisExp.addData('intro_JS1_5.stopped', intro_JS1_5.tStopRefresh)
# check responses
if resp_next_JS1_5.keys in ['', [], None]:  # No response was made
    resp_next_JS1_5.keys = None
thisExp.addData('resp_next_JS1_5.keys',resp_next_JS1_5.keys)
if resp_next_JS1_5.keys != None:  # we had a response
    thisExp.addData('resp_next_JS1_5.rt', resp_next_JS1_5.rt)
thisExp.addData('resp_next_JS1_5.started', resp_next_JS1_5.tStartRefresh)
thisExp.addData('resp_next_JS1_5.stopped', resp_next_JS1_5.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS1_5" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
Seq_Test_JS1 = data.TrialHandler(nReps=1, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Estimulos.xlsx', selection=':90'),
    seed=None, name='Seq_Test_JS1')
thisExp.addLoop(Seq_Test_JS1)  # add the loop to the experiment
thisSeq_Test_JS1 = Seq_Test_JS1.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSeq_Test_JS1.rgb)
if thisSeq_Test_JS1 != None:
    for paramName in thisSeq_Test_JS1:
        exec('{} = thisSeq_Test_JS1[paramName]'.format(paramName))

for thisSeq_Test_JS1 in Seq_Test_JS1:
    currentLoop = Seq_Test_JS1
    # abbreviate parameter names if possible (e.g. rgb = thisSeq_Test_JS1.rgb)
    if thisSeq_Test_JS1 != None:
        for paramName in thisSeq_Test_JS1:
            exec('{} = thisSeq_Test_JS1[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Test_JS1"-------
    continueRoutine = True
    # update component parameters for each repeat
    if keys_resp.index('a')==0 and RESPOSTA_JS1_TESTE==1:
        key_corr = 'a' 
    elif keys_resp.index('l')==1 and RESPOSTA_JS1_TESTE==0:
        key_corr = 'l'
    elif keys_resp.index('l')==0 and RESPOSTA_JS1_TESTE==1:
        key_corr = 'l' 
    elif keys_resp.index('a')==1 and RESPOSTA_JS1_TESTE==0:
        key_corr = 'a'
    text_test_JS1.setText(VERBO_JS1_TESTE)
    resp_test_JS1.keys = []
    resp_test_JS1.rt = []
    _resp_test_JS1_allKeys = []
    # keep track of which components have finished
    Test_JS1Components = [fixation_cross_test_JS1_1, text_test_JS1, resp_test_JS1]
    for thisComponent in Test_JS1Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Test_JS1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Test_JS1"-------
    while continueRoutine:
        # get current time
        t = Test_JS1Clock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Test_JS1Clock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_test_JS1_1* updates
        if fixation_cross_test_JS1_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_test_JS1_1.frameNStart = frameN  # exact frame index
            fixation_cross_test_JS1_1.tStart = t  # local t and not account for scr refresh
            fixation_cross_test_JS1_1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_test_JS1_1, 'tStartRefresh')  # time at next scr refresh
            fixation_cross_test_JS1_1.setAutoDraw(True)
        if fixation_cross_test_JS1_1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_test_JS1_1.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_test_JS1_1.tStop = t  # not accounting for scr refresh
                fixation_cross_test_JS1_1.frameNStop = frameN  # exact frame index
                win.timeOnFlip(fixation_cross_test_JS1_1, 'tStopRefresh')  # time at next scr refresh
                fixation_cross_test_JS1_1.setAutoDraw(False)
        
        # *text_test_JS1* updates
        if text_test_JS1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            text_test_JS1.frameNStart = frameN  # exact frame index
            text_test_JS1.tStart = t  # local t and not account for scr refresh
            text_test_JS1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_test_JS1, 'tStartRefresh')  # time at next scr refresh
            text_test_JS1.setAutoDraw(True)
        
        # *resp_test_JS1* updates
        waitOnFlip = False
        if resp_test_JS1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            resp_test_JS1.frameNStart = frameN  # exact frame index
            resp_test_JS1.tStart = t  # local t and not account for scr refresh
            resp_test_JS1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(resp_test_JS1, 'tStartRefresh')  # time at next scr refresh
            resp_test_JS1.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(resp_test_JS1.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(resp_test_JS1.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if resp_test_JS1.status == STARTED and not waitOnFlip:
            theseKeys = resp_test_JS1.getKeys(keyList=['a', 'l'], waitRelease=False)
            _resp_test_JS1_allKeys.extend(theseKeys)
            if len(_resp_test_JS1_allKeys):
                resp_test_JS1.keys = _resp_test_JS1_allKeys[-1].name  # just the last key pressed
                resp_test_JS1.rt = _resp_test_JS1_allKeys[-1].rt
                # was this correct?
                if (resp_test_JS1.keys == str(key_corr)) or (resp_test_JS1.keys == key_corr):
                    resp_test_JS1.corr = 1
                else:
                    resp_test_JS1.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Test_JS1Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Test_JS1"-------
    for thisComponent in Test_JS1Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    Seq_Test_JS1.addData('fixation_cross_test_JS1_1.started', fixation_cross_test_JS1_1.tStartRefresh)
    Seq_Test_JS1.addData('fixation_cross_test_JS1_1.stopped', fixation_cross_test_JS1_1.tStopRefresh)
    Seq_Test_JS1.addData('text_test_JS1.started', text_test_JS1.tStartRefresh)
    Seq_Test_JS1.addData('text_test_JS1.stopped', text_test_JS1.tStopRefresh)
    # check responses
    if resp_test_JS1.keys in ['', [], None]:  # No response was made
        resp_test_JS1.keys = None
        # was no response the correct answer?!
        if str(key_corr).lower() == 'none':
           resp_test_JS1.corr = 1;  # correct non-response
        else:
           resp_test_JS1.corr = 0;  # failed to respond (incorrectly)
    # store data for Seq_Test_JS1 (TrialHandler)
    Seq_Test_JS1.addData('resp_test_JS1.keys',resp_test_JS1.keys)
    Seq_Test_JS1.addData('resp_test_JS1.corr', resp_test_JS1.corr)
    if resp_test_JS1.keys != None:  # we had a response
        Seq_Test_JS1.addData('resp_test_JS1.rt', resp_test_JS1.rt)
    Seq_Test_JS1.addData('resp_test_JS1.started', resp_test_JS1.tStartRefresh)
    Seq_Test_JS1.addData('resp_test_JS1.stopped', resp_test_JS1.tStopRefresh)
    # the Routine "Test_JS1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    Test_JS1_Arrow_Rep = data.TrialHandler(nReps=2, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='Test_JS1_Arrow_Rep')
    thisExp.addLoop(Test_JS1_Arrow_Rep)  # add the loop to the experiment
    thisTest_JS1_Arrow_Rep = Test_JS1_Arrow_Rep.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTest_JS1_Arrow_Rep.rgb)
    if thisTest_JS1_Arrow_Rep != None:
        for paramName in thisTest_JS1_Arrow_Rep:
            exec('{} = thisTest_JS1_Arrow_Rep[paramName]'.format(paramName))
    
    for thisTest_JS1_Arrow_Rep in Test_JS1_Arrow_Rep:
        currentLoop = Test_JS1_Arrow_Rep
        # abbreviate parameter names if possible (e.g. rgb = thisTest_JS1_Arrow_Rep.rgb)
        if thisTest_JS1_Arrow_Rep != None:
            for paramName in thisTest_JS1_Arrow_Rep:
                exec('{} = thisTest_JS1_Arrow_Rep[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "Test_JS1_Arrow"-------
        continueRoutine = True
        routineTimer.add(5.350000)
        # update component parameters for each repeat
        if Seq_Test_JS1.thisTrialN % 15 != 0:
            continueRoutine = False
        arrow_angle = 90 * (round(np.random.rand()) * 2 - 1)
        thisExp.addData('test_JS1_arrow_angle', arrow_angle)
        
        if keys_resp.index('a')==0 and arrow_angle == 90:
            key_corr = 'a' 
        elif keys_resp.index('l')==1 and arrow_angle == -90:
            key_corr = 'l'
        elif keys_resp.index('l')==0 and arrow_angle == 90:
            key_corr = 'l' 
        elif keys_resp.index('a')==1 and arrow_angle == -90:
            key_corr = 'a'
        arrow_image_test_JS1.setOri(arrow_angle)
        resp_test_JS1_arrow.keys = []
        resp_test_JS1_arrow.rt = []
        _resp_test_JS1_arrow_allKeys = []
        # keep track of which components have finished
        Test_JS1_ArrowComponents = [fixation_cross_test_JS1, arrow_image_test_JS1, test_JS1_text_arrow, resp_test_JS1_arrow]
        for thisComponent in Test_JS1_ArrowComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        Test_JS1_ArrowClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "Test_JS1_Arrow"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = Test_JS1_ArrowClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=Test_JS1_ArrowClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_test_JS1* updates
            if fixation_cross_test_JS1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_test_JS1.frameNStart = frameN  # exact frame index
                fixation_cross_test_JS1.tStart = t  # local t and not account for scr refresh
                fixation_cross_test_JS1.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_test_JS1, 'tStartRefresh')  # time at next scr refresh
                fixation_cross_test_JS1.setAutoDraw(True)
            if fixation_cross_test_JS1.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_test_JS1.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_test_JS1.tStop = t  # not accounting for scr refresh
                    fixation_cross_test_JS1.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(fixation_cross_test_JS1, 'tStopRefresh')  # time at next scr refresh
                    fixation_cross_test_JS1.setAutoDraw(False)
            
            # *arrow_image_test_JS1* updates
            if arrow_image_test_JS1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
                # keep track of start time/frame for later
                arrow_image_test_JS1.frameNStart = frameN  # exact frame index
                arrow_image_test_JS1.tStart = t  # local t and not account for scr refresh
                arrow_image_test_JS1.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arrow_image_test_JS1, 'tStartRefresh')  # time at next scr refresh
                arrow_image_test_JS1.setAutoDraw(True)
            if arrow_image_test_JS1.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arrow_image_test_JS1.tStartRefresh + .350-frameTolerance:
                    # keep track of stop time/frame for later
                    arrow_image_test_JS1.tStop = t  # not accounting for scr refresh
                    arrow_image_test_JS1.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(arrow_image_test_JS1, 'tStopRefresh')  # time at next scr refresh
                    arrow_image_test_JS1.setAutoDraw(False)
            
            # *test_JS1_text_arrow* updates
            if test_JS1_text_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                test_JS1_text_arrow.frameNStart = frameN  # exact frame index
                test_JS1_text_arrow.tStart = t  # local t and not account for scr refresh
                test_JS1_text_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(test_JS1_text_arrow, 'tStartRefresh')  # time at next scr refresh
                test_JS1_text_arrow.setAutoDraw(True)
            if test_JS1_text_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > test_JS1_text_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    test_JS1_text_arrow.tStop = t  # not accounting for scr refresh
                    test_JS1_text_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(test_JS1_text_arrow, 'tStopRefresh')  # time at next scr refresh
                    test_JS1_text_arrow.setAutoDraw(False)
            
            # *resp_test_JS1_arrow* updates
            waitOnFlip = False
            if resp_test_JS1_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                resp_test_JS1_arrow.frameNStart = frameN  # exact frame index
                resp_test_JS1_arrow.tStart = t  # local t and not account for scr refresh
                resp_test_JS1_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_test_JS1_arrow, 'tStartRefresh')  # time at next scr refresh
                resp_test_JS1_arrow.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(resp_test_JS1_arrow.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(resp_test_JS1_arrow.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if resp_test_JS1_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_test_JS1_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_test_JS1_arrow.tStop = t  # not accounting for scr refresh
                    resp_test_JS1_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_test_JS1_arrow, 'tStopRefresh')  # time at next scr refresh
                    resp_test_JS1_arrow.status = FINISHED
            if resp_test_JS1_arrow.status == STARTED and not waitOnFlip:
                theseKeys = resp_test_JS1_arrow.getKeys(keyList=['a', 'l'], waitRelease=False)
                _resp_test_JS1_arrow_allKeys.extend(theseKeys)
                if len(_resp_test_JS1_arrow_allKeys):
                    resp_test_JS1_arrow.keys = _resp_test_JS1_arrow_allKeys[-1].name  # just the last key pressed
                    resp_test_JS1_arrow.rt = _resp_test_JS1_arrow_allKeys[-1].rt
                    # was this correct?
                    if (resp_test_JS1_arrow.keys == str(key_corr)) or (resp_test_JS1_arrow.keys == key_corr):
                        resp_test_JS1_arrow.corr = 1
                    else:
                        resp_test_JS1_arrow.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in Test_JS1_ArrowComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Test_JS1_Arrow"-------
        for thisComponent in Test_JS1_ArrowComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        Test_JS1_Arrow_Rep.addData('fixation_cross_test_JS1.started', fixation_cross_test_JS1.tStartRefresh)
        Test_JS1_Arrow_Rep.addData('fixation_cross_test_JS1.stopped', fixation_cross_test_JS1.tStopRefresh)
        Test_JS1_Arrow_Rep.addData('arrow_image_test_JS1.started', arrow_image_test_JS1.tStartRefresh)
        Test_JS1_Arrow_Rep.addData('arrow_image_test_JS1.stopped', arrow_image_test_JS1.tStopRefresh)
        Test_JS1_Arrow_Rep.addData('test_JS1_text_arrow.started', test_JS1_text_arrow.tStartRefresh)
        Test_JS1_Arrow_Rep.addData('test_JS1_text_arrow.stopped', test_JS1_text_arrow.tStopRefresh)
        # check responses
        if resp_test_JS1_arrow.keys in ['', [], None]:  # No response was made
            resp_test_JS1_arrow.keys = None
            # was no response the correct answer?!
            if str(key_corr).lower() == 'none':
               resp_test_JS1_arrow.corr = 1;  # correct non-response
            else:
               resp_test_JS1_arrow.corr = 0;  # failed to respond (incorrectly)
        # store data for Test_JS1_Arrow_Rep (TrialHandler)
        Test_JS1_Arrow_Rep.addData('resp_test_JS1_arrow.keys',resp_test_JS1_arrow.keys)
        Test_JS1_Arrow_Rep.addData('resp_test_JS1_arrow.corr', resp_test_JS1_arrow.corr)
        if resp_test_JS1_arrow.keys != None:  # we had a response
            Test_JS1_Arrow_Rep.addData('resp_test_JS1_arrow.rt', resp_test_JS1_arrow.rt)
        Test_JS1_Arrow_Rep.addData('resp_test_JS1_arrow.started', resp_test_JS1_arrow.tStartRefresh)
        Test_JS1_Arrow_Rep.addData('resp_test_JS1_arrow.stopped', resp_test_JS1_arrow.tStopRefresh)
        thisExp.nextEntry()
        
    # completed 2 repeats of 'Test_JS1_Arrow_Rep'
    
    # get names of stimulus parameters
    if Test_JS1_Arrow_Rep.trialList in ([], [None], None):
        params = []
    else:
        params = Test_JS1_Arrow_Rep.trialList[0].keys()
    # save data for this loop
    Test_JS1_Arrow_Rep.saveAsText(filename + 'Test_JS1_Arrow_Rep.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 1 repeats of 'Seq_Test_JS1'

# get names of stimulus parameters
if Seq_Test_JS1.trialList in ([], [None], None):
    params = []
else:
    params = Seq_Test_JS1.trialList[0].keys()
# save data for this loop
Seq_Test_JS1.saveAsText(filename + 'Seq_Test_JS1.csv', delim=',',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "Instructions_JS2_1"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS2_1.keys = []
resp_next_JS2_1.rt = []
_resp_next_JS2_1_allKeys = []
# keep track of which components have finished
Instructions_JS2_1Components = [JS2_1, intro_JS2_1, next_JS2_1, resp_next_JS2_1]
for thisComponent in Instructions_JS2_1Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS2_1Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS2_1"-------
while continueRoutine:
    # get current time
    t = Instructions_JS2_1Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS2_1Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *JS2_1* updates
    if JS2_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        JS2_1.frameNStart = frameN  # exact frame index
        JS2_1.tStart = t  # local t and not account for scr refresh
        JS2_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(JS2_1, 'tStartRefresh')  # time at next scr refresh
        JS2_1.setAutoDraw(True)
    if JS2_1.status == STARTED:
        # is it time to stop? (based on global clock, using actual start)
        if tThisFlipGlobal > JS2_1.tStartRefresh + 1.0-frameTolerance:
            # keep track of stop time/frame for later
            JS2_1.tStop = t  # not accounting for scr refresh
            JS2_1.frameNStop = frameN  # exact frame index
            win.timeOnFlip(JS2_1, 'tStopRefresh')  # time at next scr refresh
            JS2_1.setAutoDraw(False)
    
    # *intro_JS2_1* updates
    if intro_JS2_1.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        intro_JS2_1.frameNStart = frameN  # exact frame index
        intro_JS2_1.tStart = t  # local t and not account for scr refresh
        intro_JS2_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS2_1, 'tStartRefresh')  # time at next scr refresh
        intro_JS2_1.setAutoDraw(True)
    
    # *next_JS2_1* updates
    if next_JS2_1.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
        # keep track of start time/frame for later
        next_JS2_1.frameNStart = frameN  # exact frame index
        next_JS2_1.tStart = t  # local t and not account for scr refresh
        next_JS2_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_JS2_1, 'tStartRefresh')  # time at next scr refresh
        next_JS2_1.setAutoDraw(True)
    
    # *resp_next_JS2_1* updates
    waitOnFlip = False
    if resp_next_JS2_1.status == NOT_STARTED and tThisFlip >= 2-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS2_1.frameNStart = frameN  # exact frame index
        resp_next_JS2_1.tStart = t  # local t and not account for scr refresh
        resp_next_JS2_1.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS2_1, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS2_1.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS2_1.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS2_1.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS2_1.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS2_1.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS2_1_allKeys.extend(theseKeys)
        if len(_resp_next_JS2_1_allKeys):
            resp_next_JS2_1.keys = _resp_next_JS2_1_allKeys[-1].name  # just the last key pressed
            resp_next_JS2_1.rt = _resp_next_JS2_1_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS2_1Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS2_1"-------
for thisComponent in Instructions_JS2_1Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('JS2_1.started', JS2_1.tStartRefresh)
thisExp.addData('JS2_1.stopped', JS2_1.tStopRefresh)
thisExp.addData('intro_JS2_1.started', intro_JS2_1.tStartRefresh)
thisExp.addData('intro_JS2_1.stopped', intro_JS2_1.tStopRefresh)
thisExp.addData('next_JS2_1.started', next_JS2_1.tStartRefresh)
thisExp.addData('next_JS2_1.stopped', next_JS2_1.tStopRefresh)
# check responses
if resp_next_JS2_1.keys in ['', [], None]:  # No response was made
    resp_next_JS2_1.keys = None
thisExp.addData('resp_next_JS2_1.keys',resp_next_JS2_1.keys)
if resp_next_JS2_1.keys != None:  # we had a response
    thisExp.addData('resp_next_JS2_1.rt', resp_next_JS2_1.rt)
thisExp.addData('resp_next_JS2_1.started', resp_next_JS2_1.tStartRefresh)
thisExp.addData('resp_next_JS2_1.stopped', resp_next_JS2_1.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS2_1" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_JS2_2"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS2_2.keys = []
resp_next_JS2_2.rt = []
_resp_next_JS2_2_allKeys = []
# keep track of which components have finished
Instructions_JS2_2Components = [intro_JS2_2, next_JS2_2, resp_next_JS2_2]
for thisComponent in Instructions_JS2_2Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS2_2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS2_2"-------
while continueRoutine:
    # get current time
    t = Instructions_JS2_2Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS2_2Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS2_2* updates
    if intro_JS2_2.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS2_2.frameNStart = frameN  # exact frame index
        intro_JS2_2.tStart = t  # local t and not account for scr refresh
        intro_JS2_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS2_2, 'tStartRefresh')  # time at next scr refresh
        intro_JS2_2.setAutoDraw(True)
    
    # *next_JS2_2* updates
    if next_JS2_2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        next_JS2_2.frameNStart = frameN  # exact frame index
        next_JS2_2.tStart = t  # local t and not account for scr refresh
        next_JS2_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_JS2_2, 'tStartRefresh')  # time at next scr refresh
        next_JS2_2.setAutoDraw(True)
    
    # *resp_next_JS2_2* updates
    waitOnFlip = False
    if resp_next_JS2_2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS2_2.frameNStart = frameN  # exact frame index
        resp_next_JS2_2.tStart = t  # local t and not account for scr refresh
        resp_next_JS2_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS2_2, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS2_2.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS2_2.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS2_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS2_2.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS2_2.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS2_2_allKeys.extend(theseKeys)
        if len(_resp_next_JS2_2_allKeys):
            resp_next_JS2_2.keys = _resp_next_JS2_2_allKeys[-1].name  # just the last key pressed
            resp_next_JS2_2.rt = _resp_next_JS2_2_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS2_2Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS2_2"-------
for thisComponent in Instructions_JS2_2Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS2_2.started', intro_JS2_2.tStartRefresh)
thisExp.addData('intro_JS2_2.stopped', intro_JS2_2.tStopRefresh)
thisExp.addData('next_JS2_2.started', next_JS2_2.tStartRefresh)
thisExp.addData('next_JS2_2.stopped', next_JS2_2.tStopRefresh)
# check responses
if resp_next_JS2_2.keys in ['', [], None]:  # No response was made
    resp_next_JS2_2.keys = None
thisExp.addData('resp_next_JS2_2.keys',resp_next_JS2_2.keys)
if resp_next_JS2_2.keys != None:  # we had a response
    thisExp.addData('resp_next_JS2_2.rt', resp_next_JS2_2.rt)
thisExp.addData('resp_next_JS2_2.started', resp_next_JS2_2.tStartRefresh)
thisExp.addData('resp_next_JS2_2.stopped', resp_next_JS2_2.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS2_2" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "Instructions_JS2_3"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS2_3.keys = []
resp_next_JS2_3.rt = []
_resp_next_JS2_3_allKeys = []
# keep track of which components have finished
Instructions_JS2_3Components = [intro_JS2_3, resp_next_JS2_3]
for thisComponent in Instructions_JS2_3Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS2_3Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS2_3"-------
while continueRoutine:
    # get current time
    t = Instructions_JS2_3Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS2_3Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS2_3* updates
    if intro_JS2_3.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS2_3.frameNStart = frameN  # exact frame index
        intro_JS2_3.tStart = t  # local t and not account for scr refresh
        intro_JS2_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS2_3, 'tStartRefresh')  # time at next scr refresh
        intro_JS2_3.setAutoDraw(True)
    
    # *resp_next_JS2_3* updates
    waitOnFlip = False
    if resp_next_JS2_3.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS2_3.frameNStart = frameN  # exact frame index
        resp_next_JS2_3.tStart = t  # local t and not account for scr refresh
        resp_next_JS2_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS2_3, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS2_3.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS2_3.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS2_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS2_3.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS2_3.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS2_3_allKeys.extend(theseKeys)
        if len(_resp_next_JS2_3_allKeys):
            resp_next_JS2_3.keys = _resp_next_JS2_3_allKeys[-1].name  # just the last key pressed
            resp_next_JS2_3.rt = _resp_next_JS2_3_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS2_3Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS2_3"-------
for thisComponent in Instructions_JS2_3Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS2_3.started', intro_JS2_3.tStartRefresh)
thisExp.addData('intro_JS2_3.stopped', intro_JS2_3.tStopRefresh)
# check responses
if resp_next_JS2_3.keys in ['', [], None]:  # No response was made
    resp_next_JS2_3.keys = None
thisExp.addData('resp_next_JS2_3.keys',resp_next_JS2_3.keys)
if resp_next_JS2_3.keys != None:  # we had a response
    thisExp.addData('resp_next_JS2_3.rt', resp_next_JS2_3.rt)
thisExp.addData('resp_next_JS2_3.started', resp_next_JS2_3.tStartRefresh)
thisExp.addData('resp_next_JS2_3.stopped', resp_next_JS2_3.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS2_3" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
Seq_Train_JS2 = data.TrialHandler(nReps=100, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Estimulos.xlsx', selection=':20'),
    seed=None, name='Seq_Train_JS2')
thisExp.addLoop(Seq_Train_JS2)  # add the loop to the experiment
thisSeq_Train_JS2 = Seq_Train_JS2.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSeq_Train_JS2.rgb)
if thisSeq_Train_JS2 != None:
    for paramName in thisSeq_Train_JS2:
        exec('{} = thisSeq_Train_JS2[paramName]'.format(paramName))

for thisSeq_Train_JS2 in Seq_Train_JS2:
    currentLoop = Seq_Train_JS2
    # abbreviate parameter names if possible (e.g. rgb = thisSeq_Train_JS2.rgb)
    if thisSeq_Train_JS2 != None:
        for paramName in thisSeq_Train_JS2:
            exec('{} = thisSeq_Train_JS2[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Train_JS2"-------
    continueRoutine = True
    # update component parameters for each repeat
    if keys_resp.index('a')==0 and RESPOSTA_JS2_TREINO==1:
        key_corr = 'a' 
    elif keys_resp.index('l')==1 and RESPOSTA_JS2_TREINO==0:
        key_corr = 'l'
    elif keys_resp.index('l')==0 and RESPOSTA_JS2_TREINO==1:
        key_corr = 'l' 
    elif keys_resp.index('a')==1 and RESPOSTA_JS2_TREINO==0:
        key_corr = 'a'
    text_train_JS2.setText(VERBO_JS2_TREINO)
    resp_train_JS2.keys = []
    resp_train_JS2.rt = []
    _resp_train_JS2_allKeys = []
    # keep track of which components have finished
    Train_JS2Components = [fixation_cross_train_JS2, text_train_JS2, resp_train_JS2]
    for thisComponent in Train_JS2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Train_JS2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Train_JS2"-------
    while continueRoutine:
        # get current time
        t = Train_JS2Clock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Train_JS2Clock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_train_JS2* updates
        if fixation_cross_train_JS2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_train_JS2.frameNStart = frameN  # exact frame index
            fixation_cross_train_JS2.tStart = t  # local t and not account for scr refresh
            fixation_cross_train_JS2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_train_JS2, 'tStartRefresh')  # time at next scr refresh
            fixation_cross_train_JS2.setAutoDraw(True)
        if fixation_cross_train_JS2.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_train_JS2.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_train_JS2.tStop = t  # not accounting for scr refresh
                fixation_cross_train_JS2.frameNStop = frameN  # exact frame index
                win.timeOnFlip(fixation_cross_train_JS2, 'tStopRefresh')  # time at next scr refresh
                fixation_cross_train_JS2.setAutoDraw(False)
        
        # *text_train_JS2* updates
        if text_train_JS2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            text_train_JS2.frameNStart = frameN  # exact frame index
            text_train_JS2.tStart = t  # local t and not account for scr refresh
            text_train_JS2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_train_JS2, 'tStartRefresh')  # time at next scr refresh
            text_train_JS2.setAutoDraw(True)
        
        # *resp_train_JS2* updates
        waitOnFlip = False
        if resp_train_JS2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            resp_train_JS2.frameNStart = frameN  # exact frame index
            resp_train_JS2.tStart = t  # local t and not account for scr refresh
            resp_train_JS2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(resp_train_JS2, 'tStartRefresh')  # time at next scr refresh
            resp_train_JS2.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(resp_train_JS2.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(resp_train_JS2.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if resp_train_JS2.status == STARTED and not waitOnFlip:
            theseKeys = resp_train_JS2.getKeys(keyList=['a', 'l'], waitRelease=False)
            _resp_train_JS2_allKeys.extend(theseKeys)
            if len(_resp_train_JS2_allKeys):
                resp_train_JS2.keys = _resp_train_JS2_allKeys[-1].name  # just the last key pressed
                resp_train_JS2.rt = _resp_train_JS2_allKeys[-1].rt
                # was this correct?
                if (resp_train_JS2.keys == str(key_corr)) or (resp_train_JS2.keys == key_corr):
                    resp_train_JS2.corr = 1
                else:
                    resp_train_JS2.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Train_JS2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Train_JS2"-------
    for thisComponent in Train_JS2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    Seq_Train_JS2.addData('fixation_cross_train_JS2.started', fixation_cross_train_JS2.tStartRefresh)
    Seq_Train_JS2.addData('fixation_cross_train_JS2.stopped', fixation_cross_train_JS2.tStopRefresh)
    Seq_Train_JS2.addData('text_train_JS2.started', text_train_JS2.tStartRefresh)
    Seq_Train_JS2.addData('text_train_JS2.stopped', text_train_JS2.tStopRefresh)
    # check responses
    if resp_train_JS2.keys in ['', [], None]:  # No response was made
        resp_train_JS2.keys = None
        # was no response the correct answer?!
        if str(key_corr).lower() == 'none':
           resp_train_JS2.corr = 1;  # correct non-response
        else:
           resp_train_JS2.corr = 0;  # failed to respond (incorrectly)
    # store data for Seq_Train_JS2 (TrialHandler)
    Seq_Train_JS2.addData('resp_train_JS2.keys',resp_train_JS2.keys)
    Seq_Train_JS2.addData('resp_train_JS2.corr', resp_train_JS2.corr)
    if resp_train_JS2.keys != None:  # we had a response
        Seq_Train_JS2.addData('resp_train_JS2.rt', resp_train_JS2.rt)
    Seq_Train_JS2.addData('resp_train_JS2.started', resp_train_JS2.tStartRefresh)
    Seq_Train_JS2.addData('resp_train_JS2.stopped', resp_train_JS2.tStopRefresh)
    # the Routine "Train_JS2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "Feedback_Train_JS2"-------
    continueRoutine = True
    routineTimer.add(1.000000)
    # update component parameters for each repeat
    if resp_train_JS2.corr == 1:
        msg="Acertou!"
    else:
        msg="Errou"
    feedback_text_train_JS2.setText(msg)
    # keep track of which components have finished
    Feedback_Train_JS2Components = [feedback_text_train_JS2]
    for thisComponent in Feedback_Train_JS2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Feedback_Train_JS2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Feedback_Train_JS2"-------
    while continueRoutine and routineTimer.getTime() > 0:
        # get current time
        t = Feedback_Train_JS2Clock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Feedback_Train_JS2Clock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *feedback_text_train_JS2* updates
        if feedback_text_train_JS2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            feedback_text_train_JS2.frameNStart = frameN  # exact frame index
            feedback_text_train_JS2.tStart = t  # local t and not account for scr refresh
            feedback_text_train_JS2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(feedback_text_train_JS2, 'tStartRefresh')  # time at next scr refresh
            feedback_text_train_JS2.setAutoDraw(True)
        if feedback_text_train_JS2.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > feedback_text_train_JS2.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                feedback_text_train_JS2.tStop = t  # not accounting for scr refresh
                feedback_text_train_JS2.frameNStop = frameN  # exact frame index
                win.timeOnFlip(feedback_text_train_JS2, 'tStopRefresh')  # time at next scr refresh
                feedback_text_train_JS2.setAutoDraw(False)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Feedback_Train_JS2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Feedback_Train_JS2"-------
    for thisComponent in Feedback_Train_JS2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    respcorrs.append(resp_train_JS2.corr) 
    
    if len(respcorrs) % 20 == 0:
        seq = respcorrs[20*(int(len(respcorrs) / 20) - 1) : ] 
        if len(list(filter(lambda x: x==1, seq))) > 12:
            Seq_Train_JS2.finished = True
    Seq_Train_JS2.addData('feedback_text_train_JS2.started', feedback_text_train_JS2.tStartRefresh)
    Seq_Train_JS2.addData('feedback_text_train_JS2.stopped', feedback_text_train_JS2.tStopRefresh)
    
    # set up handler to look after randomisation of conditions etc
    Train_JS2_Arrow_Rep = data.TrialHandler(nReps=2, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='Train_JS2_Arrow_Rep')
    thisExp.addLoop(Train_JS2_Arrow_Rep)  # add the loop to the experiment
    thisTrain_JS2_Arrow_Rep = Train_JS2_Arrow_Rep.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTrain_JS2_Arrow_Rep.rgb)
    if thisTrain_JS2_Arrow_Rep != None:
        for paramName in thisTrain_JS2_Arrow_Rep:
            exec('{} = thisTrain_JS2_Arrow_Rep[paramName]'.format(paramName))
    
    for thisTrain_JS2_Arrow_Rep in Train_JS2_Arrow_Rep:
        currentLoop = Train_JS2_Arrow_Rep
        # abbreviate parameter names if possible (e.g. rgb = thisTrain_JS2_Arrow_Rep.rgb)
        if thisTrain_JS2_Arrow_Rep != None:
            for paramName in thisTrain_JS2_Arrow_Rep:
                exec('{} = thisTrain_JS2_Arrow_Rep[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "Train_JS2_Arrow"-------
        continueRoutine = True
        routineTimer.add(5.350000)
        # update component parameters for each repeat
        if Seq_Train_JS2.thisTrialN % 15 != 0:
            continueRoutine = False
        arrow_angle = 90 * (round(np.random.rand()) * 2 - 1)
        thisExp.addData('train_JS2_arrow_angle', arrow_angle)
        
        if keys_resp.index('a')==0 and arrow_angle == 90:
            key_corr = 'a' 
        elif keys_resp.index('l')==1 and arrow_angle == -90:
            key_corr = 'l'
        elif keys_resp.index('l')==0 and arrow_angle == 90:
            key_corr = 'l' 
        elif keys_resp.index('a')==1 and arrow_angle == -90:
            key_corr = 'a'
        arrow_image_train_JS2.setOri(arrow_angle)
        resp_train_JS2_arrow.keys = []
        resp_train_JS2_arrow.rt = []
        _resp_train_JS2_arrow_allKeys = []
        # keep track of which components have finished
        Train_JS2_ArrowComponents = [fixation_cross_train_JS2_2, arrow_image_train_JS2, train_JS2_text_arrow, resp_train_JS2_arrow]
        for thisComponent in Train_JS2_ArrowComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        Train_JS2_ArrowClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "Train_JS2_Arrow"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = Train_JS2_ArrowClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=Train_JS2_ArrowClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_train_JS2_2* updates
            if fixation_cross_train_JS2_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_train_JS2_2.frameNStart = frameN  # exact frame index
                fixation_cross_train_JS2_2.tStart = t  # local t and not account for scr refresh
                fixation_cross_train_JS2_2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_train_JS2_2, 'tStartRefresh')  # time at next scr refresh
                fixation_cross_train_JS2_2.setAutoDraw(True)
            if fixation_cross_train_JS2_2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_train_JS2_2.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_train_JS2_2.tStop = t  # not accounting for scr refresh
                    fixation_cross_train_JS2_2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(fixation_cross_train_JS2_2, 'tStopRefresh')  # time at next scr refresh
                    fixation_cross_train_JS2_2.setAutoDraw(False)
            
            # *arrow_image_train_JS2* updates
            if arrow_image_train_JS2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
                # keep track of start time/frame for later
                arrow_image_train_JS2.frameNStart = frameN  # exact frame index
                arrow_image_train_JS2.tStart = t  # local t and not account for scr refresh
                arrow_image_train_JS2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arrow_image_train_JS2, 'tStartRefresh')  # time at next scr refresh
                arrow_image_train_JS2.setAutoDraw(True)
            if arrow_image_train_JS2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arrow_image_train_JS2.tStartRefresh + .350-frameTolerance:
                    # keep track of stop time/frame for later
                    arrow_image_train_JS2.tStop = t  # not accounting for scr refresh
                    arrow_image_train_JS2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(arrow_image_train_JS2, 'tStopRefresh')  # time at next scr refresh
                    arrow_image_train_JS2.setAutoDraw(False)
            
            # *train_JS2_text_arrow* updates
            if train_JS2_text_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                train_JS2_text_arrow.frameNStart = frameN  # exact frame index
                train_JS2_text_arrow.tStart = t  # local t and not account for scr refresh
                train_JS2_text_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(train_JS2_text_arrow, 'tStartRefresh')  # time at next scr refresh
                train_JS2_text_arrow.setAutoDraw(True)
            if train_JS2_text_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > train_JS2_text_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    train_JS2_text_arrow.tStop = t  # not accounting for scr refresh
                    train_JS2_text_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(train_JS2_text_arrow, 'tStopRefresh')  # time at next scr refresh
                    train_JS2_text_arrow.setAutoDraw(False)
            
            # *resp_train_JS2_arrow* updates
            waitOnFlip = False
            if resp_train_JS2_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                resp_train_JS2_arrow.frameNStart = frameN  # exact frame index
                resp_train_JS2_arrow.tStart = t  # local t and not account for scr refresh
                resp_train_JS2_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_train_JS2_arrow, 'tStartRefresh')  # time at next scr refresh
                resp_train_JS2_arrow.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(resp_train_JS2_arrow.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(resp_train_JS2_arrow.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if resp_train_JS2_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_train_JS2_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_train_JS2_arrow.tStop = t  # not accounting for scr refresh
                    resp_train_JS2_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_train_JS2_arrow, 'tStopRefresh')  # time at next scr refresh
                    resp_train_JS2_arrow.status = FINISHED
            if resp_train_JS2_arrow.status == STARTED and not waitOnFlip:
                theseKeys = resp_train_JS2_arrow.getKeys(keyList=['a', 'l'], waitRelease=False)
                _resp_train_JS2_arrow_allKeys.extend(theseKeys)
                if len(_resp_train_JS2_arrow_allKeys):
                    resp_train_JS2_arrow.keys = _resp_train_JS2_arrow_allKeys[-1].name  # just the last key pressed
                    resp_train_JS2_arrow.rt = _resp_train_JS2_arrow_allKeys[-1].rt
                    # was this correct?
                    if (resp_train_JS2_arrow.keys == str(key_corr)) or (resp_train_JS2_arrow.keys == key_corr):
                        resp_train_JS2_arrow.corr = 1
                    else:
                        resp_train_JS2_arrow.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in Train_JS2_ArrowComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Train_JS2_Arrow"-------
        for thisComponent in Train_JS2_ArrowComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        Train_JS2_Arrow_Rep.addData('fixation_cross_train_JS2_2.started', fixation_cross_train_JS2_2.tStartRefresh)
        Train_JS2_Arrow_Rep.addData('fixation_cross_train_JS2_2.stopped', fixation_cross_train_JS2_2.tStopRefresh)
        Train_JS2_Arrow_Rep.addData('arrow_image_train_JS2.started', arrow_image_train_JS2.tStartRefresh)
        Train_JS2_Arrow_Rep.addData('arrow_image_train_JS2.stopped', arrow_image_train_JS2.tStopRefresh)
        Train_JS2_Arrow_Rep.addData('train_JS2_text_arrow.started', train_JS2_text_arrow.tStartRefresh)
        Train_JS2_Arrow_Rep.addData('train_JS2_text_arrow.stopped', train_JS2_text_arrow.tStopRefresh)
        # check responses
        if resp_train_JS2_arrow.keys in ['', [], None]:  # No response was made
            resp_train_JS2_arrow.keys = None
            # was no response the correct answer?!
            if str(key_corr).lower() == 'none':
               resp_train_JS2_arrow.corr = 1;  # correct non-response
            else:
               resp_train_JS2_arrow.corr = 0;  # failed to respond (incorrectly)
        # store data for Train_JS2_Arrow_Rep (TrialHandler)
        Train_JS2_Arrow_Rep.addData('resp_train_JS2_arrow.keys',resp_train_JS2_arrow.keys)
        Train_JS2_Arrow_Rep.addData('resp_train_JS2_arrow.corr', resp_train_JS2_arrow.corr)
        if resp_train_JS2_arrow.keys != None:  # we had a response
            Train_JS2_Arrow_Rep.addData('resp_train_JS2_arrow.rt', resp_train_JS2_arrow.rt)
        Train_JS2_Arrow_Rep.addData('resp_train_JS2_arrow.started', resp_train_JS2_arrow.tStartRefresh)
        Train_JS2_Arrow_Rep.addData('resp_train_JS2_arrow.stopped', resp_train_JS2_arrow.tStopRefresh)
        thisExp.nextEntry()
        
    # completed 2 repeats of 'Train_JS2_Arrow_Rep'
    
    # get names of stimulus parameters
    if Train_JS2_Arrow_Rep.trialList in ([], [None], None):
        params = []
    else:
        params = Train_JS2_Arrow_Rep.trialList[0].keys()
    # save data for this loop
    Train_JS2_Arrow_Rep.saveAsText(filename + 'Train_JS2_Arrow_Rep.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 100 repeats of 'Seq_Train_JS2'

# get names of stimulus parameters
if Seq_Train_JS2.trialList in ([], [None], None):
    params = []
else:
    params = Seq_Train_JS2.trialList[0].keys()
# save data for this loop
Seq_Train_JS2.saveAsText(filename + 'Seq_Train_JS2.csv', delim=',',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "Instructions_JS2_4"-------
continueRoutine = True
# update component parameters for each repeat
resp_next_JS2_4.keys = []
resp_next_JS2_4.rt = []
_resp_next_JS2_4_allKeys = []
# keep track of which components have finished
Instructions_JS2_4Components = [intro_JS2_4, resp_next_JS2_4]
for thisComponent in Instructions_JS2_4Components:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
Instructions_JS2_4Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "Instructions_JS2_4"-------
while continueRoutine:
    # get current time
    t = Instructions_JS2_4Clock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=Instructions_JS2_4Clock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *intro_JS2_4* updates
    if intro_JS2_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        intro_JS2_4.frameNStart = frameN  # exact frame index
        intro_JS2_4.tStart = t  # local t and not account for scr refresh
        intro_JS2_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(intro_JS2_4, 'tStartRefresh')  # time at next scr refresh
        intro_JS2_4.setAutoDraw(True)
    
    # *resp_next_JS2_4* updates
    waitOnFlip = False
    if resp_next_JS2_4.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
        # keep track of start time/frame for later
        resp_next_JS2_4.frameNStart = frameN  # exact frame index
        resp_next_JS2_4.tStart = t  # local t and not account for scr refresh
        resp_next_JS2_4.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(resp_next_JS2_4, 'tStartRefresh')  # time at next scr refresh
        resp_next_JS2_4.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(resp_next_JS2_4.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(resp_next_JS2_4.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if resp_next_JS2_4.status == STARTED and not waitOnFlip:
        theseKeys = resp_next_JS2_4.getKeys(keyList=['space'], waitRelease=False)
        _resp_next_JS2_4_allKeys.extend(theseKeys)
        if len(_resp_next_JS2_4_allKeys):
            resp_next_JS2_4.keys = _resp_next_JS2_4_allKeys[-1].name  # just the last key pressed
            resp_next_JS2_4.rt = _resp_next_JS2_4_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in Instructions_JS2_4Components:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "Instructions_JS2_4"-------
for thisComponent in Instructions_JS2_4Components:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('intro_JS2_4.started', intro_JS2_4.tStartRefresh)
thisExp.addData('intro_JS2_4.stopped', intro_JS2_4.tStopRefresh)
# check responses
if resp_next_JS2_4.keys in ['', [], None]:  # No response was made
    resp_next_JS2_4.keys = None
thisExp.addData('resp_next_JS2_4.keys',resp_next_JS2_4.keys)
if resp_next_JS2_4.keys != None:  # we had a response
    thisExp.addData('resp_next_JS2_4.rt', resp_next_JS2_4.rt)
thisExp.addData('resp_next_JS2_4.started', resp_next_JS2_4.tStartRefresh)
thisExp.addData('resp_next_JS2_4.stopped', resp_next_JS2_4.tStopRefresh)
thisExp.nextEntry()
# the Routine "Instructions_JS2_4" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
Seq_Test_JS2 = data.TrialHandler(nReps=1, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('Estimulos.xlsx', selection=':90'),
    seed=None, name='Seq_Test_JS2')
thisExp.addLoop(Seq_Test_JS2)  # add the loop to the experiment
thisSeq_Test_JS2 = Seq_Test_JS2.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisSeq_Test_JS2.rgb)
if thisSeq_Test_JS2 != None:
    for paramName in thisSeq_Test_JS2:
        exec('{} = thisSeq_Test_JS2[paramName]'.format(paramName))

for thisSeq_Test_JS2 in Seq_Test_JS2:
    currentLoop = Seq_Test_JS2
    # abbreviate parameter names if possible (e.g. rgb = thisSeq_Test_JS2.rgb)
    if thisSeq_Test_JS2 != None:
        for paramName in thisSeq_Test_JS2:
            exec('{} = thisSeq_Test_JS2[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Test_JS2"-------
    continueRoutine = True
    # update component parameters for each repeat
    if keys_resp.index('a')==0 and RESPOSTA_JS2_TESTE==1:
        key_corr = 'a' 
    elif keys_resp.index('l')==1 and RESPOSTA_JS2_TESTE==0:
        key_corr = 'l'
    elif keys_resp.index('l')==0 and RESPOSTA_JS2_TESTE==1:
        key_corr = 'l' 
    elif keys_resp.index('a')==1 and RESPOSTA_JS2_TESTE==0:
        key_corr = 'a'
    text_test_JS2.setText(VERBO_JS2_TESTE)
    resp_test_JS2.keys = []
    resp_test_JS2.rt = []
    _resp_test_JS2_allKeys = []
    # keep track of which components have finished
    Test_JS2Components = [fixation_cross_test_JS2_1, text_test_JS2, resp_test_JS2]
    for thisComponent in Test_JS2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    Test_JS2Clock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Test_JS2"-------
    while continueRoutine:
        # get current time
        t = Test_JS2Clock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=Test_JS2Clock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *fixation_cross_test_JS2_1* updates
        if fixation_cross_test_JS2_1.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            fixation_cross_test_JS2_1.frameNStart = frameN  # exact frame index
            fixation_cross_test_JS2_1.tStart = t  # local t and not account for scr refresh
            fixation_cross_test_JS2_1.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(fixation_cross_test_JS2_1, 'tStartRefresh')  # time at next scr refresh
            fixation_cross_test_JS2_1.setAutoDraw(True)
        if fixation_cross_test_JS2_1.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > fixation_cross_test_JS2_1.tStartRefresh + 1.0-frameTolerance:
                # keep track of stop time/frame for later
                fixation_cross_test_JS2_1.tStop = t  # not accounting for scr refresh
                fixation_cross_test_JS2_1.frameNStop = frameN  # exact frame index
                win.timeOnFlip(fixation_cross_test_JS2_1, 'tStopRefresh')  # time at next scr refresh
                fixation_cross_test_JS2_1.setAutoDraw(False)
        
        # *text_test_JS2* updates
        if text_test_JS2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            text_test_JS2.frameNStart = frameN  # exact frame index
            text_test_JS2.tStart = t  # local t and not account for scr refresh
            text_test_JS2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_test_JS2, 'tStartRefresh')  # time at next scr refresh
            text_test_JS2.setAutoDraw(True)
        
        # *resp_test_JS2* updates
        waitOnFlip = False
        if resp_test_JS2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
            # keep track of start time/frame for later
            resp_test_JS2.frameNStart = frameN  # exact frame index
            resp_test_JS2.tStart = t  # local t and not account for scr refresh
            resp_test_JS2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(resp_test_JS2, 'tStartRefresh')  # time at next scr refresh
            resp_test_JS2.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(resp_test_JS2.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(resp_test_JS2.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if resp_test_JS2.status == STARTED and not waitOnFlip:
            theseKeys = resp_test_JS2.getKeys(keyList=['a', 'l'], waitRelease=False)
            _resp_test_JS2_allKeys.extend(theseKeys)
            if len(_resp_test_JS2_allKeys):
                resp_test_JS2.keys = _resp_test_JS2_allKeys[-1].name  # just the last key pressed
                resp_test_JS2.rt = _resp_test_JS2_allKeys[-1].rt
                # was this correct?
                if (resp_test_JS2.keys == str(key_corr)) or (resp_test_JS2.keys == key_corr):
                    resp_test_JS2.corr = 1
                else:
                    resp_test_JS2.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in Test_JS2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Test_JS2"-------
    for thisComponent in Test_JS2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    Seq_Test_JS2.addData('fixation_cross_test_JS2_1.started', fixation_cross_test_JS2_1.tStartRefresh)
    Seq_Test_JS2.addData('fixation_cross_test_JS2_1.stopped', fixation_cross_test_JS2_1.tStopRefresh)
    Seq_Test_JS2.addData('text_test_JS2.started', text_test_JS2.tStartRefresh)
    Seq_Test_JS2.addData('text_test_JS2.stopped', text_test_JS2.tStopRefresh)
    # check responses
    if resp_test_JS2.keys in ['', [], None]:  # No response was made
        resp_test_JS2.keys = None
        # was no response the correct answer?!
        if str(key_corr).lower() == 'none':
           resp_test_JS2.corr = 1;  # correct non-response
        else:
           resp_test_JS2.corr = 0;  # failed to respond (incorrectly)
    # store data for Seq_Test_JS2 (TrialHandler)
    Seq_Test_JS2.addData('resp_test_JS2.keys',resp_test_JS2.keys)
    Seq_Test_JS2.addData('resp_test_JS2.corr', resp_test_JS2.corr)
    if resp_test_JS2.keys != None:  # we had a response
        Seq_Test_JS2.addData('resp_test_JS2.rt', resp_test_JS2.rt)
    Seq_Test_JS2.addData('resp_test_JS2.started', resp_test_JS2.tStartRefresh)
    Seq_Test_JS2.addData('resp_test_JS2.stopped', resp_test_JS2.tStopRefresh)
    # the Routine "Test_JS2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    Test_JS2_Arrow_Rep = data.TrialHandler(nReps=2, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=[None],
        seed=None, name='Test_JS2_Arrow_Rep')
    thisExp.addLoop(Test_JS2_Arrow_Rep)  # add the loop to the experiment
    thisTest_JS2_Arrow_Rep = Test_JS2_Arrow_Rep.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisTest_JS2_Arrow_Rep.rgb)
    if thisTest_JS2_Arrow_Rep != None:
        for paramName in thisTest_JS2_Arrow_Rep:
            exec('{} = thisTest_JS2_Arrow_Rep[paramName]'.format(paramName))
    
    for thisTest_JS2_Arrow_Rep in Test_JS2_Arrow_Rep:
        currentLoop = Test_JS2_Arrow_Rep
        # abbreviate parameter names if possible (e.g. rgb = thisTest_JS2_Arrow_Rep.rgb)
        if thisTest_JS2_Arrow_Rep != None:
            for paramName in thisTest_JS2_Arrow_Rep:
                exec('{} = thisTest_JS2_Arrow_Rep[paramName]'.format(paramName))
        
        # ------Prepare to start Routine "Test_JS2_Arrow"-------
        continueRoutine = True
        routineTimer.add(5.350000)
        # update component parameters for each repeat
        if Seq_Test_JS2.thisTrialN % 15 != 0:
            continueRoutine = False
        arrow_angle = 90 * (round(np.random.rand()) * 2 - 1)
        thisExp.addData('test_JS2_arrow_angle', arrow_angle)
        
        if keys_resp.index('a')==0 and arrow_angle == 90:
            key_corr = 'a' 
        elif keys_resp.index('l')==1 and arrow_angle == -90:
            key_corr = 'l'
        elif keys_resp.index('l')==0 and arrow_angle == 90:
            key_corr = 'l' 
        elif keys_resp.index('a')==1 and arrow_angle == -90:
            key_corr = 'a'
        arrow_image_test_JS2.setOri(arrow_angle)
        resp_test_JS2_arrow.keys = []
        resp_test_JS2_arrow.rt = []
        _resp_test_JS2_arrow_allKeys = []
        # keep track of which components have finished
        Test_JS2_ArrowComponents = [fixation_cross_test_JS2, arrow_image_test_JS2, test_JS2_text_arrow, resp_test_JS2_arrow]
        for thisComponent in Test_JS2_ArrowComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        Test_JS2_ArrowClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "Test_JS2_Arrow"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = Test_JS2_ArrowClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=Test_JS2_ArrowClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *fixation_cross_test_JS2* updates
            if fixation_cross_test_JS2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                fixation_cross_test_JS2.frameNStart = frameN  # exact frame index
                fixation_cross_test_JS2.tStart = t  # local t and not account for scr refresh
                fixation_cross_test_JS2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(fixation_cross_test_JS2, 'tStartRefresh')  # time at next scr refresh
                fixation_cross_test_JS2.setAutoDraw(True)
            if fixation_cross_test_JS2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > fixation_cross_test_JS2.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    fixation_cross_test_JS2.tStop = t  # not accounting for scr refresh
                    fixation_cross_test_JS2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(fixation_cross_test_JS2, 'tStopRefresh')  # time at next scr refresh
                    fixation_cross_test_JS2.setAutoDraw(False)
            
            # *arrow_image_test_JS2* updates
            if arrow_image_test_JS2.status == NOT_STARTED and tThisFlip >= 1-frameTolerance:
                # keep track of start time/frame for later
                arrow_image_test_JS2.frameNStart = frameN  # exact frame index
                arrow_image_test_JS2.tStart = t  # local t and not account for scr refresh
                arrow_image_test_JS2.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(arrow_image_test_JS2, 'tStartRefresh')  # time at next scr refresh
                arrow_image_test_JS2.setAutoDraw(True)
            if arrow_image_test_JS2.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > arrow_image_test_JS2.tStartRefresh + .350-frameTolerance:
                    # keep track of stop time/frame for later
                    arrow_image_test_JS2.tStop = t  # not accounting for scr refresh
                    arrow_image_test_JS2.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(arrow_image_test_JS2, 'tStopRefresh')  # time at next scr refresh
                    arrow_image_test_JS2.setAutoDraw(False)
            
            # *test_JS2_text_arrow* updates
            if test_JS2_text_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                test_JS2_text_arrow.frameNStart = frameN  # exact frame index
                test_JS2_text_arrow.tStart = t  # local t and not account for scr refresh
                test_JS2_text_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(test_JS2_text_arrow, 'tStartRefresh')  # time at next scr refresh
                test_JS2_text_arrow.setAutoDraw(True)
            if test_JS2_text_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > test_JS2_text_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    test_JS2_text_arrow.tStop = t  # not accounting for scr refresh
                    test_JS2_text_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(test_JS2_text_arrow, 'tStopRefresh')  # time at next scr refresh
                    test_JS2_text_arrow.setAutoDraw(False)
            
            # *resp_test_JS2_arrow* updates
            waitOnFlip = False
            if resp_test_JS2_arrow.status == NOT_STARTED and tThisFlip >= 1.35-frameTolerance:
                # keep track of start time/frame for later
                resp_test_JS2_arrow.frameNStart = frameN  # exact frame index
                resp_test_JS2_arrow.tStart = t  # local t and not account for scr refresh
                resp_test_JS2_arrow.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_test_JS2_arrow, 'tStartRefresh')  # time at next scr refresh
                resp_test_JS2_arrow.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(resp_test_JS2_arrow.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(resp_test_JS2_arrow.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if resp_test_JS2_arrow.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_test_JS2_arrow.tStartRefresh + 4-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_test_JS2_arrow.tStop = t  # not accounting for scr refresh
                    resp_test_JS2_arrow.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_test_JS2_arrow, 'tStopRefresh')  # time at next scr refresh
                    resp_test_JS2_arrow.status = FINISHED
            if resp_test_JS2_arrow.status == STARTED and not waitOnFlip:
                theseKeys = resp_test_JS2_arrow.getKeys(keyList=['a', 'l'], waitRelease=False)
                _resp_test_JS2_arrow_allKeys.extend(theseKeys)
                if len(_resp_test_JS2_arrow_allKeys):
                    resp_test_JS2_arrow.keys = _resp_test_JS2_arrow_allKeys[-1].name  # just the last key pressed
                    resp_test_JS2_arrow.rt = _resp_test_JS2_arrow_allKeys[-1].rt
                    # was this correct?
                    if (resp_test_JS2_arrow.keys == str(key_corr)) or (resp_test_JS2_arrow.keys == key_corr):
                        resp_test_JS2_arrow.corr = 1
                    else:
                        resp_test_JS2_arrow.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in Test_JS2_ArrowComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "Test_JS2_Arrow"-------
        for thisComponent in Test_JS2_ArrowComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        Test_JS2_Arrow_Rep.addData('fixation_cross_test_JS2.started', fixation_cross_test_JS2.tStartRefresh)
        Test_JS2_Arrow_Rep.addData('fixation_cross_test_JS2.stopped', fixation_cross_test_JS2.tStopRefresh)
        Test_JS2_Arrow_Rep.addData('arrow_image_test_JS2.started', arrow_image_test_JS2.tStartRefresh)
        Test_JS2_Arrow_Rep.addData('arrow_image_test_JS2.stopped', arrow_image_test_JS2.tStopRefresh)
        Test_JS2_Arrow_Rep.addData('test_JS2_text_arrow.started', test_JS2_text_arrow.tStartRefresh)
        Test_JS2_Arrow_Rep.addData('test_JS2_text_arrow.stopped', test_JS2_text_arrow.tStopRefresh)
        # check responses
        if resp_test_JS2_arrow.keys in ['', [], None]:  # No response was made
            resp_test_JS2_arrow.keys = None
            # was no response the correct answer?!
            if str(key_corr).lower() == 'none':
               resp_test_JS2_arrow.corr = 1;  # correct non-response
            else:
               resp_test_JS2_arrow.corr = 0;  # failed to respond (incorrectly)
        # store data for Test_JS2_Arrow_Rep (TrialHandler)
        Test_JS2_Arrow_Rep.addData('resp_test_JS2_arrow.keys',resp_test_JS2_arrow.keys)
        Test_JS2_Arrow_Rep.addData('resp_test_JS2_arrow.corr', resp_test_JS2_arrow.corr)
        if resp_test_JS2_arrow.keys != None:  # we had a response
            Test_JS2_Arrow_Rep.addData('resp_test_JS2_arrow.rt', resp_test_JS2_arrow.rt)
        Test_JS2_Arrow_Rep.addData('resp_test_JS2_arrow.started', resp_test_JS2_arrow.tStartRefresh)
        Test_JS2_Arrow_Rep.addData('resp_test_JS2_arrow.stopped', resp_test_JS2_arrow.tStopRefresh)
        thisExp.nextEntry()
        
    # completed 2 repeats of 'Test_JS2_Arrow_Rep'
    
    # get names of stimulus parameters
    if Test_JS2_Arrow_Rep.trialList in ([], [None], None):
        params = []
    else:
        params = Test_JS2_Arrow_Rep.trialList[0].keys()
    # save data for this loop
    Test_JS2_Arrow_Rep.saveAsText(filename + 'Test_JS2_Arrow_Rep.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    thisExp.nextEntry()
    
# completed 1 repeats of 'Seq_Test_JS2'

# get names of stimulus parameters
if Seq_Test_JS2.trialList in ([], [None], None):
    params = []
else:
    params = Seq_Test_JS2.trialList[0].keys()
# save data for this loop
Seq_Test_JS2.saveAsText(filename + 'Seq_Test_JS2.csv', delim=',',
    stimOut=params,
    dataOut=['n','all_mean','all_std', 'all_raw'])

# ------Prepare to start Routine "End"-------
continueRoutine = True
# update component parameters for each repeat
next_end_resp.keys = []
next_end_resp.rt = []
_next_end_resp_allKeys = []
# keep track of which components have finished
EndComponents = [end, next_end, next_end_resp]
for thisComponent in EndComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
EndClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "End"-------
while continueRoutine:
    # get current time
    t = EndClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=EndClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *end* updates
    if end.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        end.frameNStart = frameN  # exact frame index
        end.tStart = t  # local t and not account for scr refresh
        end.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(end, 'tStartRefresh')  # time at next scr refresh
        end.setAutoDraw(True)
    
    # *next_end* updates
    if next_end.status == NOT_STARTED and tThisFlip >= .5-frameTolerance:
        # keep track of start time/frame for later
        next_end.frameNStart = frameN  # exact frame index
        next_end.tStart = t  # local t and not account for scr refresh
        next_end.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_end, 'tStartRefresh')  # time at next scr refresh
        next_end.setAutoDraw(True)
    
    # *next_end_resp* updates
    waitOnFlip = False
    if next_end_resp.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
        # keep track of start time/frame for later
        next_end_resp.frameNStart = frameN  # exact frame index
        next_end_resp.tStart = t  # local t and not account for scr refresh
        next_end_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(next_end_resp, 'tStartRefresh')  # time at next scr refresh
        next_end_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(next_end_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(next_end_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if next_end_resp.status == STARTED and not waitOnFlip:
        theseKeys = next_end_resp.getKeys(keyList=['space'], waitRelease=False)
        _next_end_resp_allKeys.extend(theseKeys)
        if len(_next_end_resp_allKeys):
            next_end_resp.keys = _next_end_resp_allKeys[-1].name  # just the last key pressed
            next_end_resp.rt = _next_end_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in EndComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "End"-------
for thisComponent in EndComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('end.started', end.tStartRefresh)
thisExp.addData('end.stopped', end.tStopRefresh)
thisExp.addData('next_end.started', next_end.tStartRefresh)
thisExp.addData('next_end.stopped', next_end.tStopRefresh)
# check responses
if next_end_resp.keys in ['', [], None]:  # No response was made
    next_end_resp.keys = None
thisExp.addData('next_end_resp.keys',next_end_resp.keys)
if next_end_resp.keys != None:  # we had a response
    thisExp.addData('next_end_resp.rt', next_end_resp.rt)
thisExp.addData('next_end_resp.started', next_end_resp.tStartRefresh)
thisExp.addData('next_end_resp.stopped', next_end_resp.tStopRefresh)
thisExp.nextEntry()
# the Routine "End" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv', delim='comma')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
