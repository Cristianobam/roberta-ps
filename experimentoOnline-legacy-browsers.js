﻿/************************** 
 * Experimentoonline Test *
 **************************/

// init psychoJS:
const psychoJS = new PsychoJS({
  debug: true
});

// open window:
psychoJS.openWindow({
  fullscr: true,
  color: new util.Color([0, 0, 0]),
  units: 'deg',
  waitBlanking: true
});

// store info about the experiment session:
let expName = 'experimentoOnline';  // from the Builder filename that created this script
let expInfo = {'Participante': ''};

// Start code blocks for 'Before Experiment'
// schedule the experiment:
psychoJS.schedule(psychoJS.gui.DlgFromDict({
  dictionary: expInfo,
  title: expName
}));

const flowScheduler = new Scheduler(psychoJS);
const dialogCancelScheduler = new Scheduler(psychoJS);
psychoJS.scheduleCondition(function() { return (psychoJS.gui.dialogComponent.button === 'OK'); }, flowScheduler, dialogCancelScheduler);

// flowScheduler gets run if the participants presses OK
flowScheduler.add(updateInfo); // add timeStamp
flowScheduler.add(experimentInit);
flowScheduler.add(WelcomeRoutineBegin());
flowScheduler.add(WelcomeRoutineEachFrame());
flowScheduler.add(WelcomeRoutineEnd());
flowScheduler.add(Instructions_DL1RoutineBegin());
flowScheduler.add(Instructions_DL1RoutineEachFrame());
flowScheduler.add(Instructions_DL1RoutineEnd());
flowScheduler.add(Instructions_DL2RoutineBegin());
flowScheduler.add(Instructions_DL2RoutineEachFrame());
flowScheduler.add(Instructions_DL2RoutineEnd());
flowScheduler.add(Instructions_DL3RoutineBegin());
flowScheduler.add(Instructions_DL3RoutineEachFrame());
flowScheduler.add(Instructions_DL3RoutineEnd());
flowScheduler.add(Instructions_DL4RoutineBegin());
flowScheduler.add(Instructions_DL4RoutineEachFrame());
flowScheduler.add(Instructions_DL4RoutineEnd());
flowScheduler.add(Instructions_DL5RoutineBegin());
flowScheduler.add(Instructions_DL5RoutineEachFrame());
flowScheduler.add(Instructions_DL5RoutineEnd());
flowScheduler.add(Instructions_DL6RoutineBegin());
flowScheduler.add(Instructions_DL6RoutineEachFrame());
flowScheduler.add(Instructions_DL6RoutineEnd());
flowScheduler.add(Instructions_DL7RoutineBegin());
flowScheduler.add(Instructions_DL7RoutineEachFrame());
flowScheduler.add(Instructions_DL7RoutineEnd());
const Seq_Train_DLLoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(Seq_Train_DLLoopBegin, Seq_Train_DLLoopScheduler);
flowScheduler.add(Seq_Train_DLLoopScheduler);
flowScheduler.add(Seq_Train_DLLoopEnd);
flowScheduler.add(Instructions_DL8RoutineBegin());
flowScheduler.add(Instructions_DL8RoutineEachFrame());
flowScheduler.add(Instructions_DL8RoutineEnd());
const Seq_Test_DLLoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(Seq_Test_DLLoopBegin, Seq_Test_DLLoopScheduler);
flowScheduler.add(Seq_Test_DLLoopScheduler);
flowScheduler.add(Seq_Test_DLLoopEnd);
flowScheduler.add(Instructions_JS1_1RoutineBegin());
flowScheduler.add(Instructions_JS1_1RoutineEachFrame());
flowScheduler.add(Instructions_JS1_1RoutineEnd());
flowScheduler.add(Instructions_JS1_2RoutineBegin());
flowScheduler.add(Instructions_JS1_2RoutineEachFrame());
flowScheduler.add(Instructions_JS1_2RoutineEnd());
flowScheduler.add(Instructions_JS1_3RoutineBegin());
flowScheduler.add(Instructions_JS1_3RoutineEachFrame());
flowScheduler.add(Instructions_JS1_3RoutineEnd());
const Seq_Train_JS1LoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(Seq_Train_JS1LoopBegin, Seq_Train_JS1LoopScheduler);
flowScheduler.add(Seq_Train_JS1LoopScheduler);
flowScheduler.add(Seq_Train_JS1LoopEnd);
flowScheduler.add(Instructions_JS1_4RoutineBegin());
flowScheduler.add(Instructions_JS1_4RoutineEachFrame());
flowScheduler.add(Instructions_JS1_4RoutineEnd());
const Seq_Test_JS1LoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(Seq_Test_JS1LoopBegin, Seq_Test_JS1LoopScheduler);
flowScheduler.add(Seq_Test_JS1LoopScheduler);
flowScheduler.add(Seq_Test_JS1LoopEnd);
flowScheduler.add(Instructions_JS2_1RoutineBegin());
flowScheduler.add(Instructions_JS2_1RoutineEachFrame());
flowScheduler.add(Instructions_JS2_1RoutineEnd());
flowScheduler.add(Instructions_JS2_2RoutineBegin());
flowScheduler.add(Instructions_JS2_2RoutineEachFrame());
flowScheduler.add(Instructions_JS2_2RoutineEnd());
flowScheduler.add(Instructions_JS2_3RoutineBegin());
flowScheduler.add(Instructions_JS2_3RoutineEachFrame());
flowScheduler.add(Instructions_JS2_3RoutineEnd());
const Seq_Train_JS2LoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(Seq_Train_JS2LoopBegin, Seq_Train_JS2LoopScheduler);
flowScheduler.add(Seq_Train_JS2LoopScheduler);
flowScheduler.add(Seq_Train_JS2LoopEnd);
flowScheduler.add(Instructions_JS2_4RoutineBegin());
flowScheduler.add(Instructions_JS2_4RoutineEachFrame());
flowScheduler.add(Instructions_JS2_4RoutineEnd());
const Seq_Test_JS2LoopScheduler = new Scheduler(psychoJS);
flowScheduler.add(Seq_Test_JS2LoopBegin, Seq_Test_JS2LoopScheduler);
flowScheduler.add(Seq_Test_JS2LoopScheduler);
flowScheduler.add(Seq_Test_JS2LoopEnd);
flowScheduler.add(EndRoutineBegin());
flowScheduler.add(EndRoutineEachFrame());
flowScheduler.add(EndRoutineEnd());
flowScheduler.add(quitPsychoJS, '', true);

// quit if user presses Cancel in dialog box:
dialogCancelScheduler.add(quitPsychoJS, '', false);

psychoJS.start({
  expName: expName,
  expInfo: expInfo,
  resources: [
    {'name': 'Estimulos.xlsx', 'path': 'Estimulos.xlsx'},
    {'name': 'arrow.png', 'path': 'arrow.png'}
  ]
});

psychoJS.experimentLogger.setLevel(core.Logger.ServerLevel.DEBUG);

function updateInfo() {
  expInfo['date'] = util.MonotonicClock.getDateStr();  // add a simple timestamp
  expInfo['expName'] = expName;
  expInfo['psychopyVersion'] = '2020.2.10';
  expInfo['OS'] = window.navigator.platform;

  // store frame rate of monitor if we can measure it successfully
  expInfo['frameRate'] = psychoJS.window.getActualFrameRate();
  if (typeof expInfo['frameRate'] !== 'undefined')
    frameDur = 1.0 / Math.round(expInfo['frameRate']);
  else
    frameDur = 1.0 / 60.0; // couldn't get a reliable measure so guess

  // add info from the URL:
  util.addInfoFromUrl(expInfo);
  
  return Scheduler.Event.NEXT;
}

function experimentInit() {
  // Initialize components for Routine "Welcome"
  WelcomeClock = new util.Clock();
  intro = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro',
    text: 'Olá, participante!\n\nVamos começar o experimento?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 6], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_welcome = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_welcome',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  next_welcome_resp = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  import * as np from 'numpy';
  var keys_resp;
  if ((np.random.rand() > 0.5)) {
      keys_resp = ["l", "a"];
      thisExp.addData("Keys", keys_resp);
  } else {
      keys_resp = ["a", "l"];
      thisExp.addData("Keys", keys_resp);
  }
  
  // Initialize components for Routine "Instructions_DL1"
  Instructions_DL1Clock = new util.Clock();
  DL = new visual.TextStim({
    win: psychoJS.window,
    name: 'DL',
    text: 'Tarefa de Decisão Lexical',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 2,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  intro_DL1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL1',
    text: 'No centro da tela você verá uma CRUZ BRANCA para indicar o local aproximado em que deve fixar seu olhar. \n\nLogo depois, uma palavra aparecerá e você deverá decidir, o mais rapidamente possível, se esta palavra existe ou não em português.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  next_DL1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_DL1',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_next_DL1 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL2"
  Instructions_DL2Clock = new util.Clock();
  intro_DL2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL2',
    text: 'Todas as palavras que aparecerão serão verbos ou pseudoverbos (verbos que não existem em português, mas que parecem com verbos por conta de sua terminação) e após sua aparição você deve responder se a palavra lida era um verbo.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_DL2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_DL2',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_DL2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL3"
  Instructions_DL3Clock = new util.Clock();
  intro_DL3 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL3',
    text: f'''
  Se você julgar que o que leu foi um VERBO, aperte, O MAIS RÁPIDO POSSÍVEL, em seu teclado, a TECLA {keys_resp[0].upper()}. 
  
  Caso tenha lido um verbo que NÃO EXISTA, aperte a TECLA {keys_resp[1].upper()}.
  ''',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_DL3 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_DL3',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_DL3 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL4"
  Instructions_DL4Clock = new util.Clock();
  intro_DL4 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL4',
    text: f'''
  Por exemplo: 
  
  - Se após a cruz branca você ler BEIJAR, esse é um verbo pertencente ao vocabulário da Língua Portuguesa, então você deve, nesse caso, apertar a tecla {keys_resp[0].upper()}. 
  
  - Caso após a cruz branca você ler BAIJER, esse NÃO é um verbo pertencente ao vocabulário da Língua Portuguesa, então você deve, nesse caso, apertar a tecla {keys_resp[1].upper()}.
  ''',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_DL4 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_DL4',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_DL4 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL5"
  Instructions_DL5Clock = new util.Clock();
  intro_DL5 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL5',
    text: 'IMPORTANTE: Em alguns momentos, durante o andamento da tarefa, você verá na tela uma seta que pode apontar para cima ou para baixo. Após o aparecimento da seta, haverá a pergunta: A SETA APONTA PARA CIMA?  \n \nNeste momento, de acordo com a orientação da seta, você também usará as mesmas teclas do teclado correspondentes ao SIM e ao NÃO para responder.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_DL5 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_DL5',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_DL5 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL6"
  Instructions_DL6Clock = new util.Clock();
  intro_DL6 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL6',
    text: 'Antes da tarefa começar, você poderá treinar um pouco para garantir que entendeu a dinâmica desta parte do experimento.\n \nLembre-se: sua tarefa agora é a de julgar, o mais rápido possível, se o que apareceu escrito na tela foi um verbo em português ou simplesmente uma sequência de letras que não faz sentido.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_DL6 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_DL6',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_DL6 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL7"
  Instructions_DL7Clock = new util.Clock();
  intro_DL7 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL7',
    text: 'Procure responder o mais rápido possível e já posicione e mantenha sempre seus dedos indicadores  sobre as teclas referentes às possíveis respostas ( tecla A ou L).\n \nVamos começar? Quando estiver pronto para treinar, APERTE ESPAÇO PARA PROSSEGUIR',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  resp_next_DL7 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Train_DL"
  Train_DLClock = new util.Clock();
  fixation_cross_train_DL = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_train_DL', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -1, interpolate: true,
  });
  
  text_train_DL = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_train_DL',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1.31,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_train_DL = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Feedback_Train_DL"
  Feedback_Train_DLClock = new util.Clock();
  respcorrs = [];
  
  msg = "doh!";
  
  feedback_text_train_DL = new visual.TextStim({
    win: psychoJS.window,
    name: 'feedback_text_train_DL',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "Train_DL_Arrow"
  Train_DL_ArrowClock = new util.Clock();
  fixation_cross_train_DL_2 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_train_DL_2', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -2, interpolate: true,
  });
  
  arrow_image_train_DL = new visual.ImageStim({
    win : psychoJS.window,
    name : 'arrow_image_train_DL', units : undefined, 
    image : 'arrow.png', mask : undefined,
    ori : 1.0, pos : [0, 0], size : [4, 6],
    color : new util.Color([1, 1, 1]), opacity : 1,
    flipHoriz : false, flipVert : false,
    texRes : 512, interpolate : true, depth : -3.0 
  });
  train_DL_text_arrow = new visual.TextStim({
    win: psychoJS.window,
    name: 'train_DL_text_arrow',
    text: 'A seta estava apontando para cima?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -4.0 
  });
  
  resp_train_DL_arrow = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_DL8"
  Instructions_DL8Clock = new util.Clock();
  intro_DL8 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_DL8',
    text: 'Fim do Treino!\n\nLembre-se: sua tarefa é a de julgar, o mais rápido possível, se o que apareceu escrito foi um verbo em português ou simplesmente uma sequência de letras que não faz sentido.\n \nQuando estiver pronto, APERTE ESPAÇO PARA DAR INÍCIO ao teste e sempre mantenha seus dedos indicadores posicionados sobre as teclas A e L.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  resp_next_DL8 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Test_DL"
  Test_DLClock = new util.Clock();
  fixation_cross_test_DL = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_test_DL', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -1, interpolate: true,
  });
  
  text_test_DL = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_test_DL',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1.31,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_test_DL = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Test_DL_Arrow"
  Test_DL_ArrowClock = new util.Clock();
  fixation_cross_test_DL_2 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_test_DL_2', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -2, interpolate: true,
  });
  
  arrow_image_test_DL = new visual.ImageStim({
    win : psychoJS.window,
    name : 'arrow_image_test_DL', units : undefined, 
    image : 'arrow.png', mask : undefined,
    ori : 1.0, pos : [0, 0], size : [4, 6],
    color : new util.Color([1, 1, 1]), opacity : 1,
    flipHoriz : false, flipVert : false,
    texRes : 512, interpolate : true, depth : -3.0 
  });
  test_DL_text_arrow = new visual.TextStim({
    win: psychoJS.window,
    name: 'test_DL_text_arrow',
    text: 'A seta estava apontando para cima?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -4.0 
  });
  
  resp_test_DL_arrow = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS1_1"
  Instructions_JS1_1Clock = new util.Clock();
  JS1_1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'JS1_1',
    text: 'Tarefa de Julgamento Semântico\n\nParte 1',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 2,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  intro_JS1_1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS1_1',
    text: 'Esta etapa é semelhante à anterior, só que agora, no centro da tela você verá uma CRUZ BRANCA, depois dela surgirá um verbo e você deve responder, usando as mesmas teclas de antes (A ou L), se esse verbo para ser feito utiliza ESPECIFICAMENTE A BOCA, O NARIZ OU AS MÃOS.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  next_JS1_1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_JS1_1',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_next_JS1_1 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS1_2"
  Instructions_JS1_2Clock = new util.Clock();
  intro_JS1_2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS1_2',
    text: f'''
  Se A RESPOSTA FOR NÃO, aperte, o MAIS RÁPIDO POSSÍVEL, a tecla {keys_resp[1].upper()}.
  
  Se A AÇÃO UTILIZAR UMA DESSAS PARTES DO CORPO (BOCA, NARIZ OU MÃO), A RESPOSTA SERÁ SIM,  ENTÃO aperte, o MAIS RÁPIDO POSSÍVEL, a tecla {keys_resp[0].upper()}.
  ''',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_JS1_2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_JS1_2',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_JS1_2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS1_3"
  Instructions_JS1_3Clock = new util.Clock();
  intro_JS1_3 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS1_3',
    text: 'Vamos lá? Os verbos lidos a seguir são verbos que utilizam ESPECIFICAMENTE boca, nariz ou mãos para serem executados?\n \nQuando estiver pronto para responder, aperte espaço para iniciar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  resp_next_JS1_3 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Train_JS1"
  Train_JS1Clock = new util.Clock();
  fixation_cross_train_JS1 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_train_JS1', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -1, interpolate: true,
  });
  
  text_train_JS1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_train_JS1',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1.31,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_train_JS1 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Feedback_Train_JS1"
  Feedback_Train_JS1Clock = new util.Clock();
  respcorrs = [];
  
  feedback_text_train_JS1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'feedback_text_train_JS1',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "Train_JS1_Arrow"
  Train_JS1_ArrowClock = new util.Clock();
  fixation_cross_train_JS1_2 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_train_JS1_2', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -2, interpolate: true,
  });
  
  arrow_image_train_JS1 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'arrow_image_train_JS1', units : undefined, 
    image : 'arrow.png', mask : undefined,
    ori : 1.0, pos : [0, 0], size : [4, 6],
    color : new util.Color([1, 1, 1]), opacity : 1,
    flipHoriz : false, flipVert : false,
    texRes : 512, interpolate : true, depth : -3.0 
  });
  train_JS1_text_arrow = new visual.TextStim({
    win: psychoJS.window,
    name: 'train_JS1_text_arrow',
    text: 'A seta estava apontando para cima?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -4.0 
  });
  
  resp_train_JS1_arrow = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS1_4"
  Instructions_JS1_4Clock = new util.Clock();
  intro_JS1_4 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS1_4',
    text: 'Fim do treino!\n \nLembre-se: sua tarefa agora é a de julgar, o mais rápido possível, se os verbos lidos são verbos que utilizam ESPECIFICAMENTE boca, nariz ou mãos para serem executados?\n \nQuando estiver pronto, APERTE ESPAÇO PARA DAR INÍCIO ao teste e sempre mantenha seus dedos indicadores posicionados sobre as teclas A e L.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  resp_next_JS1_4 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Test_JS1"
  Test_JS1Clock = new util.Clock();
  fixation_cross_test_JS1_1 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_test_JS1_1', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -1, interpolate: true,
  });
  
  text_test_JS1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_test_JS1',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1.31,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_test_JS1 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Test_JS1_Arrow"
  Test_JS1_ArrowClock = new util.Clock();
  fixation_cross_test_JS1 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_test_JS1', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -2, interpolate: true,
  });
  
  arrow_image_test_JS1 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'arrow_image_test_JS1', units : undefined, 
    image : 'arrow.png', mask : undefined,
    ori : 1.0, pos : [0, 0], size : [4, 6],
    color : new util.Color([1, 1, 1]), opacity : 1,
    flipHoriz : false, flipVert : false,
    texRes : 512, interpolate : true, depth : -3.0 
  });
  test_JS1_text_arrow = new visual.TextStim({
    win: psychoJS.window,
    name: 'test_JS1_text_arrow',
    text: 'A seta estava apontando para cima?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -4.0 
  });
  
  resp_test_JS1_arrow = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS2_1"
  Instructions_JS2_1Clock = new util.Clock();
  JS2_1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'JS2_1',
    text: 'Tarefa de Julgamento Semântico\n\nParte 2',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 2,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  intro_JS2_1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS2_1',
    text: f'''
  Nesta última fase da tarefa, você lerá na tela um par de verbos e deve responder se esses dois verbos são sinônimos entre si. 
  
  São considerados sinônimos, os verbos que têm sentido semelhante.
  
  Se o verbo NÃO for sinônimo, aperte o MAIS RÁPIDO POSSÍVEL, a TECLA {keys_resp[1].upper()}.
  Se o verbo for sinônimo, aperte o MAIS RÁPIDO POSSÍVEL, a TECLA {keys_resp[0].upper()}.
  ''',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  next_JS2_1 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_JS2_1',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_next_JS2_1 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS2_2"
  Instructions_JS2_2Clock = new util.Clock();
  intro_JS2_2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS2_2',
    text: f'''
  Por exemplo:
   
  Se após a cruz branca, aparecer o par de verbos AMAR-SAMBAR, você deve apertar a  TECLA {keys_resp[1].upper()},  porque amar e sambar NÃO  são sinônimos.
   
  Se após a cruz branca, aparecer o par de verbos AMAR-ADORAR, você deve apertar a TECLA {keys_resp[0].upper()},  porque amar e adorar SÃO  sinônimos.
  ''',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_JS2_2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_JS2_2',
    text: 'Pressione ESPAÇO para continuar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  resp_next_JS2_2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS2_3"
  Instructions_JS2_3Clock = new util.Clock();
  intro_JS2_3 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS2_3',
    text: 'IMPORTANTE: procure responder o mais rápido possível e mantenha sempre seus dedos indicadores  sobre as teclas referentes às possíveis respostas (tecla A ou L).\n \nNovamente, antes de começar o registro oficial de respostas, haverá um treino para que você entenda a dinâmica da tarefa.\n \nVamos lá?\n \nAperte espaço para prosseguir.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  resp_next_JS2_3 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Train_JS2"
  Train_JS2Clock = new util.Clock();
  fixation_cross_train_JS2 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_train_JS2', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -1, interpolate: true,
  });
  
  text_train_JS2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_train_JS2',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_train_JS2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Feedback_Train_JS2"
  Feedback_Train_JS2Clock = new util.Clock();
  respcorrs = [];
  
  feedback_text_train_JS2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'feedback_text_train_JS2',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  // Initialize components for Routine "Train_JS2_Arrow"
  Train_JS2_ArrowClock = new util.Clock();
  fixation_cross_train_JS2_2 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_train_JS2_2', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -2, interpolate: true,
  });
  
  arrow_image_train_JS2 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'arrow_image_train_JS2', units : undefined, 
    image : 'arrow.png', mask : undefined,
    ori : 1.0, pos : [0, 0], size : [4, 6],
    color : new util.Color([1, 1, 1]), opacity : 1,
    flipHoriz : false, flipVert : false,
    texRes : 512, interpolate : true, depth : -3.0 
  });
  train_JS2_text_arrow = new visual.TextStim({
    win: psychoJS.window,
    name: 'train_JS2_text_arrow',
    text: 'A seta estava apontando para cima?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -4.0 
  });
  
  resp_train_JS2_arrow = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Instructions_JS2_4"
  Instructions_JS2_4Clock = new util.Clock();
  intro_JS2_4 = new visual.TextStim({
    win: psychoJS.window,
    name: 'intro_JS2_4',
    text: 'Fim do treino!\n \nLembre-se: sua tarefa agora é a de julgar, o mais rápido possível, se os pares de verbos são SINÔNIMOS.\n \nQuando estiver pronto, APERTE ESPAÇO PARA DAR INÍCIO ao teste e sempre mantenha seus dedos indicadores posicionados nas teclas A e L.',
    font: 'Arial',
    units: undefined, 
    pos: [0, 2], height: 1,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  resp_next_JS2_4 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Test_JS2"
  Test_JS2Clock = new util.Clock();
  fixation_cross_test_JS2_1 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_test_JS2_1', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -1, interpolate: true,
  });
  
  text_test_JS2 = new visual.TextStim({
    win: psychoJS.window,
    name: 'text_test_JS2',
    text: 'default text',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -2.0 
  });
  
  resp_test_JS2 = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "Test_JS2_Arrow"
  Test_JS2_ArrowClock = new util.Clock();
  fixation_cross_test_JS2 = new visual.ShapeStim ({
    win: psychoJS.window, name: 'fixation_cross_test_JS2', 
    vertices: 'cross', size:[0.5, 0.5],
    ori: 0, pos: [0, 0],
    lineWidth: 1, lineColor: new util.Color([1, 1, 1]),
    fillColor: new util.Color([1, 1, 1]),
    opacity: 1, depth: -2, interpolate: true,
  });
  
  arrow_image_test_JS2 = new visual.ImageStim({
    win : psychoJS.window,
    name : 'arrow_image_test_JS2', units : undefined, 
    image : 'arrow.png', mask : undefined,
    ori : 1.0, pos : [0, 0], size : [4, 6],
    color : new util.Color([1, 1, 1]), opacity : 1,
    flipHoriz : false, flipVert : false,
    texRes : 512, interpolate : true, depth : -3.0 
  });
  test_JS2_text_arrow = new visual.TextStim({
    win: psychoJS.window,
    name: 'test_JS2_text_arrow',
    text: 'A seta estava apontando para cima?',
    font: 'Arial',
    units: undefined, 
    pos: [0, 0], height: 1,  wrapWidth: undefined, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -4.0 
  });
  
  resp_test_JS2_arrow = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Initialize components for Routine "End"
  EndClock = new util.Clock();
  end = new visual.TextStim({
    win: psychoJS.window,
    name: 'end',
    text: 'Bem, acabamos por aqui!\n\nMuito obrigada por participar. ',
    font: 'Arial',
    units: undefined, 
    pos: [0, 6], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: 0.0 
  });
  
  next_end = new visual.TextStim({
    win: psychoJS.window,
    name: 'next_end',
    text: 'Pressione ESPAÇO para finalizar.',
    font: 'Arial',
    units: undefined, 
    pos: [0, (- 8)], height: 1.31,  wrapWidth: 30, ori: 0,
    color: new util.Color('white'),  opacity: 1,
    depth: -1.0 
  });
  
  next_end_resp = new core.Keyboard({psychoJS: psychoJS, clock: new util.Clock(), waitForStart: true});
  
  // Create some handy timers
  globalClock = new util.Clock();  // to track the time since experiment started
  routineTimer = new util.CountdownTimer();  // to track time remaining of each (non-slip) routine
  
  return Scheduler.Event.NEXT;
}

function WelcomeRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Welcome'-------
    t = 0;
    WelcomeClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    next_welcome_resp.keys = undefined;
    next_welcome_resp.rt = undefined;
    _next_welcome_resp_allKeys = [];
    // keep track of which components have finished
    WelcomeComponents = [];
    WelcomeComponents.push(intro);
    WelcomeComponents.push(next_welcome);
    WelcomeComponents.push(next_welcome_resp);
    
    WelcomeComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function WelcomeRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Welcome'-------
    // get current time
    t = WelcomeClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro* updates
    if (t >= 0.0 && intro.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro.tStart = t;  // (not accounting for frame time here)
      intro.frameNStart = frameN;  // exact frame index
      
      intro.setAutoDraw(true);
    }

    
    // *next_welcome* updates
    if (t >= 0.5 && next_welcome.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_welcome.tStart = t;  // (not accounting for frame time here)
      next_welcome.frameNStart = frameN;  // exact frame index
      
      next_welcome.setAutoDraw(true);
    }

    
    // *next_welcome_resp* updates
    if (t >= 0.5 && next_welcome_resp.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_welcome_resp.tStart = t;  // (not accounting for frame time here)
      next_welcome_resp.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { next_welcome_resp.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { next_welcome_resp.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { next_welcome_resp.clearEvents(); });
    }

    if (next_welcome_resp.status === PsychoJS.Status.STARTED) {
      let theseKeys = next_welcome_resp.getKeys({keyList: ['space'], waitRelease: false});
      _next_welcome_resp_allKeys = _next_welcome_resp_allKeys.concat(theseKeys);
      if (_next_welcome_resp_allKeys.length > 0) {
        next_welcome_resp.keys = _next_welcome_resp_allKeys[_next_welcome_resp_allKeys.length - 1].name;  // just the last key pressed
        next_welcome_resp.rt = _next_welcome_resp_allKeys[_next_welcome_resp_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    WelcomeComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function WelcomeRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Welcome'-------
    WelcomeComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('next_welcome_resp.keys', next_welcome_resp.keys);
    if (typeof next_welcome_resp.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('next_welcome_resp.rt', next_welcome_resp.rt);
        routineTimer.reset();
        }
    
    next_welcome_resp.stop();
    // the Routine "Welcome" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL1'-------
    t = 0;
    Instructions_DL1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL1.keys = undefined;
    resp_next_DL1.rt = undefined;
    _resp_next_DL1_allKeys = [];
    // keep track of which components have finished
    Instructions_DL1Components = [];
    Instructions_DL1Components.push(DL);
    Instructions_DL1Components.push(intro_DL1);
    Instructions_DL1Components.push(next_DL1);
    Instructions_DL1Components.push(resp_next_DL1);
    
    Instructions_DL1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL1'-------
    // get current time
    t = Instructions_DL1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *DL* updates
    if (t >= 0.0 && DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      DL.tStart = t;  // (not accounting for frame time here)
      DL.frameNStart = frameN;  // exact frame index
      
      DL.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((DL.status === PsychoJS.Status.STARTED || DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      DL.setAutoDraw(false);
    }
    
    // *intro_DL1* updates
    if (t >= 1 && intro_DL1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL1.tStart = t;  // (not accounting for frame time here)
      intro_DL1.frameNStart = frameN;  // exact frame index
      
      intro_DL1.setAutoDraw(true);
    }

    
    // *next_DL1* updates
    if (t >= 2 && next_DL1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_DL1.tStart = t;  // (not accounting for frame time here)
      next_DL1.frameNStart = frameN;  // exact frame index
      
      next_DL1.setAutoDraw(true);
    }

    
    // *resp_next_DL1* updates
    if (t >= 2 && resp_next_DL1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL1.tStart = t;  // (not accounting for frame time here)
      resp_next_DL1.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL1.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL1.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL1.clearEvents(); });
    }

    if (resp_next_DL1.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL1.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL1_allKeys = _resp_next_DL1_allKeys.concat(theseKeys);
      if (_resp_next_DL1_allKeys.length > 0) {
        resp_next_DL1.keys = _resp_next_DL1_allKeys[_resp_next_DL1_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL1.rt = _resp_next_DL1_allKeys[_resp_next_DL1_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL1'-------
    Instructions_DL1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL1.keys', resp_next_DL1.keys);
    if (typeof resp_next_DL1.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL1.rt', resp_next_DL1.rt);
        routineTimer.reset();
        }
    
    resp_next_DL1.stop();
    // the Routine "Instructions_DL1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL2'-------
    t = 0;
    Instructions_DL2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL2.keys = undefined;
    resp_next_DL2.rt = undefined;
    _resp_next_DL2_allKeys = [];
    // keep track of which components have finished
    Instructions_DL2Components = [];
    Instructions_DL2Components.push(intro_DL2);
    Instructions_DL2Components.push(next_DL2);
    Instructions_DL2Components.push(resp_next_DL2);
    
    Instructions_DL2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL2'-------
    // get current time
    t = Instructions_DL2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL2* updates
    if (t >= 0.0 && intro_DL2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL2.tStart = t;  // (not accounting for frame time here)
      intro_DL2.frameNStart = frameN;  // exact frame index
      
      intro_DL2.setAutoDraw(true);
    }

    
    // *next_DL2* updates
    if (t >= 1 && next_DL2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_DL2.tStart = t;  // (not accounting for frame time here)
      next_DL2.frameNStart = frameN;  // exact frame index
      
      next_DL2.setAutoDraw(true);
    }

    
    // *resp_next_DL2* updates
    if (t >= 1 && resp_next_DL2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL2.tStart = t;  // (not accounting for frame time here)
      resp_next_DL2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL2.clearEvents(); });
    }

    if (resp_next_DL2.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL2.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL2_allKeys = _resp_next_DL2_allKeys.concat(theseKeys);
      if (_resp_next_DL2_allKeys.length > 0) {
        resp_next_DL2.keys = _resp_next_DL2_allKeys[_resp_next_DL2_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL2.rt = _resp_next_DL2_allKeys[_resp_next_DL2_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL2'-------
    Instructions_DL2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL2.keys', resp_next_DL2.keys);
    if (typeof resp_next_DL2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL2.rt', resp_next_DL2.rt);
        routineTimer.reset();
        }
    
    resp_next_DL2.stop();
    // the Routine "Instructions_DL2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL3RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL3'-------
    t = 0;
    Instructions_DL3Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL3.keys = undefined;
    resp_next_DL3.rt = undefined;
    _resp_next_DL3_allKeys = [];
    // keep track of which components have finished
    Instructions_DL3Components = [];
    Instructions_DL3Components.push(intro_DL3);
    Instructions_DL3Components.push(next_DL3);
    Instructions_DL3Components.push(resp_next_DL3);
    
    Instructions_DL3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL3RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL3'-------
    // get current time
    t = Instructions_DL3Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL3* updates
    if (t >= 0.0 && intro_DL3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL3.tStart = t;  // (not accounting for frame time here)
      intro_DL3.frameNStart = frameN;  // exact frame index
      
      intro_DL3.setAutoDraw(true);
    }

    
    // *next_DL3* updates
    if (t >= 1 && next_DL3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_DL3.tStart = t;  // (not accounting for frame time here)
      next_DL3.frameNStart = frameN;  // exact frame index
      
      next_DL3.setAutoDraw(true);
    }

    
    // *resp_next_DL3* updates
    if (t >= 1 && resp_next_DL3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL3.tStart = t;  // (not accounting for frame time here)
      resp_next_DL3.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL3.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL3.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL3.clearEvents(); });
    }

    if (resp_next_DL3.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL3.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL3_allKeys = _resp_next_DL3_allKeys.concat(theseKeys);
      if (_resp_next_DL3_allKeys.length > 0) {
        resp_next_DL3.keys = _resp_next_DL3_allKeys[_resp_next_DL3_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL3.rt = _resp_next_DL3_allKeys[_resp_next_DL3_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL3RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL3'-------
    Instructions_DL3Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL3.keys', resp_next_DL3.keys);
    if (typeof resp_next_DL3.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL3.rt', resp_next_DL3.rt);
        routineTimer.reset();
        }
    
    resp_next_DL3.stop();
    // the Routine "Instructions_DL3" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL4RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL4'-------
    t = 0;
    Instructions_DL4Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL4.keys = undefined;
    resp_next_DL4.rt = undefined;
    _resp_next_DL4_allKeys = [];
    // keep track of which components have finished
    Instructions_DL4Components = [];
    Instructions_DL4Components.push(intro_DL4);
    Instructions_DL4Components.push(next_DL4);
    Instructions_DL4Components.push(resp_next_DL4);
    
    Instructions_DL4Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL4RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL4'-------
    // get current time
    t = Instructions_DL4Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL4* updates
    if (t >= 0.0 && intro_DL4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL4.tStart = t;  // (not accounting for frame time here)
      intro_DL4.frameNStart = frameN;  // exact frame index
      
      intro_DL4.setAutoDraw(true);
    }

    
    // *next_DL4* updates
    if (t >= 1 && next_DL4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_DL4.tStart = t;  // (not accounting for frame time here)
      next_DL4.frameNStart = frameN;  // exact frame index
      
      next_DL4.setAutoDraw(true);
    }

    
    // *resp_next_DL4* updates
    if (t >= 1 && resp_next_DL4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL4.tStart = t;  // (not accounting for frame time here)
      resp_next_DL4.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL4.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL4.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL4.clearEvents(); });
    }

    if (resp_next_DL4.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL4.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL4_allKeys = _resp_next_DL4_allKeys.concat(theseKeys);
      if (_resp_next_DL4_allKeys.length > 0) {
        resp_next_DL4.keys = _resp_next_DL4_allKeys[_resp_next_DL4_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL4.rt = _resp_next_DL4_allKeys[_resp_next_DL4_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL4Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL4RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL4'-------
    Instructions_DL4Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL4.keys', resp_next_DL4.keys);
    if (typeof resp_next_DL4.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL4.rt', resp_next_DL4.rt);
        routineTimer.reset();
        }
    
    resp_next_DL4.stop();
    // the Routine "Instructions_DL4" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL5RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL5'-------
    t = 0;
    Instructions_DL5Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL5.keys = undefined;
    resp_next_DL5.rt = undefined;
    _resp_next_DL5_allKeys = [];
    // keep track of which components have finished
    Instructions_DL5Components = [];
    Instructions_DL5Components.push(intro_DL5);
    Instructions_DL5Components.push(next_DL5);
    Instructions_DL5Components.push(resp_next_DL5);
    
    Instructions_DL5Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL5RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL5'-------
    // get current time
    t = Instructions_DL5Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL5* updates
    if (t >= 0.0 && intro_DL5.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL5.tStart = t;  // (not accounting for frame time here)
      intro_DL5.frameNStart = frameN;  // exact frame index
      
      intro_DL5.setAutoDraw(true);
    }

    
    // *next_DL5* updates
    if (t >= 1 && next_DL5.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_DL5.tStart = t;  // (not accounting for frame time here)
      next_DL5.frameNStart = frameN;  // exact frame index
      
      next_DL5.setAutoDraw(true);
    }

    
    // *resp_next_DL5* updates
    if (t >= 1 && resp_next_DL5.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL5.tStart = t;  // (not accounting for frame time here)
      resp_next_DL5.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL5.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL5.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL5.clearEvents(); });
    }

    if (resp_next_DL5.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL5.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL5_allKeys = _resp_next_DL5_allKeys.concat(theseKeys);
      if (_resp_next_DL5_allKeys.length > 0) {
        resp_next_DL5.keys = _resp_next_DL5_allKeys[_resp_next_DL5_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL5.rt = _resp_next_DL5_allKeys[_resp_next_DL5_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL5Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL5RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL5'-------
    Instructions_DL5Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL5.keys', resp_next_DL5.keys);
    if (typeof resp_next_DL5.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL5.rt', resp_next_DL5.rt);
        routineTimer.reset();
        }
    
    resp_next_DL5.stop();
    // the Routine "Instructions_DL5" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL6RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL6'-------
    t = 0;
    Instructions_DL6Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL6.keys = undefined;
    resp_next_DL6.rt = undefined;
    _resp_next_DL6_allKeys = [];
    // keep track of which components have finished
    Instructions_DL6Components = [];
    Instructions_DL6Components.push(intro_DL6);
    Instructions_DL6Components.push(next_DL6);
    Instructions_DL6Components.push(resp_next_DL6);
    
    Instructions_DL6Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL6RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL6'-------
    // get current time
    t = Instructions_DL6Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL6* updates
    if (t >= 0.0 && intro_DL6.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL6.tStart = t;  // (not accounting for frame time here)
      intro_DL6.frameNStart = frameN;  // exact frame index
      
      intro_DL6.setAutoDraw(true);
    }

    
    // *next_DL6* updates
    if (t >= 1 && next_DL6.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_DL6.tStart = t;  // (not accounting for frame time here)
      next_DL6.frameNStart = frameN;  // exact frame index
      
      next_DL6.setAutoDraw(true);
    }

    
    // *resp_next_DL6* updates
    if (t >= 1 && resp_next_DL6.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL6.tStart = t;  // (not accounting for frame time here)
      resp_next_DL6.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL6.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL6.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL6.clearEvents(); });
    }

    if (resp_next_DL6.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL6.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL6_allKeys = _resp_next_DL6_allKeys.concat(theseKeys);
      if (_resp_next_DL6_allKeys.length > 0) {
        resp_next_DL6.keys = _resp_next_DL6_allKeys[_resp_next_DL6_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL6.rt = _resp_next_DL6_allKeys[_resp_next_DL6_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL6Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL6RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL6'-------
    Instructions_DL6Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL6.keys', resp_next_DL6.keys);
    if (typeof resp_next_DL6.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL6.rt', resp_next_DL6.rt);
        routineTimer.reset();
        }
    
    resp_next_DL6.stop();
    // the Routine "Instructions_DL6" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL7RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL7'-------
    t = 0;
    Instructions_DL7Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL7.keys = undefined;
    resp_next_DL7.rt = undefined;
    _resp_next_DL7_allKeys = [];
    // keep track of which components have finished
    Instructions_DL7Components = [];
    Instructions_DL7Components.push(intro_DL7);
    Instructions_DL7Components.push(resp_next_DL7);
    
    Instructions_DL7Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL7RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL7'-------
    // get current time
    t = Instructions_DL7Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL7* updates
    if (t >= 0.0 && intro_DL7.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL7.tStart = t;  // (not accounting for frame time here)
      intro_DL7.frameNStart = frameN;  // exact frame index
      
      intro_DL7.setAutoDraw(true);
    }

    
    // *resp_next_DL7* updates
    if (t >= 1 && resp_next_DL7.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL7.tStart = t;  // (not accounting for frame time here)
      resp_next_DL7.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL7.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL7.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL7.clearEvents(); });
    }

    if (resp_next_DL7.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL7.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL7_allKeys = _resp_next_DL7_allKeys.concat(theseKeys);
      if (_resp_next_DL7_allKeys.length > 0) {
        resp_next_DL7.keys = _resp_next_DL7_allKeys[_resp_next_DL7_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL7.rt = _resp_next_DL7_allKeys[_resp_next_DL7_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL7Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL7RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL7'-------
    Instructions_DL7Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL7.keys', resp_next_DL7.keys);
    if (typeof resp_next_DL7.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL7.rt', resp_next_DL7.rt);
        routineTimer.reset();
        }
    
    resp_next_DL7.stop();
    // the Routine "Instructions_DL7" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Seq_Train_DLLoopBegin(Seq_Train_DLLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Seq_Train_DL = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 100, method: TrialHandler.Method.SEQUENTIAL,
    extraInfo: expInfo, originPath: undefined,
    trialList: TrialHandler.importConditions(psychoJS.serverManager, 'Estimulos.xlsx', ':20'),
    seed: undefined, name: 'Seq_Train_DL'
  });
  psychoJS.experiment.addLoop(Seq_Train_DL); // add the loop to the experiment
  currentLoop = Seq_Train_DL;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Seq_Train_DL.forEach(function() {
    const snapshot = Seq_Train_DL.getSnapshot();

    Seq_Train_DLLoopScheduler.add(importConditions(snapshot));
    Seq_Train_DLLoopScheduler.add(Train_DLRoutineBegin(snapshot));
    Seq_Train_DLLoopScheduler.add(Train_DLRoutineEachFrame(snapshot));
    Seq_Train_DLLoopScheduler.add(Train_DLRoutineEnd(snapshot));
    Seq_Train_DLLoopScheduler.add(Feedback_Train_DLRoutineBegin(snapshot));
    Seq_Train_DLLoopScheduler.add(Feedback_Train_DLRoutineEachFrame(snapshot));
    Seq_Train_DLLoopScheduler.add(Feedback_Train_DLRoutineEnd(snapshot));
    const Train_DL_Arrow_RepLoopScheduler = new Scheduler(psychoJS);
    Seq_Train_DLLoopScheduler.add(Train_DL_Arrow_RepLoopBegin, Train_DL_Arrow_RepLoopScheduler);
    Seq_Train_DLLoopScheduler.add(Train_DL_Arrow_RepLoopScheduler);
    Seq_Train_DLLoopScheduler.add(Train_DL_Arrow_RepLoopEnd);
    Seq_Train_DLLoopScheduler.add(endLoopIteration(Seq_Train_DLLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Train_DL_Arrow_RepLoopBegin(Train_DL_Arrow_RepLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Train_DL_Arrow_Rep = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 2, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: undefined,
    seed: undefined, name: 'Train_DL_Arrow_Rep'
  });
  psychoJS.experiment.addLoop(Train_DL_Arrow_Rep); // add the loop to the experiment
  currentLoop = Train_DL_Arrow_Rep;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Train_DL_Arrow_Rep.forEach(function() {
    const snapshot = Train_DL_Arrow_Rep.getSnapshot();

    Train_DL_Arrow_RepLoopScheduler.add(importConditions(snapshot));
    Train_DL_Arrow_RepLoopScheduler.add(Train_DL_ArrowRoutineBegin(snapshot));
    Train_DL_Arrow_RepLoopScheduler.add(Train_DL_ArrowRoutineEachFrame(snapshot));
    Train_DL_Arrow_RepLoopScheduler.add(Train_DL_ArrowRoutineEnd(snapshot));
    Train_DL_Arrow_RepLoopScheduler.add(endLoopIteration(Train_DL_Arrow_RepLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Train_DL_Arrow_RepLoopEnd() {
  psychoJS.experiment.removeLoop(Train_DL_Arrow_Rep);

  return Scheduler.Event.NEXT;
}

function Seq_Train_DLLoopEnd() {
  psychoJS.experiment.removeLoop(Seq_Train_DL);

  return Scheduler.Event.NEXT;
}

function Seq_Test_DLLoopBegin(Seq_Test_DLLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Seq_Test_DL = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 1, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: TrialHandler.importConditions(psychoJS.serverManager, 'Estimulos.xlsx', ':180'),
    seed: undefined, name: 'Seq_Test_DL'
  });
  psychoJS.experiment.addLoop(Seq_Test_DL); // add the loop to the experiment
  currentLoop = Seq_Test_DL;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Seq_Test_DL.forEach(function() {
    const snapshot = Seq_Test_DL.getSnapshot();

    Seq_Test_DLLoopScheduler.add(importConditions(snapshot));
    Seq_Test_DLLoopScheduler.add(Test_DLRoutineBegin(snapshot));
    Seq_Test_DLLoopScheduler.add(Test_DLRoutineEachFrame(snapshot));
    Seq_Test_DLLoopScheduler.add(Test_DLRoutineEnd(snapshot));
    const Test_DL_Arrow_RepLoopScheduler = new Scheduler(psychoJS);
    Seq_Test_DLLoopScheduler.add(Test_DL_Arrow_RepLoopBegin, Test_DL_Arrow_RepLoopScheduler);
    Seq_Test_DLLoopScheduler.add(Test_DL_Arrow_RepLoopScheduler);
    Seq_Test_DLLoopScheduler.add(Test_DL_Arrow_RepLoopEnd);
    Seq_Test_DLLoopScheduler.add(endLoopIteration(Seq_Test_DLLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Test_DL_Arrow_RepLoopBegin(Test_DL_Arrow_RepLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Test_DL_Arrow_Rep = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 2, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: undefined,
    seed: undefined, name: 'Test_DL_Arrow_Rep'
  });
  psychoJS.experiment.addLoop(Test_DL_Arrow_Rep); // add the loop to the experiment
  currentLoop = Test_DL_Arrow_Rep;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Test_DL_Arrow_Rep.forEach(function() {
    const snapshot = Test_DL_Arrow_Rep.getSnapshot();

    Test_DL_Arrow_RepLoopScheduler.add(importConditions(snapshot));
    Test_DL_Arrow_RepLoopScheduler.add(Test_DL_ArrowRoutineBegin(snapshot));
    Test_DL_Arrow_RepLoopScheduler.add(Test_DL_ArrowRoutineEachFrame(snapshot));
    Test_DL_Arrow_RepLoopScheduler.add(Test_DL_ArrowRoutineEnd(snapshot));
    Test_DL_Arrow_RepLoopScheduler.add(endLoopIteration(Test_DL_Arrow_RepLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Test_DL_Arrow_RepLoopEnd() {
  psychoJS.experiment.removeLoop(Test_DL_Arrow_Rep);

  return Scheduler.Event.NEXT;
}

function Seq_Test_DLLoopEnd() {
  psychoJS.experiment.removeLoop(Seq_Test_DL);

  return Scheduler.Event.NEXT;
}

function Seq_Train_JS1LoopBegin(Seq_Train_JS1LoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Seq_Train_JS1 = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 100, method: TrialHandler.Method.SEQUENTIAL,
    extraInfo: expInfo, originPath: undefined,
    trialList: TrialHandler.importConditions(psychoJS.serverManager, 'Estimulos.xlsx', ':20'),
    seed: undefined, name: 'Seq_Train_JS1'
  });
  psychoJS.experiment.addLoop(Seq_Train_JS1); // add the loop to the experiment
  currentLoop = Seq_Train_JS1;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Seq_Train_JS1.forEach(function() {
    const snapshot = Seq_Train_JS1.getSnapshot();

    Seq_Train_JS1LoopScheduler.add(importConditions(snapshot));
    Seq_Train_JS1LoopScheduler.add(Train_JS1RoutineBegin(snapshot));
    Seq_Train_JS1LoopScheduler.add(Train_JS1RoutineEachFrame(snapshot));
    Seq_Train_JS1LoopScheduler.add(Train_JS1RoutineEnd(snapshot));
    Seq_Train_JS1LoopScheduler.add(Feedback_Train_JS1RoutineBegin(snapshot));
    Seq_Train_JS1LoopScheduler.add(Feedback_Train_JS1RoutineEachFrame(snapshot));
    Seq_Train_JS1LoopScheduler.add(Feedback_Train_JS1RoutineEnd(snapshot));
    const Train_JS1_Arrow_RepLoopScheduler = new Scheduler(psychoJS);
    Seq_Train_JS1LoopScheduler.add(Train_JS1_Arrow_RepLoopBegin, Train_JS1_Arrow_RepLoopScheduler);
    Seq_Train_JS1LoopScheduler.add(Train_JS1_Arrow_RepLoopScheduler);
    Seq_Train_JS1LoopScheduler.add(Train_JS1_Arrow_RepLoopEnd);
    Seq_Train_JS1LoopScheduler.add(endLoopIteration(Seq_Train_JS1LoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Train_JS1_Arrow_RepLoopBegin(Train_JS1_Arrow_RepLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Train_JS1_Arrow_Rep = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 2, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: undefined,
    seed: undefined, name: 'Train_JS1_Arrow_Rep'
  });
  psychoJS.experiment.addLoop(Train_JS1_Arrow_Rep); // add the loop to the experiment
  currentLoop = Train_JS1_Arrow_Rep;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Train_JS1_Arrow_Rep.forEach(function() {
    const snapshot = Train_JS1_Arrow_Rep.getSnapshot();

    Train_JS1_Arrow_RepLoopScheduler.add(importConditions(snapshot));
    Train_JS1_Arrow_RepLoopScheduler.add(Train_JS1_ArrowRoutineBegin(snapshot));
    Train_JS1_Arrow_RepLoopScheduler.add(Train_JS1_ArrowRoutineEachFrame(snapshot));
    Train_JS1_Arrow_RepLoopScheduler.add(Train_JS1_ArrowRoutineEnd(snapshot));
    Train_JS1_Arrow_RepLoopScheduler.add(endLoopIteration(Train_JS1_Arrow_RepLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Train_JS1_Arrow_RepLoopEnd() {
  psychoJS.experiment.removeLoop(Train_JS1_Arrow_Rep);

  return Scheduler.Event.NEXT;
}

function Seq_Train_JS1LoopEnd() {
  psychoJS.experiment.removeLoop(Seq_Train_JS1);

  return Scheduler.Event.NEXT;
}

function Seq_Test_JS1LoopBegin(Seq_Test_JS1LoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Seq_Test_JS1 = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 1, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: TrialHandler.importConditions(psychoJS.serverManager, 'Estimulos.xlsx', ':90'),
    seed: undefined, name: 'Seq_Test_JS1'
  });
  psychoJS.experiment.addLoop(Seq_Test_JS1); // add the loop to the experiment
  currentLoop = Seq_Test_JS1;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Seq_Test_JS1.forEach(function() {
    const snapshot = Seq_Test_JS1.getSnapshot();

    Seq_Test_JS1LoopScheduler.add(importConditions(snapshot));
    Seq_Test_JS1LoopScheduler.add(Test_JS1RoutineBegin(snapshot));
    Seq_Test_JS1LoopScheduler.add(Test_JS1RoutineEachFrame(snapshot));
    Seq_Test_JS1LoopScheduler.add(Test_JS1RoutineEnd(snapshot));
    const Test_JS1_Arrow_RepLoopScheduler = new Scheduler(psychoJS);
    Seq_Test_JS1LoopScheduler.add(Test_JS1_Arrow_RepLoopBegin, Test_JS1_Arrow_RepLoopScheduler);
    Seq_Test_JS1LoopScheduler.add(Test_JS1_Arrow_RepLoopScheduler);
    Seq_Test_JS1LoopScheduler.add(Test_JS1_Arrow_RepLoopEnd);
    Seq_Test_JS1LoopScheduler.add(endLoopIteration(Seq_Test_JS1LoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Test_JS1_Arrow_RepLoopBegin(Test_JS1_Arrow_RepLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Test_JS1_Arrow_Rep = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 2, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: undefined,
    seed: undefined, name: 'Test_JS1_Arrow_Rep'
  });
  psychoJS.experiment.addLoop(Test_JS1_Arrow_Rep); // add the loop to the experiment
  currentLoop = Test_JS1_Arrow_Rep;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Test_JS1_Arrow_Rep.forEach(function() {
    const snapshot = Test_JS1_Arrow_Rep.getSnapshot();

    Test_JS1_Arrow_RepLoopScheduler.add(importConditions(snapshot));
    Test_JS1_Arrow_RepLoopScheduler.add(Test_JS1_ArrowRoutineBegin(snapshot));
    Test_JS1_Arrow_RepLoopScheduler.add(Test_JS1_ArrowRoutineEachFrame(snapshot));
    Test_JS1_Arrow_RepLoopScheduler.add(Test_JS1_ArrowRoutineEnd(snapshot));
    Test_JS1_Arrow_RepLoopScheduler.add(endLoopIteration(Test_JS1_Arrow_RepLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Test_JS1_Arrow_RepLoopEnd() {
  psychoJS.experiment.removeLoop(Test_JS1_Arrow_Rep);

  return Scheduler.Event.NEXT;
}

function Seq_Test_JS1LoopEnd() {
  psychoJS.experiment.removeLoop(Seq_Test_JS1);

  return Scheduler.Event.NEXT;
}

function Seq_Train_JS2LoopBegin(Seq_Train_JS2LoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Seq_Train_JS2 = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 100, method: TrialHandler.Method.SEQUENTIAL,
    extraInfo: expInfo, originPath: undefined,
    trialList: TrialHandler.importConditions(psychoJS.serverManager, 'Estimulos.xlsx', ':20'),
    seed: undefined, name: 'Seq_Train_JS2'
  });
  psychoJS.experiment.addLoop(Seq_Train_JS2); // add the loop to the experiment
  currentLoop = Seq_Train_JS2;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Seq_Train_JS2.forEach(function() {
    const snapshot = Seq_Train_JS2.getSnapshot();

    Seq_Train_JS2LoopScheduler.add(importConditions(snapshot));
    Seq_Train_JS2LoopScheduler.add(Train_JS2RoutineBegin(snapshot));
    Seq_Train_JS2LoopScheduler.add(Train_JS2RoutineEachFrame(snapshot));
    Seq_Train_JS2LoopScheduler.add(Train_JS2RoutineEnd(snapshot));
    Seq_Train_JS2LoopScheduler.add(Feedback_Train_JS2RoutineBegin(snapshot));
    Seq_Train_JS2LoopScheduler.add(Feedback_Train_JS2RoutineEachFrame(snapshot));
    Seq_Train_JS2LoopScheduler.add(Feedback_Train_JS2RoutineEnd(snapshot));
    const Train_JS2_Arrow_RepLoopScheduler = new Scheduler(psychoJS);
    Seq_Train_JS2LoopScheduler.add(Train_JS2_Arrow_RepLoopBegin, Train_JS2_Arrow_RepLoopScheduler);
    Seq_Train_JS2LoopScheduler.add(Train_JS2_Arrow_RepLoopScheduler);
    Seq_Train_JS2LoopScheduler.add(Train_JS2_Arrow_RepLoopEnd);
    Seq_Train_JS2LoopScheduler.add(endLoopIteration(Seq_Train_JS2LoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Train_JS2_Arrow_RepLoopBegin(Train_JS2_Arrow_RepLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Train_JS2_Arrow_Rep = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 2, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: undefined,
    seed: undefined, name: 'Train_JS2_Arrow_Rep'
  });
  psychoJS.experiment.addLoop(Train_JS2_Arrow_Rep); // add the loop to the experiment
  currentLoop = Train_JS2_Arrow_Rep;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Train_JS2_Arrow_Rep.forEach(function() {
    const snapshot = Train_JS2_Arrow_Rep.getSnapshot();

    Train_JS2_Arrow_RepLoopScheduler.add(importConditions(snapshot));
    Train_JS2_Arrow_RepLoopScheduler.add(Train_JS2_ArrowRoutineBegin(snapshot));
    Train_JS2_Arrow_RepLoopScheduler.add(Train_JS2_ArrowRoutineEachFrame(snapshot));
    Train_JS2_Arrow_RepLoopScheduler.add(Train_JS2_ArrowRoutineEnd(snapshot));
    Train_JS2_Arrow_RepLoopScheduler.add(endLoopIteration(Train_JS2_Arrow_RepLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Train_JS2_Arrow_RepLoopEnd() {
  psychoJS.experiment.removeLoop(Train_JS2_Arrow_Rep);

  return Scheduler.Event.NEXT;
}

function Seq_Train_JS2LoopEnd() {
  psychoJS.experiment.removeLoop(Seq_Train_JS2);

  return Scheduler.Event.NEXT;
}

function Seq_Test_JS2LoopBegin(Seq_Test_JS2LoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Seq_Test_JS2 = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 1, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: TrialHandler.importConditions(psychoJS.serverManager, 'Estimulos.xlsx', ':90'),
    seed: undefined, name: 'Seq_Test_JS2'
  });
  psychoJS.experiment.addLoop(Seq_Test_JS2); // add the loop to the experiment
  currentLoop = Seq_Test_JS2;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Seq_Test_JS2.forEach(function() {
    const snapshot = Seq_Test_JS2.getSnapshot();

    Seq_Test_JS2LoopScheduler.add(importConditions(snapshot));
    Seq_Test_JS2LoopScheduler.add(Test_JS2RoutineBegin(snapshot));
    Seq_Test_JS2LoopScheduler.add(Test_JS2RoutineEachFrame(snapshot));
    Seq_Test_JS2LoopScheduler.add(Test_JS2RoutineEnd(snapshot));
    const Test_JS2_Arrow_RepLoopScheduler = new Scheduler(psychoJS);
    Seq_Test_JS2LoopScheduler.add(Test_JS2_Arrow_RepLoopBegin, Test_JS2_Arrow_RepLoopScheduler);
    Seq_Test_JS2LoopScheduler.add(Test_JS2_Arrow_RepLoopScheduler);
    Seq_Test_JS2LoopScheduler.add(Test_JS2_Arrow_RepLoopEnd);
    Seq_Test_JS2LoopScheduler.add(endLoopIteration(Seq_Test_JS2LoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Test_JS2_Arrow_RepLoopBegin(Test_JS2_Arrow_RepLoopScheduler) {
  // set up handler to look after randomisation of conditions etc
  Test_JS2_Arrow_Rep = new TrialHandler({
    psychoJS: psychoJS,
    nReps: 2, method: TrialHandler.Method.RANDOM,
    extraInfo: expInfo, originPath: undefined,
    trialList: undefined,
    seed: undefined, name: 'Test_JS2_Arrow_Rep'
  });
  psychoJS.experiment.addLoop(Test_JS2_Arrow_Rep); // add the loop to the experiment
  currentLoop = Test_JS2_Arrow_Rep;  // we're now the current loop

  // Schedule all the trials in the trialList:
  Test_JS2_Arrow_Rep.forEach(function() {
    const snapshot = Test_JS2_Arrow_Rep.getSnapshot();

    Test_JS2_Arrow_RepLoopScheduler.add(importConditions(snapshot));
    Test_JS2_Arrow_RepLoopScheduler.add(Test_JS2_ArrowRoutineBegin(snapshot));
    Test_JS2_Arrow_RepLoopScheduler.add(Test_JS2_ArrowRoutineEachFrame(snapshot));
    Test_JS2_Arrow_RepLoopScheduler.add(Test_JS2_ArrowRoutineEnd(snapshot));
    Test_JS2_Arrow_RepLoopScheduler.add(endLoopIteration(Test_JS2_Arrow_RepLoopScheduler, snapshot));
  });

  return Scheduler.Event.NEXT;
}

function Test_JS2_Arrow_RepLoopEnd() {
  psychoJS.experiment.removeLoop(Test_JS2_Arrow_Rep);

  return Scheduler.Event.NEXT;
}

function Seq_Test_JS2LoopEnd() {
  psychoJS.experiment.removeLoop(Seq_Test_JS2);

  return Scheduler.Event.NEXT;
}

function Train_DLRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Train_DL'-------
    t = 0;
    Train_DLClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.000000);
    // update component parameters for each repeat
    if (((keys_resp.index("a") === 0) && (RESPOSTA_DL_TREINO === 1))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (RESPOSTA_DL_TREINO === 0))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (RESPOSTA_DL_TREINO === 1))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (RESPOSTA_DL_TREINO === 0))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    text_train_DL.setText(VERBO_DL_TREINO);
    resp_train_DL.keys = undefined;
    resp_train_DL.rt = undefined;
    _resp_train_DL_allKeys = [];
    // keep track of which components have finished
    Train_DLComponents = [];
    Train_DLComponents.push(fixation_cross_train_DL);
    Train_DLComponents.push(text_train_DL);
    Train_DLComponents.push(resp_train_DL);
    
    Train_DLComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Train_DLRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Train_DL'-------
    // get current time
    t = Train_DLClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_train_DL* updates
    if (t >= 0.0 && fixation_cross_train_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_train_DL.tStart = t;  // (not accounting for frame time here)
      fixation_cross_train_DL.frameNStart = frameN;  // exact frame index
      
      fixation_cross_train_DL.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_train_DL.status === PsychoJS.Status.STARTED || fixation_cross_train_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_train_DL.setAutoDraw(false);
    }
    
    // *text_train_DL* updates
    if (t >= 1 && text_train_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_train_DL.tStart = t;  // (not accounting for frame time here)
      text_train_DL.frameNStart = frameN;  // exact frame index
      
      text_train_DL.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((text_train_DL.status === PsychoJS.Status.STARTED || text_train_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      text_train_DL.setAutoDraw(false);
    }
    
    // *resp_train_DL* updates
    if (t >= 1 && resp_train_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_train_DL.tStart = t;  // (not accounting for frame time here)
      resp_train_DL.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_train_DL.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_train_DL.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_train_DL.clearEvents(); });
    }

    frameRemains = 1 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_train_DL.status === PsychoJS.Status.STARTED || resp_train_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_train_DL.status = PsychoJS.Status.FINISHED;
  }

    if (resp_train_DL.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_train_DL.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_train_DL_allKeys = _resp_train_DL_allKeys.concat(theseKeys);
      if (_resp_train_DL_allKeys.length > 0) {
        resp_train_DL.keys = _resp_train_DL_allKeys[_resp_train_DL_allKeys.length - 1].name;  // just the last key pressed
        resp_train_DL.rt = _resp_train_DL_allKeys[_resp_train_DL_allKeys.length - 1].rt;
        // was this correct?
        if (resp_train_DL.keys == key_corr) {
            resp_train_DL.corr = 1;
        } else {
            resp_train_DL.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Train_DLComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Train_DLRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Train_DL'-------
    Train_DLComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_train_DL.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_train_DL.corr = 1;  // correct non-response
      } else {
         resp_train_DL.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_train_DL.keys', resp_train_DL.keys);
    psychoJS.experiment.addData('resp_train_DL.corr', resp_train_DL.corr);
    if (typeof resp_train_DL.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_train_DL.rt', resp_train_DL.rt);
        routineTimer.reset();
        }
    
    resp_train_DL.stop();
    return Scheduler.Event.NEXT;
  };
}

function Feedback_Train_DLRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Feedback_Train_DL'-------
    t = 0;
    Feedback_Train_DLClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(1.000000);
    // update component parameters for each repeat
    if ((! resp_train_DL.keys)) {
        msg = "Demorou muito!";
    } else {
        if ((resp_train_DL.corr === 1)) {
            msg = "Acertou!";
        } else {
            msg = "Errou";
        }
    }
    
    feedback_text_train_DL.setText(msg);
    // keep track of which components have finished
    Feedback_Train_DLComponents = [];
    Feedback_Train_DLComponents.push(feedback_text_train_DL);
    
    Feedback_Train_DLComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Feedback_Train_DLRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Feedback_Train_DL'-------
    // get current time
    t = Feedback_Train_DLClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *feedback_text_train_DL* updates
    if (t >= 0.0 && feedback_text_train_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      feedback_text_train_DL.tStart = t;  // (not accounting for frame time here)
      feedback_text_train_DL.frameNStart = frameN;  // exact frame index
      
      feedback_text_train_DL.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((feedback_text_train_DL.status === PsychoJS.Status.STARTED || feedback_text_train_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      feedback_text_train_DL.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Feedback_Train_DLComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Feedback_Train_DLRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Feedback_Train_DL'-------
    Feedback_Train_DLComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    respcorrs.append(resp_train_DL.corr);
    if (((respcorrs.length % 20) === 0)) {
        seq = respcorrs.slice((20 * (Number.parseInt((respcorrs.length / 20)) - 1)));
        if ((list(filter((x) => {
        return (x === 1);
    }, seq)).length > 12)) {
            Seq_Train_DL.finished = true;
        }
    }
    
    return Scheduler.Event.NEXT;
  };
}

function Train_DL_ArrowRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Train_DL_Arrow'-------
    t = 0;
    Train_DL_ArrowClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.350000);
    // update component parameters for each repeat
    if (((Seq_Train_DL.thisTrialN % 15) !== 0)) {
        continueRoutine = false;
    }
    
    arrow_angle = (90 * ((round(np.random.rand()) * 2) - 1));
    thisExp.addData("train_DL_arrow_angle", arrow_angle);
    if (((keys_resp.index("a") === 0) && (arrow_angle === 90))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (arrow_angle === (- 90)))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (arrow_angle === 90))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (arrow_angle === (- 90)))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    arrow_image_train_DL.setOri(arrow_angle);
    resp_train_DL_arrow.keys = undefined;
    resp_train_DL_arrow.rt = undefined;
    _resp_train_DL_arrow_allKeys = [];
    // keep track of which components have finished
    Train_DL_ArrowComponents = [];
    Train_DL_ArrowComponents.push(fixation_cross_train_DL_2);
    Train_DL_ArrowComponents.push(arrow_image_train_DL);
    Train_DL_ArrowComponents.push(train_DL_text_arrow);
    Train_DL_ArrowComponents.push(resp_train_DL_arrow);
    
    Train_DL_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Train_DL_ArrowRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Train_DL_Arrow'-------
    // get current time
    t = Train_DL_ArrowClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_train_DL_2* updates
    if (t >= 0.0 && fixation_cross_train_DL_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_train_DL_2.tStart = t;  // (not accounting for frame time here)
      fixation_cross_train_DL_2.frameNStart = frameN;  // exact frame index
      
      fixation_cross_train_DL_2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_train_DL_2.status === PsychoJS.Status.STARTED || fixation_cross_train_DL_2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_train_DL_2.setAutoDraw(false);
    }
    
    // *arrow_image_train_DL* updates
    if (t >= 1 && arrow_image_train_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      arrow_image_train_DL.tStart = t;  // (not accounting for frame time here)
      arrow_image_train_DL.frameNStart = frameN;  // exact frame index
      
      arrow_image_train_DL.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((arrow_image_train_DL.status === PsychoJS.Status.STARTED || arrow_image_train_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      arrow_image_train_DL.setAutoDraw(false);
    }
    
    // *train_DL_text_arrow* updates
    if (t >= 1.35 && train_DL_text_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      train_DL_text_arrow.tStart = t;  // (not accounting for frame time here)
      train_DL_text_arrow.frameNStart = frameN;  // exact frame index
      
      train_DL_text_arrow.setAutoDraw(true);
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((train_DL_text_arrow.status === PsychoJS.Status.STARTED || train_DL_text_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      train_DL_text_arrow.setAutoDraw(false);
    }
    
    // *resp_train_DL_arrow* updates
    if (t >= 1.35 && resp_train_DL_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_train_DL_arrow.tStart = t;  // (not accounting for frame time here)
      resp_train_DL_arrow.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_train_DL_arrow.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_train_DL_arrow.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_train_DL_arrow.clearEvents(); });
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_train_DL_arrow.status === PsychoJS.Status.STARTED || resp_train_DL_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_train_DL_arrow.status = PsychoJS.Status.FINISHED;
  }

    if (resp_train_DL_arrow.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_train_DL_arrow.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_train_DL_arrow_allKeys = _resp_train_DL_arrow_allKeys.concat(theseKeys);
      if (_resp_train_DL_arrow_allKeys.length > 0) {
        resp_train_DL_arrow.keys = _resp_train_DL_arrow_allKeys[_resp_train_DL_arrow_allKeys.length - 1].name;  // just the last key pressed
        resp_train_DL_arrow.rt = _resp_train_DL_arrow_allKeys[_resp_train_DL_arrow_allKeys.length - 1].rt;
        // was this correct?
        if (resp_train_DL_arrow.keys == key_corr) {
            resp_train_DL_arrow.corr = 1;
        } else {
            resp_train_DL_arrow.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Train_DL_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Train_DL_ArrowRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Train_DL_Arrow'-------
    Train_DL_ArrowComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_train_DL_arrow.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_train_DL_arrow.corr = 1;  // correct non-response
      } else {
         resp_train_DL_arrow.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_train_DL_arrow.keys', resp_train_DL_arrow.keys);
    psychoJS.experiment.addData('resp_train_DL_arrow.corr', resp_train_DL_arrow.corr);
    if (typeof resp_train_DL_arrow.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_train_DL_arrow.rt', resp_train_DL_arrow.rt);
        routineTimer.reset();
        }
    
    resp_train_DL_arrow.stop();
    return Scheduler.Event.NEXT;
  };
}

function Instructions_DL8RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_DL8'-------
    t = 0;
    Instructions_DL8Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_DL8.keys = undefined;
    resp_next_DL8.rt = undefined;
    _resp_next_DL8_allKeys = [];
    // keep track of which components have finished
    Instructions_DL8Components = [];
    Instructions_DL8Components.push(intro_DL8);
    Instructions_DL8Components.push(resp_next_DL8);
    
    Instructions_DL8Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_DL8RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_DL8'-------
    // get current time
    t = Instructions_DL8Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_DL8* updates
    if (t >= 0.0 && intro_DL8.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_DL8.tStart = t;  // (not accounting for frame time here)
      intro_DL8.frameNStart = frameN;  // exact frame index
      
      intro_DL8.setAutoDraw(true);
    }

    
    // *resp_next_DL8* updates
    if (t >= 1 && resp_next_DL8.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_DL8.tStart = t;  // (not accounting for frame time here)
      resp_next_DL8.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_DL8.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL8.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_DL8.clearEvents(); });
    }

    if (resp_next_DL8.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_DL8.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_DL8_allKeys = _resp_next_DL8_allKeys.concat(theseKeys);
      if (_resp_next_DL8_allKeys.length > 0) {
        resp_next_DL8.keys = _resp_next_DL8_allKeys[_resp_next_DL8_allKeys.length - 1].name;  // just the last key pressed
        resp_next_DL8.rt = _resp_next_DL8_allKeys[_resp_next_DL8_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_DL8Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_DL8RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_DL8'-------
    Instructions_DL8Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_DL8.keys', resp_next_DL8.keys);
    if (typeof resp_next_DL8.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_DL8.rt', resp_next_DL8.rt);
        routineTimer.reset();
        }
    
    resp_next_DL8.stop();
    // the Routine "Instructions_DL8" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Test_DLRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Test_DL'-------
    t = 0;
    Test_DLClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.000000);
    // update component parameters for each repeat
    if (((keys_resp.index("a") === 0) && (RESPOSTA_DL_TESTE === 1))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (RESPOSTA_DL_TESTE === 0))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (RESPOSTA_DL_TESTE === 1))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (RESPOSTA_DL_TESTE === 0))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    text_test_DL.setText(VERBO_DL_TESTE);
    resp_test_DL.keys = undefined;
    resp_test_DL.rt = undefined;
    _resp_test_DL_allKeys = [];
    // keep track of which components have finished
    Test_DLComponents = [];
    Test_DLComponents.push(fixation_cross_test_DL);
    Test_DLComponents.push(text_test_DL);
    Test_DLComponents.push(resp_test_DL);
    
    Test_DLComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Test_DLRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Test_DL'-------
    // get current time
    t = Test_DLClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_test_DL* updates
    if (t >= 0.0 && fixation_cross_test_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_test_DL.tStart = t;  // (not accounting for frame time here)
      fixation_cross_test_DL.frameNStart = frameN;  // exact frame index
      
      fixation_cross_test_DL.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_test_DL.status === PsychoJS.Status.STARTED || fixation_cross_test_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_test_DL.setAutoDraw(false);
    }
    
    // *text_test_DL* updates
    if (t >= 1 && text_test_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_test_DL.tStart = t;  // (not accounting for frame time here)
      text_test_DL.frameNStart = frameN;  // exact frame index
      
      text_test_DL.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((text_test_DL.status === PsychoJS.Status.STARTED || text_test_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      text_test_DL.setAutoDraw(false);
    }
    
    // *resp_test_DL* updates
    if (t >= 1 && resp_test_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_test_DL.tStart = t;  // (not accounting for frame time here)
      resp_test_DL.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_test_DL.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_test_DL.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_test_DL.clearEvents(); });
    }

    frameRemains = 1 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_test_DL.status === PsychoJS.Status.STARTED || resp_test_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_test_DL.status = PsychoJS.Status.FINISHED;
  }

    if (resp_test_DL.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_test_DL.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_test_DL_allKeys = _resp_test_DL_allKeys.concat(theseKeys);
      if (_resp_test_DL_allKeys.length > 0) {
        resp_test_DL.keys = _resp_test_DL_allKeys[_resp_test_DL_allKeys.length - 1].name;  // just the last key pressed
        resp_test_DL.rt = _resp_test_DL_allKeys[_resp_test_DL_allKeys.length - 1].rt;
        // was this correct?
        if (resp_test_DL.keys == key_corr) {
            resp_test_DL.corr = 1;
        } else {
            resp_test_DL.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Test_DLComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Test_DLRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Test_DL'-------
    Test_DLComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_test_DL.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_test_DL.corr = 1;  // correct non-response
      } else {
         resp_test_DL.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_test_DL.keys', resp_test_DL.keys);
    psychoJS.experiment.addData('resp_test_DL.corr', resp_test_DL.corr);
    if (typeof resp_test_DL.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_test_DL.rt', resp_test_DL.rt);
        routineTimer.reset();
        }
    
    resp_test_DL.stop();
    return Scheduler.Event.NEXT;
  };
}

function Test_DL_ArrowRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Test_DL_Arrow'-------
    t = 0;
    Test_DL_ArrowClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.350000);
    // update component parameters for each repeat
    if (((Seq_Test_DL.thisTrialN % 15) !== 0)) {
        continueRoutine = false;
    }
    
    arrow_angle = (90 * ((round(np.random.rand()) * 2) - 1));
    thisExp.addData("test_DL_arrow_angle", arrow_angle);
    if (((keys_resp.index("a") === 0) && (arrow_angle === 90))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (arrow_angle === (- 90)))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (arrow_angle === 90))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (arrow_angle === (- 90)))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    arrow_image_test_DL.setOri(arrow_angle);
    resp_test_DL_arrow.keys = undefined;
    resp_test_DL_arrow.rt = undefined;
    _resp_test_DL_arrow_allKeys = [];
    // keep track of which components have finished
    Test_DL_ArrowComponents = [];
    Test_DL_ArrowComponents.push(fixation_cross_test_DL_2);
    Test_DL_ArrowComponents.push(arrow_image_test_DL);
    Test_DL_ArrowComponents.push(test_DL_text_arrow);
    Test_DL_ArrowComponents.push(resp_test_DL_arrow);
    
    Test_DL_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Test_DL_ArrowRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Test_DL_Arrow'-------
    // get current time
    t = Test_DL_ArrowClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_test_DL_2* updates
    if (t >= 0.0 && fixation_cross_test_DL_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_test_DL_2.tStart = t;  // (not accounting for frame time here)
      fixation_cross_test_DL_2.frameNStart = frameN;  // exact frame index
      
      fixation_cross_test_DL_2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_test_DL_2.status === PsychoJS.Status.STARTED || fixation_cross_test_DL_2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_test_DL_2.setAutoDraw(false);
    }
    
    // *arrow_image_test_DL* updates
    if (t >= 1 && arrow_image_test_DL.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      arrow_image_test_DL.tStart = t;  // (not accounting for frame time here)
      arrow_image_test_DL.frameNStart = frameN;  // exact frame index
      
      arrow_image_test_DL.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((arrow_image_test_DL.status === PsychoJS.Status.STARTED || arrow_image_test_DL.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      arrow_image_test_DL.setAutoDraw(false);
    }
    
    // *test_DL_text_arrow* updates
    if (t >= 1.35 && test_DL_text_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      test_DL_text_arrow.tStart = t;  // (not accounting for frame time here)
      test_DL_text_arrow.frameNStart = frameN;  // exact frame index
      
      test_DL_text_arrow.setAutoDraw(true);
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((test_DL_text_arrow.status === PsychoJS.Status.STARTED || test_DL_text_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      test_DL_text_arrow.setAutoDraw(false);
    }
    
    // *resp_test_DL_arrow* updates
    if (t >= 1.35 && resp_test_DL_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_test_DL_arrow.tStart = t;  // (not accounting for frame time here)
      resp_test_DL_arrow.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_test_DL_arrow.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_test_DL_arrow.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_test_DL_arrow.clearEvents(); });
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_test_DL_arrow.status === PsychoJS.Status.STARTED || resp_test_DL_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_test_DL_arrow.status = PsychoJS.Status.FINISHED;
  }

    if (resp_test_DL_arrow.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_test_DL_arrow.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_test_DL_arrow_allKeys = _resp_test_DL_arrow_allKeys.concat(theseKeys);
      if (_resp_test_DL_arrow_allKeys.length > 0) {
        resp_test_DL_arrow.keys = _resp_test_DL_arrow_allKeys[_resp_test_DL_arrow_allKeys.length - 1].name;  // just the last key pressed
        resp_test_DL_arrow.rt = _resp_test_DL_arrow_allKeys[_resp_test_DL_arrow_allKeys.length - 1].rt;
        // was this correct?
        if (resp_test_DL_arrow.keys == key_corr) {
            resp_test_DL_arrow.corr = 1;
        } else {
            resp_test_DL_arrow.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Test_DL_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Test_DL_ArrowRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Test_DL_Arrow'-------
    Test_DL_ArrowComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_test_DL_arrow.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_test_DL_arrow.corr = 1;  // correct non-response
      } else {
         resp_test_DL_arrow.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_test_DL_arrow.keys', resp_test_DL_arrow.keys);
    psychoJS.experiment.addData('resp_test_DL_arrow.corr', resp_test_DL_arrow.corr);
    if (typeof resp_test_DL_arrow.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_test_DL_arrow.rt', resp_test_DL_arrow.rt);
        routineTimer.reset();
        }
    
    resp_test_DL_arrow.stop();
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS1_1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS1_1'-------
    t = 0;
    Instructions_JS1_1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS1_1.keys = undefined;
    resp_next_JS1_1.rt = undefined;
    _resp_next_JS1_1_allKeys = [];
    // keep track of which components have finished
    Instructions_JS1_1Components = [];
    Instructions_JS1_1Components.push(JS1_1);
    Instructions_JS1_1Components.push(intro_JS1_1);
    Instructions_JS1_1Components.push(next_JS1_1);
    Instructions_JS1_1Components.push(resp_next_JS1_1);
    
    Instructions_JS1_1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS1_1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS1_1'-------
    // get current time
    t = Instructions_JS1_1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *JS1_1* updates
    if (t >= 0.0 && JS1_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      JS1_1.tStart = t;  // (not accounting for frame time here)
      JS1_1.frameNStart = frameN;  // exact frame index
      
      JS1_1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((JS1_1.status === PsychoJS.Status.STARTED || JS1_1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      JS1_1.setAutoDraw(false);
    }
    
    // *intro_JS1_1* updates
    if (t >= 1 && intro_JS1_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS1_1.tStart = t;  // (not accounting for frame time here)
      intro_JS1_1.frameNStart = frameN;  // exact frame index
      
      intro_JS1_1.setAutoDraw(true);
    }

    
    // *next_JS1_1* updates
    if (t >= 2 && next_JS1_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_JS1_1.tStart = t;  // (not accounting for frame time here)
      next_JS1_1.frameNStart = frameN;  // exact frame index
      
      next_JS1_1.setAutoDraw(true);
    }

    
    // *resp_next_JS1_1* updates
    if (t >= 2 && resp_next_JS1_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS1_1.tStart = t;  // (not accounting for frame time here)
      resp_next_JS1_1.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS1_1.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_1.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_1.clearEvents(); });
    }

    if (resp_next_JS1_1.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS1_1.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS1_1_allKeys = _resp_next_JS1_1_allKeys.concat(theseKeys);
      if (_resp_next_JS1_1_allKeys.length > 0) {
        resp_next_JS1_1.keys = _resp_next_JS1_1_allKeys[_resp_next_JS1_1_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS1_1.rt = _resp_next_JS1_1_allKeys[_resp_next_JS1_1_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS1_1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS1_1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS1_1'-------
    Instructions_JS1_1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS1_1.keys', resp_next_JS1_1.keys);
    if (typeof resp_next_JS1_1.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS1_1.rt', resp_next_JS1_1.rt);
        routineTimer.reset();
        }
    
    resp_next_JS1_1.stop();
    // the Routine "Instructions_JS1_1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS1_2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS1_2'-------
    t = 0;
    Instructions_JS1_2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS1_2.keys = undefined;
    resp_next_JS1_2.rt = undefined;
    _resp_next_JS1_2_allKeys = [];
    // keep track of which components have finished
    Instructions_JS1_2Components = [];
    Instructions_JS1_2Components.push(intro_JS1_2);
    Instructions_JS1_2Components.push(next_JS1_2);
    Instructions_JS1_2Components.push(resp_next_JS1_2);
    
    Instructions_JS1_2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS1_2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS1_2'-------
    // get current time
    t = Instructions_JS1_2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_JS1_2* updates
    if (t >= 0 && intro_JS1_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS1_2.tStart = t;  // (not accounting for frame time here)
      intro_JS1_2.frameNStart = frameN;  // exact frame index
      
      intro_JS1_2.setAutoDraw(true);
    }

    
    // *next_JS1_2* updates
    if (t >= 1 && next_JS1_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_JS1_2.tStart = t;  // (not accounting for frame time here)
      next_JS1_2.frameNStart = frameN;  // exact frame index
      
      next_JS1_2.setAutoDraw(true);
    }

    
    // *resp_next_JS1_2* updates
    if (t >= 1 && resp_next_JS1_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS1_2.tStart = t;  // (not accounting for frame time here)
      resp_next_JS1_2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS1_2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_2.clearEvents(); });
    }

    if (resp_next_JS1_2.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS1_2.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS1_2_allKeys = _resp_next_JS1_2_allKeys.concat(theseKeys);
      if (_resp_next_JS1_2_allKeys.length > 0) {
        resp_next_JS1_2.keys = _resp_next_JS1_2_allKeys[_resp_next_JS1_2_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS1_2.rt = _resp_next_JS1_2_allKeys[_resp_next_JS1_2_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS1_2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS1_2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS1_2'-------
    Instructions_JS1_2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS1_2.keys', resp_next_JS1_2.keys);
    if (typeof resp_next_JS1_2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS1_2.rt', resp_next_JS1_2.rt);
        routineTimer.reset();
        }
    
    resp_next_JS1_2.stop();
    // the Routine "Instructions_JS1_2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS1_3RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS1_3'-------
    t = 0;
    Instructions_JS1_3Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS1_3.keys = undefined;
    resp_next_JS1_3.rt = undefined;
    _resp_next_JS1_3_allKeys = [];
    // keep track of which components have finished
    Instructions_JS1_3Components = [];
    Instructions_JS1_3Components.push(intro_JS1_3);
    Instructions_JS1_3Components.push(resp_next_JS1_3);
    
    Instructions_JS1_3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS1_3RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS1_3'-------
    // get current time
    t = Instructions_JS1_3Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_JS1_3* updates
    if (t >= 0 && intro_JS1_3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS1_3.tStart = t;  // (not accounting for frame time here)
      intro_JS1_3.frameNStart = frameN;  // exact frame index
      
      intro_JS1_3.setAutoDraw(true);
    }

    
    // *resp_next_JS1_3* updates
    if (t >= 1 && resp_next_JS1_3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS1_3.tStart = t;  // (not accounting for frame time here)
      resp_next_JS1_3.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS1_3.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_3.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_3.clearEvents(); });
    }

    if (resp_next_JS1_3.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS1_3.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS1_3_allKeys = _resp_next_JS1_3_allKeys.concat(theseKeys);
      if (_resp_next_JS1_3_allKeys.length > 0) {
        resp_next_JS1_3.keys = _resp_next_JS1_3_allKeys[_resp_next_JS1_3_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS1_3.rt = _resp_next_JS1_3_allKeys[_resp_next_JS1_3_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS1_3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS1_3RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS1_3'-------
    Instructions_JS1_3Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS1_3.keys', resp_next_JS1_3.keys);
    if (typeof resp_next_JS1_3.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS1_3.rt', resp_next_JS1_3.rt);
        routineTimer.reset();
        }
    
    resp_next_JS1_3.stop();
    // the Routine "Instructions_JS1_3" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Train_JS1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Train_JS1'-------
    t = 0;
    Train_JS1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    if (((keys_resp.index("a") === 0) && (RESPOSTA_JS1_TREINO === 1))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (RESPOSTA_JS1_TREINO === 0))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (RESPOSTA_JS1_TREINO === 1))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (RESPOSTA_JS1_TREINO === 0))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    text_train_JS1.setText(VERBO_JS1_TREINO);
    resp_train_JS1.keys = undefined;
    resp_train_JS1.rt = undefined;
    _resp_train_JS1_allKeys = [];
    // keep track of which components have finished
    Train_JS1Components = [];
    Train_JS1Components.push(fixation_cross_train_JS1);
    Train_JS1Components.push(text_train_JS1);
    Train_JS1Components.push(resp_train_JS1);
    
    Train_JS1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Train_JS1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Train_JS1'-------
    // get current time
    t = Train_JS1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_train_JS1* updates
    if (t >= 0.0 && fixation_cross_train_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_train_JS1.tStart = t;  // (not accounting for frame time here)
      fixation_cross_train_JS1.frameNStart = frameN;  // exact frame index
      
      fixation_cross_train_JS1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_train_JS1.status === PsychoJS.Status.STARTED || fixation_cross_train_JS1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_train_JS1.setAutoDraw(false);
    }
    
    // *text_train_JS1* updates
    if (t >= 1 && text_train_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_train_JS1.tStart = t;  // (not accounting for frame time here)
      text_train_JS1.frameNStart = frameN;  // exact frame index
      
      text_train_JS1.setAutoDraw(true);
    }

    
    // *resp_train_JS1* updates
    if (t >= 1 && resp_train_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_train_JS1.tStart = t;  // (not accounting for frame time here)
      resp_train_JS1.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_train_JS1.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS1.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS1.clearEvents(); });
    }

    if (resp_train_JS1.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_train_JS1.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_train_JS1_allKeys = _resp_train_JS1_allKeys.concat(theseKeys);
      if (_resp_train_JS1_allKeys.length > 0) {
        resp_train_JS1.keys = _resp_train_JS1_allKeys[_resp_train_JS1_allKeys.length - 1].name;  // just the last key pressed
        resp_train_JS1.rt = _resp_train_JS1_allKeys[_resp_train_JS1_allKeys.length - 1].rt;
        // was this correct?
        if (resp_train_JS1.keys == key_corr) {
            resp_train_JS1.corr = 1;
        } else {
            resp_train_JS1.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Train_JS1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Train_JS1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Train_JS1'-------
    Train_JS1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_train_JS1.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_train_JS1.corr = 1;  // correct non-response
      } else {
         resp_train_JS1.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_train_JS1.keys', resp_train_JS1.keys);
    psychoJS.experiment.addData('resp_train_JS1.corr', resp_train_JS1.corr);
    if (typeof resp_train_JS1.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_train_JS1.rt', resp_train_JS1.rt);
        routineTimer.reset();
        }
    
    resp_train_JS1.stop();
    // the Routine "Train_JS1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Feedback_Train_JS1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Feedback_Train_JS1'-------
    t = 0;
    Feedback_Train_JS1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(1.000000);
    // update component parameters for each repeat
    if ((resp_train_JS1.corr === 1)) {
        msg = "Acertou!";
    } else {
        msg = "Errou";
    }
    
    feedback_text_train_JS1.setText(msg);
    // keep track of which components have finished
    Feedback_Train_JS1Components = [];
    Feedback_Train_JS1Components.push(feedback_text_train_JS1);
    
    Feedback_Train_JS1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Feedback_Train_JS1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Feedback_Train_JS1'-------
    // get current time
    t = Feedback_Train_JS1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *feedback_text_train_JS1* updates
    if (t >= 0.0 && feedback_text_train_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      feedback_text_train_JS1.tStart = t;  // (not accounting for frame time here)
      feedback_text_train_JS1.frameNStart = frameN;  // exact frame index
      
      feedback_text_train_JS1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((feedback_text_train_JS1.status === PsychoJS.Status.STARTED || feedback_text_train_JS1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      feedback_text_train_JS1.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Feedback_Train_JS1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Feedback_Train_JS1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Feedback_Train_JS1'-------
    Feedback_Train_JS1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    respcorrs.append(resp_train_JS1.corr);
    if (((respcorrs.length % 20) === 0)) {
        seq = respcorrs.slice((20 * (Number.parseInt((respcorrs.length / 20)) - 1)));
        if ((list(filter((x) => {
        return (x === 1);
    }, seq)).length > 12)) {
            Seq_Train_JS1.finished = true;
        }
    }
    
    return Scheduler.Event.NEXT;
  };
}

function Train_JS1_ArrowRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Train_JS1_Arrow'-------
    t = 0;
    Train_JS1_ArrowClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.350000);
    // update component parameters for each repeat
    if (((Seq_Train_JS1.thisTrialN % 15) !== 0)) {
        continueRoutine = false;
    }
    
    arrow_angle = (90 * ((round(np.random.rand()) * 2) - 1));
    thisExp.addData("train_JS1_arrow_angle", arrow_angle);
    if (((keys_resp.index("a") === 0) && (arrow_angle === 90))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (arrow_angle === (- 90)))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (arrow_angle === 90))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (arrow_angle === (- 90)))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    arrow_image_train_JS1.setOri(arrow_angle);
    resp_train_JS1_arrow.keys = undefined;
    resp_train_JS1_arrow.rt = undefined;
    _resp_train_JS1_arrow_allKeys = [];
    // keep track of which components have finished
    Train_JS1_ArrowComponents = [];
    Train_JS1_ArrowComponents.push(fixation_cross_train_JS1_2);
    Train_JS1_ArrowComponents.push(arrow_image_train_JS1);
    Train_JS1_ArrowComponents.push(train_JS1_text_arrow);
    Train_JS1_ArrowComponents.push(resp_train_JS1_arrow);
    
    Train_JS1_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Train_JS1_ArrowRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Train_JS1_Arrow'-------
    // get current time
    t = Train_JS1_ArrowClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_train_JS1_2* updates
    if (t >= 0.0 && fixation_cross_train_JS1_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_train_JS1_2.tStart = t;  // (not accounting for frame time here)
      fixation_cross_train_JS1_2.frameNStart = frameN;  // exact frame index
      
      fixation_cross_train_JS1_2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_train_JS1_2.status === PsychoJS.Status.STARTED || fixation_cross_train_JS1_2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_train_JS1_2.setAutoDraw(false);
    }
    
    // *arrow_image_train_JS1* updates
    if (t >= 1 && arrow_image_train_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      arrow_image_train_JS1.tStart = t;  // (not accounting for frame time here)
      arrow_image_train_JS1.frameNStart = frameN;  // exact frame index
      
      arrow_image_train_JS1.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((arrow_image_train_JS1.status === PsychoJS.Status.STARTED || arrow_image_train_JS1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      arrow_image_train_JS1.setAutoDraw(false);
    }
    
    // *train_JS1_text_arrow* updates
    if (t >= 1.35 && train_JS1_text_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      train_JS1_text_arrow.tStart = t;  // (not accounting for frame time here)
      train_JS1_text_arrow.frameNStart = frameN;  // exact frame index
      
      train_JS1_text_arrow.setAutoDraw(true);
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((train_JS1_text_arrow.status === PsychoJS.Status.STARTED || train_JS1_text_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      train_JS1_text_arrow.setAutoDraw(false);
    }
    
    // *resp_train_JS1_arrow* updates
    if (t >= 1.35 && resp_train_JS1_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_train_JS1_arrow.tStart = t;  // (not accounting for frame time here)
      resp_train_JS1_arrow.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_train_JS1_arrow.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS1_arrow.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS1_arrow.clearEvents(); });
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_train_JS1_arrow.status === PsychoJS.Status.STARTED || resp_train_JS1_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_train_JS1_arrow.status = PsychoJS.Status.FINISHED;
  }

    if (resp_train_JS1_arrow.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_train_JS1_arrow.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_train_JS1_arrow_allKeys = _resp_train_JS1_arrow_allKeys.concat(theseKeys);
      if (_resp_train_JS1_arrow_allKeys.length > 0) {
        resp_train_JS1_arrow.keys = _resp_train_JS1_arrow_allKeys[_resp_train_JS1_arrow_allKeys.length - 1].name;  // just the last key pressed
        resp_train_JS1_arrow.rt = _resp_train_JS1_arrow_allKeys[_resp_train_JS1_arrow_allKeys.length - 1].rt;
        // was this correct?
        if (resp_train_JS1_arrow.keys == key_corr) {
            resp_train_JS1_arrow.corr = 1;
        } else {
            resp_train_JS1_arrow.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Train_JS1_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Train_JS1_ArrowRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Train_JS1_Arrow'-------
    Train_JS1_ArrowComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_train_JS1_arrow.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_train_JS1_arrow.corr = 1;  // correct non-response
      } else {
         resp_train_JS1_arrow.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_train_JS1_arrow.keys', resp_train_JS1_arrow.keys);
    psychoJS.experiment.addData('resp_train_JS1_arrow.corr', resp_train_JS1_arrow.corr);
    if (typeof resp_train_JS1_arrow.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_train_JS1_arrow.rt', resp_train_JS1_arrow.rt);
        routineTimer.reset();
        }
    
    resp_train_JS1_arrow.stop();
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS1_4RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS1_4'-------
    t = 0;
    Instructions_JS1_4Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS1_4.keys = undefined;
    resp_next_JS1_4.rt = undefined;
    _resp_next_JS1_4_allKeys = [];
    // keep track of which components have finished
    Instructions_JS1_4Components = [];
    Instructions_JS1_4Components.push(intro_JS1_4);
    Instructions_JS1_4Components.push(resp_next_JS1_4);
    
    Instructions_JS1_4Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS1_4RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS1_4'-------
    // get current time
    t = Instructions_JS1_4Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_JS1_4* updates
    if (t >= 0.0 && intro_JS1_4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS1_4.tStart = t;  // (not accounting for frame time here)
      intro_JS1_4.frameNStart = frameN;  // exact frame index
      
      intro_JS1_4.setAutoDraw(true);
    }

    
    // *resp_next_JS1_4* updates
    if (t >= 1 && resp_next_JS1_4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS1_4.tStart = t;  // (not accounting for frame time here)
      resp_next_JS1_4.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS1_4.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_4.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS1_4.clearEvents(); });
    }

    if (resp_next_JS1_4.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS1_4.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS1_4_allKeys = _resp_next_JS1_4_allKeys.concat(theseKeys);
      if (_resp_next_JS1_4_allKeys.length > 0) {
        resp_next_JS1_4.keys = _resp_next_JS1_4_allKeys[_resp_next_JS1_4_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS1_4.rt = _resp_next_JS1_4_allKeys[_resp_next_JS1_4_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS1_4Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS1_4RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS1_4'-------
    Instructions_JS1_4Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS1_4.keys', resp_next_JS1_4.keys);
    if (typeof resp_next_JS1_4.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS1_4.rt', resp_next_JS1_4.rt);
        routineTimer.reset();
        }
    
    resp_next_JS1_4.stop();
    // the Routine "Instructions_JS1_4" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Test_JS1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Test_JS1'-------
    t = 0;
    Test_JS1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    if (((keys_resp.index("a") === 0) && (RESPOSTA_JS1_TESTE === 1))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (RESPOSTA_JS1_TESTE === 0))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (RESPOSTA_JS1_TESTE === 1))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (RESPOSTA_JS1_TESTE === 0))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    text_test_JS1.setText(VERBO_JS1_TESTE);
    resp_test_JS1.keys = undefined;
    resp_test_JS1.rt = undefined;
    _resp_test_JS1_allKeys = [];
    // keep track of which components have finished
    Test_JS1Components = [];
    Test_JS1Components.push(fixation_cross_test_JS1_1);
    Test_JS1Components.push(text_test_JS1);
    Test_JS1Components.push(resp_test_JS1);
    
    Test_JS1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Test_JS1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Test_JS1'-------
    // get current time
    t = Test_JS1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_test_JS1_1* updates
    if (t >= 0.0 && fixation_cross_test_JS1_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_test_JS1_1.tStart = t;  // (not accounting for frame time here)
      fixation_cross_test_JS1_1.frameNStart = frameN;  // exact frame index
      
      fixation_cross_test_JS1_1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_test_JS1_1.status === PsychoJS.Status.STARTED || fixation_cross_test_JS1_1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_test_JS1_1.setAutoDraw(false);
    }
    
    // *text_test_JS1* updates
    if (t >= 1 && text_test_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_test_JS1.tStart = t;  // (not accounting for frame time here)
      text_test_JS1.frameNStart = frameN;  // exact frame index
      
      text_test_JS1.setAutoDraw(true);
    }

    
    // *resp_test_JS1* updates
    if (t >= 1 && resp_test_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_test_JS1.tStart = t;  // (not accounting for frame time here)
      resp_test_JS1.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_test_JS1.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS1.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS1.clearEvents(); });
    }

    if (resp_test_JS1.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_test_JS1.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_test_JS1_allKeys = _resp_test_JS1_allKeys.concat(theseKeys);
      if (_resp_test_JS1_allKeys.length > 0) {
        resp_test_JS1.keys = _resp_test_JS1_allKeys[_resp_test_JS1_allKeys.length - 1].name;  // just the last key pressed
        resp_test_JS1.rt = _resp_test_JS1_allKeys[_resp_test_JS1_allKeys.length - 1].rt;
        // was this correct?
        if (resp_test_JS1.keys == key_corr) {
            resp_test_JS1.corr = 1;
        } else {
            resp_test_JS1.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Test_JS1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Test_JS1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Test_JS1'-------
    Test_JS1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_test_JS1.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_test_JS1.corr = 1;  // correct non-response
      } else {
         resp_test_JS1.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_test_JS1.keys', resp_test_JS1.keys);
    psychoJS.experiment.addData('resp_test_JS1.corr', resp_test_JS1.corr);
    if (typeof resp_test_JS1.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_test_JS1.rt', resp_test_JS1.rt);
        routineTimer.reset();
        }
    
    resp_test_JS1.stop();
    // the Routine "Test_JS1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Test_JS1_ArrowRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Test_JS1_Arrow'-------
    t = 0;
    Test_JS1_ArrowClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.350000);
    // update component parameters for each repeat
    if (((Seq_Test_JS1.thisTrialN % 15) !== 0)) {
        continueRoutine = false;
    }
    
    arrow_angle = (90 * ((round(np.random.rand()) * 2) - 1));
    thisExp.addData("test_JS1_arrow_angle", arrow_angle);
    if (((keys_resp.index("a") === 0) && (arrow_angle === 90))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (arrow_angle === (- 90)))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (arrow_angle === 90))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (arrow_angle === (- 90)))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    arrow_image_test_JS1.setOri(arrow_angle);
    resp_test_JS1_arrow.keys = undefined;
    resp_test_JS1_arrow.rt = undefined;
    _resp_test_JS1_arrow_allKeys = [];
    // keep track of which components have finished
    Test_JS1_ArrowComponents = [];
    Test_JS1_ArrowComponents.push(fixation_cross_test_JS1);
    Test_JS1_ArrowComponents.push(arrow_image_test_JS1);
    Test_JS1_ArrowComponents.push(test_JS1_text_arrow);
    Test_JS1_ArrowComponents.push(resp_test_JS1_arrow);
    
    Test_JS1_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Test_JS1_ArrowRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Test_JS1_Arrow'-------
    // get current time
    t = Test_JS1_ArrowClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_test_JS1* updates
    if (t >= 0.0 && fixation_cross_test_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_test_JS1.tStart = t;  // (not accounting for frame time here)
      fixation_cross_test_JS1.frameNStart = frameN;  // exact frame index
      
      fixation_cross_test_JS1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_test_JS1.status === PsychoJS.Status.STARTED || fixation_cross_test_JS1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_test_JS1.setAutoDraw(false);
    }
    
    // *arrow_image_test_JS1* updates
    if (t >= 1 && arrow_image_test_JS1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      arrow_image_test_JS1.tStart = t;  // (not accounting for frame time here)
      arrow_image_test_JS1.frameNStart = frameN;  // exact frame index
      
      arrow_image_test_JS1.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((arrow_image_test_JS1.status === PsychoJS.Status.STARTED || arrow_image_test_JS1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      arrow_image_test_JS1.setAutoDraw(false);
    }
    
    // *test_JS1_text_arrow* updates
    if (t >= 1.35 && test_JS1_text_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      test_JS1_text_arrow.tStart = t;  // (not accounting for frame time here)
      test_JS1_text_arrow.frameNStart = frameN;  // exact frame index
      
      test_JS1_text_arrow.setAutoDraw(true);
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((test_JS1_text_arrow.status === PsychoJS.Status.STARTED || test_JS1_text_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      test_JS1_text_arrow.setAutoDraw(false);
    }
    
    // *resp_test_JS1_arrow* updates
    if (t >= 1.35 && resp_test_JS1_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_test_JS1_arrow.tStart = t;  // (not accounting for frame time here)
      resp_test_JS1_arrow.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_test_JS1_arrow.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS1_arrow.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS1_arrow.clearEvents(); });
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_test_JS1_arrow.status === PsychoJS.Status.STARTED || resp_test_JS1_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_test_JS1_arrow.status = PsychoJS.Status.FINISHED;
  }

    if (resp_test_JS1_arrow.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_test_JS1_arrow.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_test_JS1_arrow_allKeys = _resp_test_JS1_arrow_allKeys.concat(theseKeys);
      if (_resp_test_JS1_arrow_allKeys.length > 0) {
        resp_test_JS1_arrow.keys = _resp_test_JS1_arrow_allKeys[_resp_test_JS1_arrow_allKeys.length - 1].name;  // just the last key pressed
        resp_test_JS1_arrow.rt = _resp_test_JS1_arrow_allKeys[_resp_test_JS1_arrow_allKeys.length - 1].rt;
        // was this correct?
        if (resp_test_JS1_arrow.keys == key_corr) {
            resp_test_JS1_arrow.corr = 1;
        } else {
            resp_test_JS1_arrow.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Test_JS1_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Test_JS1_ArrowRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Test_JS1_Arrow'-------
    Test_JS1_ArrowComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_test_JS1_arrow.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_test_JS1_arrow.corr = 1;  // correct non-response
      } else {
         resp_test_JS1_arrow.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_test_JS1_arrow.keys', resp_test_JS1_arrow.keys);
    psychoJS.experiment.addData('resp_test_JS1_arrow.corr', resp_test_JS1_arrow.corr);
    if (typeof resp_test_JS1_arrow.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_test_JS1_arrow.rt', resp_test_JS1_arrow.rt);
        routineTimer.reset();
        }
    
    resp_test_JS1_arrow.stop();
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS2_1RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS2_1'-------
    t = 0;
    Instructions_JS2_1Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS2_1.keys = undefined;
    resp_next_JS2_1.rt = undefined;
    _resp_next_JS2_1_allKeys = [];
    // keep track of which components have finished
    Instructions_JS2_1Components = [];
    Instructions_JS2_1Components.push(JS2_1);
    Instructions_JS2_1Components.push(intro_JS2_1);
    Instructions_JS2_1Components.push(next_JS2_1);
    Instructions_JS2_1Components.push(resp_next_JS2_1);
    
    Instructions_JS2_1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS2_1RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS2_1'-------
    // get current time
    t = Instructions_JS2_1Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *JS2_1* updates
    if (t >= 0.0 && JS2_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      JS2_1.tStart = t;  // (not accounting for frame time here)
      JS2_1.frameNStart = frameN;  // exact frame index
      
      JS2_1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((JS2_1.status === PsychoJS.Status.STARTED || JS2_1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      JS2_1.setAutoDraw(false);
    }
    
    // *intro_JS2_1* updates
    if (t >= 1 && intro_JS2_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS2_1.tStart = t;  // (not accounting for frame time here)
      intro_JS2_1.frameNStart = frameN;  // exact frame index
      
      intro_JS2_1.setAutoDraw(true);
    }

    
    // *next_JS2_1* updates
    if (t >= 2 && next_JS2_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_JS2_1.tStart = t;  // (not accounting for frame time here)
      next_JS2_1.frameNStart = frameN;  // exact frame index
      
      next_JS2_1.setAutoDraw(true);
    }

    
    // *resp_next_JS2_1* updates
    if (t >= 2 && resp_next_JS2_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS2_1.tStart = t;  // (not accounting for frame time here)
      resp_next_JS2_1.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS2_1.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_1.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_1.clearEvents(); });
    }

    if (resp_next_JS2_1.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS2_1.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS2_1_allKeys = _resp_next_JS2_1_allKeys.concat(theseKeys);
      if (_resp_next_JS2_1_allKeys.length > 0) {
        resp_next_JS2_1.keys = _resp_next_JS2_1_allKeys[_resp_next_JS2_1_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS2_1.rt = _resp_next_JS2_1_allKeys[_resp_next_JS2_1_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS2_1Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS2_1RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS2_1'-------
    Instructions_JS2_1Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS2_1.keys', resp_next_JS2_1.keys);
    if (typeof resp_next_JS2_1.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS2_1.rt', resp_next_JS2_1.rt);
        routineTimer.reset();
        }
    
    resp_next_JS2_1.stop();
    // the Routine "Instructions_JS2_1" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS2_2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS2_2'-------
    t = 0;
    Instructions_JS2_2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS2_2.keys = undefined;
    resp_next_JS2_2.rt = undefined;
    _resp_next_JS2_2_allKeys = [];
    // keep track of which components have finished
    Instructions_JS2_2Components = [];
    Instructions_JS2_2Components.push(intro_JS2_2);
    Instructions_JS2_2Components.push(next_JS2_2);
    Instructions_JS2_2Components.push(resp_next_JS2_2);
    
    Instructions_JS2_2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS2_2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS2_2'-------
    // get current time
    t = Instructions_JS2_2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_JS2_2* updates
    if (t >= 0 && intro_JS2_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS2_2.tStart = t;  // (not accounting for frame time here)
      intro_JS2_2.frameNStart = frameN;  // exact frame index
      
      intro_JS2_2.setAutoDraw(true);
    }

    
    // *next_JS2_2* updates
    if (t >= 1 && next_JS2_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_JS2_2.tStart = t;  // (not accounting for frame time here)
      next_JS2_2.frameNStart = frameN;  // exact frame index
      
      next_JS2_2.setAutoDraw(true);
    }

    
    // *resp_next_JS2_2* updates
    if (t >= 1 && resp_next_JS2_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS2_2.tStart = t;  // (not accounting for frame time here)
      resp_next_JS2_2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS2_2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_2.clearEvents(); });
    }

    if (resp_next_JS2_2.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS2_2.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS2_2_allKeys = _resp_next_JS2_2_allKeys.concat(theseKeys);
      if (_resp_next_JS2_2_allKeys.length > 0) {
        resp_next_JS2_2.keys = _resp_next_JS2_2_allKeys[_resp_next_JS2_2_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS2_2.rt = _resp_next_JS2_2_allKeys[_resp_next_JS2_2_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS2_2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS2_2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS2_2'-------
    Instructions_JS2_2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS2_2.keys', resp_next_JS2_2.keys);
    if (typeof resp_next_JS2_2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS2_2.rt', resp_next_JS2_2.rt);
        routineTimer.reset();
        }
    
    resp_next_JS2_2.stop();
    // the Routine "Instructions_JS2_2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS2_3RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS2_3'-------
    t = 0;
    Instructions_JS2_3Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS2_3.keys = undefined;
    resp_next_JS2_3.rt = undefined;
    _resp_next_JS2_3_allKeys = [];
    // keep track of which components have finished
    Instructions_JS2_3Components = [];
    Instructions_JS2_3Components.push(intro_JS2_3);
    Instructions_JS2_3Components.push(resp_next_JS2_3);
    
    Instructions_JS2_3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS2_3RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS2_3'-------
    // get current time
    t = Instructions_JS2_3Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_JS2_3* updates
    if (t >= 0 && intro_JS2_3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS2_3.tStart = t;  // (not accounting for frame time here)
      intro_JS2_3.frameNStart = frameN;  // exact frame index
      
      intro_JS2_3.setAutoDraw(true);
    }

    
    // *resp_next_JS2_3* updates
    if (t >= 1 && resp_next_JS2_3.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS2_3.tStart = t;  // (not accounting for frame time here)
      resp_next_JS2_3.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS2_3.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_3.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_3.clearEvents(); });
    }

    if (resp_next_JS2_3.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS2_3.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS2_3_allKeys = _resp_next_JS2_3_allKeys.concat(theseKeys);
      if (_resp_next_JS2_3_allKeys.length > 0) {
        resp_next_JS2_3.keys = _resp_next_JS2_3_allKeys[_resp_next_JS2_3_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS2_3.rt = _resp_next_JS2_3_allKeys[_resp_next_JS2_3_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS2_3Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS2_3RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS2_3'-------
    Instructions_JS2_3Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS2_3.keys', resp_next_JS2_3.keys);
    if (typeof resp_next_JS2_3.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS2_3.rt', resp_next_JS2_3.rt);
        routineTimer.reset();
        }
    
    resp_next_JS2_3.stop();
    // the Routine "Instructions_JS2_3" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Train_JS2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Train_JS2'-------
    t = 0;
    Train_JS2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    if (((keys_resp.index("a") === 0) && (RESPOSTA_JS2_TREINO === 1))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (RESPOSTA_JS2_TREINO === 0))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (RESPOSTA_JS2_TREINO === 1))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (RESPOSTA_JS2_TREINO === 0))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    text_train_JS2.setText(VERBO_JS2_TREINO);
    resp_train_JS2.keys = undefined;
    resp_train_JS2.rt = undefined;
    _resp_train_JS2_allKeys = [];
    // keep track of which components have finished
    Train_JS2Components = [];
    Train_JS2Components.push(fixation_cross_train_JS2);
    Train_JS2Components.push(text_train_JS2);
    Train_JS2Components.push(resp_train_JS2);
    
    Train_JS2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Train_JS2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Train_JS2'-------
    // get current time
    t = Train_JS2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_train_JS2* updates
    if (t >= 0.0 && fixation_cross_train_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_train_JS2.tStart = t;  // (not accounting for frame time here)
      fixation_cross_train_JS2.frameNStart = frameN;  // exact frame index
      
      fixation_cross_train_JS2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_train_JS2.status === PsychoJS.Status.STARTED || fixation_cross_train_JS2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_train_JS2.setAutoDraw(false);
    }
    
    // *text_train_JS2* updates
    if (t >= 1 && text_train_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_train_JS2.tStart = t;  // (not accounting for frame time here)
      text_train_JS2.frameNStart = frameN;  // exact frame index
      
      text_train_JS2.setAutoDraw(true);
    }

    
    // *resp_train_JS2* updates
    if (t >= 1 && resp_train_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_train_JS2.tStart = t;  // (not accounting for frame time here)
      resp_train_JS2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_train_JS2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS2.clearEvents(); });
    }

    if (resp_train_JS2.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_train_JS2.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_train_JS2_allKeys = _resp_train_JS2_allKeys.concat(theseKeys);
      if (_resp_train_JS2_allKeys.length > 0) {
        resp_train_JS2.keys = _resp_train_JS2_allKeys[_resp_train_JS2_allKeys.length - 1].name;  // just the last key pressed
        resp_train_JS2.rt = _resp_train_JS2_allKeys[_resp_train_JS2_allKeys.length - 1].rt;
        // was this correct?
        if (resp_train_JS2.keys == key_corr) {
            resp_train_JS2.corr = 1;
        } else {
            resp_train_JS2.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Train_JS2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Train_JS2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Train_JS2'-------
    Train_JS2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_train_JS2.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_train_JS2.corr = 1;  // correct non-response
      } else {
         resp_train_JS2.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_train_JS2.keys', resp_train_JS2.keys);
    psychoJS.experiment.addData('resp_train_JS2.corr', resp_train_JS2.corr);
    if (typeof resp_train_JS2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_train_JS2.rt', resp_train_JS2.rt);
        routineTimer.reset();
        }
    
    resp_train_JS2.stop();
    // the Routine "Train_JS2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Feedback_Train_JS2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Feedback_Train_JS2'-------
    t = 0;
    Feedback_Train_JS2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(1.000000);
    // update component parameters for each repeat
    if ((resp_train_JS2.corr === 1)) {
        msg = "Acertou!";
    } else {
        msg = "Errou";
    }
    
    feedback_text_train_JS2.setText(msg);
    // keep track of which components have finished
    Feedback_Train_JS2Components = [];
    Feedback_Train_JS2Components.push(feedback_text_train_JS2);
    
    Feedback_Train_JS2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Feedback_Train_JS2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Feedback_Train_JS2'-------
    // get current time
    t = Feedback_Train_JS2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *feedback_text_train_JS2* updates
    if (t >= 0.0 && feedback_text_train_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      feedback_text_train_JS2.tStart = t;  // (not accounting for frame time here)
      feedback_text_train_JS2.frameNStart = frameN;  // exact frame index
      
      feedback_text_train_JS2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((feedback_text_train_JS2.status === PsychoJS.Status.STARTED || feedback_text_train_JS2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      feedback_text_train_JS2.setAutoDraw(false);
    }
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Feedback_Train_JS2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Feedback_Train_JS2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Feedback_Train_JS2'-------
    Feedback_Train_JS2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    respcorrs.append(resp_train_JS2.corr);
    if (((respcorrs.length % 20) === 0)) {
        seq = respcorrs.slice((20 * (Number.parseInt((respcorrs.length / 20)) - 1)));
        if ((list(filter((x) => {
        return (x === 1);
    }, seq)).length > 12)) {
            Seq_Train_JS2.finished = true;
        }
    }
    
    return Scheduler.Event.NEXT;
  };
}

function Train_JS2_ArrowRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Train_JS2_Arrow'-------
    t = 0;
    Train_JS2_ArrowClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.350000);
    // update component parameters for each repeat
    if (((Seq_Train_JS2.thisTrialN % 15) !== 0)) {
        continueRoutine = false;
    }
    
    arrow_angle = (90 * ((round(np.random.rand()) * 2) - 1));
    thisExp.addData("train_JS2_arrow_angle", arrow_angle);
    if (((keys_resp.index("a") === 0) && (arrow_angle === 90))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (arrow_angle === (- 90)))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (arrow_angle === 90))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (arrow_angle === (- 90)))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    arrow_image_train_JS2.setOri(arrow_angle);
    resp_train_JS2_arrow.keys = undefined;
    resp_train_JS2_arrow.rt = undefined;
    _resp_train_JS2_arrow_allKeys = [];
    // keep track of which components have finished
    Train_JS2_ArrowComponents = [];
    Train_JS2_ArrowComponents.push(fixation_cross_train_JS2_2);
    Train_JS2_ArrowComponents.push(arrow_image_train_JS2);
    Train_JS2_ArrowComponents.push(train_JS2_text_arrow);
    Train_JS2_ArrowComponents.push(resp_train_JS2_arrow);
    
    Train_JS2_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Train_JS2_ArrowRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Train_JS2_Arrow'-------
    // get current time
    t = Train_JS2_ArrowClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_train_JS2_2* updates
    if (t >= 0.0 && fixation_cross_train_JS2_2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_train_JS2_2.tStart = t;  // (not accounting for frame time here)
      fixation_cross_train_JS2_2.frameNStart = frameN;  // exact frame index
      
      fixation_cross_train_JS2_2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_train_JS2_2.status === PsychoJS.Status.STARTED || fixation_cross_train_JS2_2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_train_JS2_2.setAutoDraw(false);
    }
    
    // *arrow_image_train_JS2* updates
    if (t >= 1 && arrow_image_train_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      arrow_image_train_JS2.tStart = t;  // (not accounting for frame time here)
      arrow_image_train_JS2.frameNStart = frameN;  // exact frame index
      
      arrow_image_train_JS2.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((arrow_image_train_JS2.status === PsychoJS.Status.STARTED || arrow_image_train_JS2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      arrow_image_train_JS2.setAutoDraw(false);
    }
    
    // *train_JS2_text_arrow* updates
    if (t >= 1.35 && train_JS2_text_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      train_JS2_text_arrow.tStart = t;  // (not accounting for frame time here)
      train_JS2_text_arrow.frameNStart = frameN;  // exact frame index
      
      train_JS2_text_arrow.setAutoDraw(true);
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((train_JS2_text_arrow.status === PsychoJS.Status.STARTED || train_JS2_text_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      train_JS2_text_arrow.setAutoDraw(false);
    }
    
    // *resp_train_JS2_arrow* updates
    if (t >= 1.35 && resp_train_JS2_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_train_JS2_arrow.tStart = t;  // (not accounting for frame time here)
      resp_train_JS2_arrow.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_train_JS2_arrow.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS2_arrow.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_train_JS2_arrow.clearEvents(); });
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_train_JS2_arrow.status === PsychoJS.Status.STARTED || resp_train_JS2_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_train_JS2_arrow.status = PsychoJS.Status.FINISHED;
  }

    if (resp_train_JS2_arrow.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_train_JS2_arrow.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_train_JS2_arrow_allKeys = _resp_train_JS2_arrow_allKeys.concat(theseKeys);
      if (_resp_train_JS2_arrow_allKeys.length > 0) {
        resp_train_JS2_arrow.keys = _resp_train_JS2_arrow_allKeys[_resp_train_JS2_arrow_allKeys.length - 1].name;  // just the last key pressed
        resp_train_JS2_arrow.rt = _resp_train_JS2_arrow_allKeys[_resp_train_JS2_arrow_allKeys.length - 1].rt;
        // was this correct?
        if (resp_train_JS2_arrow.keys == key_corr) {
            resp_train_JS2_arrow.corr = 1;
        } else {
            resp_train_JS2_arrow.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Train_JS2_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Train_JS2_ArrowRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Train_JS2_Arrow'-------
    Train_JS2_ArrowComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_train_JS2_arrow.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_train_JS2_arrow.corr = 1;  // correct non-response
      } else {
         resp_train_JS2_arrow.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_train_JS2_arrow.keys', resp_train_JS2_arrow.keys);
    psychoJS.experiment.addData('resp_train_JS2_arrow.corr', resp_train_JS2_arrow.corr);
    if (typeof resp_train_JS2_arrow.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_train_JS2_arrow.rt', resp_train_JS2_arrow.rt);
        routineTimer.reset();
        }
    
    resp_train_JS2_arrow.stop();
    return Scheduler.Event.NEXT;
  };
}

function Instructions_JS2_4RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Instructions_JS2_4'-------
    t = 0;
    Instructions_JS2_4Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    resp_next_JS2_4.keys = undefined;
    resp_next_JS2_4.rt = undefined;
    _resp_next_JS2_4_allKeys = [];
    // keep track of which components have finished
    Instructions_JS2_4Components = [];
    Instructions_JS2_4Components.push(intro_JS2_4);
    Instructions_JS2_4Components.push(resp_next_JS2_4);
    
    Instructions_JS2_4Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Instructions_JS2_4RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Instructions_JS2_4'-------
    // get current time
    t = Instructions_JS2_4Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *intro_JS2_4* updates
    if (t >= 0.0 && intro_JS2_4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      intro_JS2_4.tStart = t;  // (not accounting for frame time here)
      intro_JS2_4.frameNStart = frameN;  // exact frame index
      
      intro_JS2_4.setAutoDraw(true);
    }

    
    // *resp_next_JS2_4* updates
    if (t >= 1 && resp_next_JS2_4.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_next_JS2_4.tStart = t;  // (not accounting for frame time here)
      resp_next_JS2_4.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_next_JS2_4.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_4.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_next_JS2_4.clearEvents(); });
    }

    if (resp_next_JS2_4.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_next_JS2_4.getKeys({keyList: ['space'], waitRelease: false});
      _resp_next_JS2_4_allKeys = _resp_next_JS2_4_allKeys.concat(theseKeys);
      if (_resp_next_JS2_4_allKeys.length > 0) {
        resp_next_JS2_4.keys = _resp_next_JS2_4_allKeys[_resp_next_JS2_4_allKeys.length - 1].name;  // just the last key pressed
        resp_next_JS2_4.rt = _resp_next_JS2_4_allKeys[_resp_next_JS2_4_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Instructions_JS2_4Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Instructions_JS2_4RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Instructions_JS2_4'-------
    Instructions_JS2_4Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('resp_next_JS2_4.keys', resp_next_JS2_4.keys);
    if (typeof resp_next_JS2_4.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_next_JS2_4.rt', resp_next_JS2_4.rt);
        routineTimer.reset();
        }
    
    resp_next_JS2_4.stop();
    // the Routine "Instructions_JS2_4" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Test_JS2RoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Test_JS2'-------
    t = 0;
    Test_JS2Clock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    if (((keys_resp.index("a") === 0) && (RESPOSTA_JS2_TESTE === 1))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (RESPOSTA_JS2_TESTE === 0))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (RESPOSTA_JS2_TESTE === 1))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (RESPOSTA_JS2_TESTE === 0))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    text_test_JS2.setText(VERBO_JS2_TESTE);
    resp_test_JS2.keys = undefined;
    resp_test_JS2.rt = undefined;
    _resp_test_JS2_allKeys = [];
    // keep track of which components have finished
    Test_JS2Components = [];
    Test_JS2Components.push(fixation_cross_test_JS2_1);
    Test_JS2Components.push(text_test_JS2);
    Test_JS2Components.push(resp_test_JS2);
    
    Test_JS2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Test_JS2RoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Test_JS2'-------
    // get current time
    t = Test_JS2Clock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_test_JS2_1* updates
    if (t >= 0.0 && fixation_cross_test_JS2_1.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_test_JS2_1.tStart = t;  // (not accounting for frame time here)
      fixation_cross_test_JS2_1.frameNStart = frameN;  // exact frame index
      
      fixation_cross_test_JS2_1.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_test_JS2_1.status === PsychoJS.Status.STARTED || fixation_cross_test_JS2_1.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_test_JS2_1.setAutoDraw(false);
    }
    
    // *text_test_JS2* updates
    if (t >= 1 && text_test_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      text_test_JS2.tStart = t;  // (not accounting for frame time here)
      text_test_JS2.frameNStart = frameN;  // exact frame index
      
      text_test_JS2.setAutoDraw(true);
    }

    
    // *resp_test_JS2* updates
    if (t >= 1 && resp_test_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_test_JS2.tStart = t;  // (not accounting for frame time here)
      resp_test_JS2.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_test_JS2.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS2.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS2.clearEvents(); });
    }

    if (resp_test_JS2.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_test_JS2.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_test_JS2_allKeys = _resp_test_JS2_allKeys.concat(theseKeys);
      if (_resp_test_JS2_allKeys.length > 0) {
        resp_test_JS2.keys = _resp_test_JS2_allKeys[_resp_test_JS2_allKeys.length - 1].name;  // just the last key pressed
        resp_test_JS2.rt = _resp_test_JS2_allKeys[_resp_test_JS2_allKeys.length - 1].rt;
        // was this correct?
        if (resp_test_JS2.keys == key_corr) {
            resp_test_JS2.corr = 1;
        } else {
            resp_test_JS2.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Test_JS2Components.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Test_JS2RoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Test_JS2'-------
    Test_JS2Components.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_test_JS2.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_test_JS2.corr = 1;  // correct non-response
      } else {
         resp_test_JS2.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_test_JS2.keys', resp_test_JS2.keys);
    psychoJS.experiment.addData('resp_test_JS2.corr', resp_test_JS2.corr);
    if (typeof resp_test_JS2.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_test_JS2.rt', resp_test_JS2.rt);
        routineTimer.reset();
        }
    
    resp_test_JS2.stop();
    // the Routine "Test_JS2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function Test_JS2_ArrowRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'Test_JS2_Arrow'-------
    t = 0;
    Test_JS2_ArrowClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    routineTimer.add(5.350000);
    // update component parameters for each repeat
    if (((Seq_Test_JS2.thisTrialN % 15) !== 0)) {
        continueRoutine = false;
    }
    
    arrow_angle = (90 * ((round(np.random.rand()) * 2) - 1));
    thisExp.addData("test_JS2_arrow_angle", arrow_angle);
    if (((keys_resp.index("a") === 0) && (arrow_angle === 90))) {
        key_corr = "a";
    } else {
        if (((keys_resp.index("l") === 1) && (arrow_angle === (- 90)))) {
            key_corr = "l";
        } else {
            if (((keys_resp.index("l") === 0) && (arrow_angle === 90))) {
                key_corr = "l";
            } else {
                if (((keys_resp.index("a") === 1) && (arrow_angle === (- 90)))) {
                    key_corr = "a";
                }
            }
        }
    }
    
    arrow_image_test_JS2.setOri(arrow_angle);
    resp_test_JS2_arrow.keys = undefined;
    resp_test_JS2_arrow.rt = undefined;
    _resp_test_JS2_arrow_allKeys = [];
    // keep track of which components have finished
    Test_JS2_ArrowComponents = [];
    Test_JS2_ArrowComponents.push(fixation_cross_test_JS2);
    Test_JS2_ArrowComponents.push(arrow_image_test_JS2);
    Test_JS2_ArrowComponents.push(test_JS2_text_arrow);
    Test_JS2_ArrowComponents.push(resp_test_JS2_arrow);
    
    Test_JS2_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function Test_JS2_ArrowRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'Test_JS2_Arrow'-------
    // get current time
    t = Test_JS2_ArrowClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *fixation_cross_test_JS2* updates
    if (t >= 0.0 && fixation_cross_test_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      fixation_cross_test_JS2.tStart = t;  // (not accounting for frame time here)
      fixation_cross_test_JS2.frameNStart = frameN;  // exact frame index
      
      fixation_cross_test_JS2.setAutoDraw(true);
    }

    frameRemains = 0.0 + 1.0 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((fixation_cross_test_JS2.status === PsychoJS.Status.STARTED || fixation_cross_test_JS2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      fixation_cross_test_JS2.setAutoDraw(false);
    }
    
    // *arrow_image_test_JS2* updates
    if (t >= 1 && arrow_image_test_JS2.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      arrow_image_test_JS2.tStart = t;  // (not accounting for frame time here)
      arrow_image_test_JS2.frameNStart = frameN;  // exact frame index
      
      arrow_image_test_JS2.setAutoDraw(true);
    }

    frameRemains = 1 + 0.35 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((arrow_image_test_JS2.status === PsychoJS.Status.STARTED || arrow_image_test_JS2.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      arrow_image_test_JS2.setAutoDraw(false);
    }
    
    // *test_JS2_text_arrow* updates
    if (t >= 1.35 && test_JS2_text_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      test_JS2_text_arrow.tStart = t;  // (not accounting for frame time here)
      test_JS2_text_arrow.frameNStart = frameN;  // exact frame index
      
      test_JS2_text_arrow.setAutoDraw(true);
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((test_JS2_text_arrow.status === PsychoJS.Status.STARTED || test_JS2_text_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      test_JS2_text_arrow.setAutoDraw(false);
    }
    
    // *resp_test_JS2_arrow* updates
    if (t >= 1.35 && resp_test_JS2_arrow.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      resp_test_JS2_arrow.tStart = t;  // (not accounting for frame time here)
      resp_test_JS2_arrow.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { resp_test_JS2_arrow.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS2_arrow.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { resp_test_JS2_arrow.clearEvents(); });
    }

    frameRemains = 1.35 + 4 - psychoJS.window.monitorFramePeriod * 0.75;  // most of one frame period left
    if ((resp_test_JS2_arrow.status === PsychoJS.Status.STARTED || resp_test_JS2_arrow.status === PsychoJS.Status.FINISHED) && t >= frameRemains) {
      resp_test_JS2_arrow.status = PsychoJS.Status.FINISHED;
  }

    if (resp_test_JS2_arrow.status === PsychoJS.Status.STARTED) {
      let theseKeys = resp_test_JS2_arrow.getKeys({keyList: ['a', 'l'], waitRelease: false});
      _resp_test_JS2_arrow_allKeys = _resp_test_JS2_arrow_allKeys.concat(theseKeys);
      if (_resp_test_JS2_arrow_allKeys.length > 0) {
        resp_test_JS2_arrow.keys = _resp_test_JS2_arrow_allKeys[_resp_test_JS2_arrow_allKeys.length - 1].name;  // just the last key pressed
        resp_test_JS2_arrow.rt = _resp_test_JS2_arrow_allKeys[_resp_test_JS2_arrow_allKeys.length - 1].rt;
        // was this correct?
        if (resp_test_JS2_arrow.keys == key_corr) {
            resp_test_JS2_arrow.corr = 1;
        } else {
            resp_test_JS2_arrow.corr = 0;
        }
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    Test_JS2_ArrowComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine && routineTimer.getTime() > 0) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function Test_JS2_ArrowRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'Test_JS2_Arrow'-------
    Test_JS2_ArrowComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    // was no response the correct answer?!
    if (resp_test_JS2_arrow.keys === undefined) {
      if (['None','none',undefined].includes(key_corr)) {
         resp_test_JS2_arrow.corr = 1;  // correct non-response
      } else {
         resp_test_JS2_arrow.corr = 0;  // failed to respond (incorrectly)
      }
    }
    // store data for thisExp (ExperimentHandler)
    psychoJS.experiment.addData('resp_test_JS2_arrow.keys', resp_test_JS2_arrow.keys);
    psychoJS.experiment.addData('resp_test_JS2_arrow.corr', resp_test_JS2_arrow.corr);
    if (typeof resp_test_JS2_arrow.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('resp_test_JS2_arrow.rt', resp_test_JS2_arrow.rt);
        routineTimer.reset();
        }
    
    resp_test_JS2_arrow.stop();
    return Scheduler.Event.NEXT;
  };
}

function EndRoutineBegin(snapshot) {
  return function () {
    //------Prepare to start Routine 'End'-------
    t = 0;
    EndClock.reset(); // clock
    frameN = -1;
    continueRoutine = true; // until we're told otherwise
    // update component parameters for each repeat
    next_end_resp.keys = undefined;
    next_end_resp.rt = undefined;
    _next_end_resp_allKeys = [];
    // keep track of which components have finished
    EndComponents = [];
    EndComponents.push(end);
    EndComponents.push(next_end);
    EndComponents.push(next_end_resp);
    
    EndComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent)
        thisComponent.status = PsychoJS.Status.NOT_STARTED;
       });
    return Scheduler.Event.NEXT;
  }
}

function EndRoutineEachFrame(snapshot) {
  return function () {
    //------Loop for each frame of Routine 'End'-------
    // get current time
    t = EndClock.getTime();
    frameN = frameN + 1;// number of completed frames (so 0 is the first frame)
    // update/draw components on each frame
    
    // *end* updates
    if (t >= 0.0 && end.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      end.tStart = t;  // (not accounting for frame time here)
      end.frameNStart = frameN;  // exact frame index
      
      end.setAutoDraw(true);
    }

    
    // *next_end* updates
    if (t >= 0.5 && next_end.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_end.tStart = t;  // (not accounting for frame time here)
      next_end.frameNStart = frameN;  // exact frame index
      
      next_end.setAutoDraw(true);
    }

    
    // *next_end_resp* updates
    if (t >= 0.5 && next_end_resp.status === PsychoJS.Status.NOT_STARTED) {
      // keep track of start time/frame for later
      next_end_resp.tStart = t;  // (not accounting for frame time here)
      next_end_resp.frameNStart = frameN;  // exact frame index
      
      // keyboard checking is just starting
      psychoJS.window.callOnFlip(function() { next_end_resp.clock.reset(); });  // t=0 on next screen flip
      psychoJS.window.callOnFlip(function() { next_end_resp.start(); }); // start on screen flip
      psychoJS.window.callOnFlip(function() { next_end_resp.clearEvents(); });
    }

    if (next_end_resp.status === PsychoJS.Status.STARTED) {
      let theseKeys = next_end_resp.getKeys({keyList: ['space'], waitRelease: false});
      _next_end_resp_allKeys = _next_end_resp_allKeys.concat(theseKeys);
      if (_next_end_resp_allKeys.length > 0) {
        next_end_resp.keys = _next_end_resp_allKeys[_next_end_resp_allKeys.length - 1].name;  // just the last key pressed
        next_end_resp.rt = _next_end_resp_allKeys[_next_end_resp_allKeys.length - 1].rt;
        // a response ends the routine
        continueRoutine = false;
      }
    }
    
    // check for quit (typically the Esc key)
    if (psychoJS.experiment.experimentEnded || psychoJS.eventManager.getKeys({keyList:['escape']}).length > 0) {
      return quitPsychoJS('The [Escape] key was pressed. Goodbye!', false);
    }
    
    // check if the Routine should terminate
    if (!continueRoutine) {  // a component has requested a forced-end of Routine
      return Scheduler.Event.NEXT;
    }
    
    continueRoutine = false;  // reverts to True if at least one component still running
    EndComponents.forEach( function(thisComponent) {
      if ('status' in thisComponent && thisComponent.status !== PsychoJS.Status.FINISHED) {
        continueRoutine = true;
      }
    });
    
    // refresh the screen if continuing
    if (continueRoutine) {
      return Scheduler.Event.FLIP_REPEAT;
    } else {
      return Scheduler.Event.NEXT;
    }
  };
}

function EndRoutineEnd(snapshot) {
  return function () {
    //------Ending Routine 'End'-------
    EndComponents.forEach( function(thisComponent) {
      if (typeof thisComponent.setAutoDraw === 'function') {
        thisComponent.setAutoDraw(false);
      }
    });
    psychoJS.experiment.addData('next_end_resp.keys', next_end_resp.keys);
    if (typeof next_end_resp.keys !== 'undefined') {  // we had a response
        psychoJS.experiment.addData('next_end_resp.rt', next_end_resp.rt);
        routineTimer.reset();
        }
    
    next_end_resp.stop();
    // the Routine "End" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset();
    
    return Scheduler.Event.NEXT;
  };
}

function endLoopIteration(scheduler, snapshot) {
  // ------Prepare for next entry------
  return function () {
    if (typeof snapshot !== 'undefined') {
      // ------Check if user ended loop early------
      if (snapshot.finished) {
        // Check for and save orphaned data
        if (psychoJS.experiment.isEntryEmpty()) {
          psychoJS.experiment.nextEntry(snapshot);
        }
        scheduler.stop();
      } else {
        const thisTrial = snapshot.getCurrentTrial();
        if (typeof thisTrial === 'undefined' || !('isTrials' in thisTrial) || thisTrial.isTrials) {
          psychoJS.experiment.nextEntry(snapshot);
        }
      }
    return Scheduler.Event.NEXT;
    }
  };
}

function importConditions(currentLoop) {
  return function () {
    psychoJS.importAttributes(currentLoop.getCurrentTrial());
    return Scheduler.Event.NEXT;
    };
}

function quitPsychoJS(message, isCompleted) {
  // Check for and save orphaned data
  if (psychoJS.experiment.isEntryEmpty()) {
    psychoJS.experiment.nextEntry();
  }
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  psychoJS.window.close();
  psychoJS.quit({message: message, isCompleted: isCompleted});
  
  return Scheduler.Event.QUIT;
}
